#!/usr/bin/env python

#-------------------------------------------------------------------------------
# SConstruct: ctu's tiny utilities
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <peter.j.bui@gmail.com>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

import os
import shutil
import sys

#-------------------------------------------------------------------------------
# Configuration
#-------------------------------------------------------------------------------

env = Environment()

env.build_dir   = 'build'
env.dest_dir    = os.environ['HOME']
env.lib_dir     = [os.path.join(env.dest_dir, 'lib')]
env.include_dir = [os.path.join(env.dest_dir, 'include')]

env.Append(CFLAGS = ['-Wall', '-std=c99'])

#-------------------------------------------------------------------------------
# Actions
#-------------------------------------------------------------------------------

def clean(target, source, env):
    os.system('scons -Q -c')
    if os.path.exists(env.build_dir):
	shutil.rmtree(env.build_dir)
	print "Removed " + env.build_dir

def uninstall(target, source, env):                                             
    if hasattr(env, 'uninstall_files'):
	for f in map(str, env.uninstall_files):
	    if os.path.exists(f):
		os.remove(f)
		print 'Removed ' + f
    else:
	print >>sys.stderr, 'No files to uninstall'

#-------------------------------------------------------------------------------
# Targets
#-------------------------------------------------------------------------------

env.targets = BUILD_TARGETS

if len(env.targets) == 0:
    env.targets.append('library')

#-------------------------------------------------------------------------------

env.Alias('clean', env.Command('#clean', '#SConstruct', clean))

#-------------------------------------------------------------------------------

Export('env clean uninstall')

#-------------------------------------------------------------------------------

env.SConscript('src/SConscript', build_dir='#build', duplicate=0)
env.SConscript('src/tests/SConscript', build_dir='#build/tests', duplicate=0)

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
