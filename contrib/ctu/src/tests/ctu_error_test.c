/*------------------------------------------------------------------------------
 * ctu_error_test.c: ctu error test 
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <peter.j.bui@gmail.com>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "ctu.h"

/*------------------------------------------------------------------------------
 * Functions 
**----------------------------------------------------------------------------*/

int
main(int argc, char* argv[]) {
    CTU_DebugStream = CTU_ErrorStream = stderr;

    ctu_debug("main", "argc: %d", argc);
    ctu_error("main", EXIT_SUCCESS, "argv[0]: %s", argv[0]);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
