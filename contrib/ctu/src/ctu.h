/*------------------------------------------------------------------------------
 * ctu.h: ctu's tiny utilities
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#ifndef	__CTU_H__
#define	__CTU_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "ctu_error.h"
#include "ctu_timer.h"
#include "ctu_type.h"

/*------------------------------------------------------------------------------
 * Documentation
**----------------------------------------------------------------------------*/

/** 
 * @mainpage CTU: CTU's Tiny Utilities
 * @author   Peter Bui
 *
 * This is a loose collection of small, but useful C functions.  Included in
 * this toolkit are:
 *
 * - Debug/error logging functions.
 * - Timing functions
 * - Type definitions
 *
 */

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
