/*------------------------------------------------------------------------------
 * ctu_error.c: ctu error functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "ctu.h"

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

FILE* CTU_DebugStream;
FILE* CTU_ErrorStream;

/*-----------------------------------------------------------------------------
 * Macros
**----------------------------------------------------------------------------*/

/**
 * CTU variable formatted print macro.
 * @param fs	FILE* stream
 * @param s	Tag string
 * @param fn	Function name string
 * @param fmt	Format string
 * @param argv	Format string arguments
 */

#define CTU_VFPRINTF(fs, s, fn, fmt, argv) \
    va_start((argv), (fmt)); \
    fprintf((fs), "[%s] %s: ", (s), (fn)); \
    vfprintf((fs), (fmt), (argv)); \
    fputc('\n', (fs));

/*------------------------------------------------------------------------------
 * Functions 
**----------------------------------------------------------------------------*/

/**
 * Output debug message.
 * @param fn	Function name string
 * @param fmt	Format string
 * @param ...	Format string arguments
 */

void
ctu_debug(const char* fn, const char* fmt, ...) {
    va_list argv;
    CTU_VFPRINTF(CTU_DebugStream, "D", fn, fmt, argv);
}

/*----------------------------------------------------------------------------*/

/**
 * Output error message.
 * @param fn	Function name string
 * @param e	Error code
 * @param fmt	Format string
 * @param ...	Format string arguments
 */

void
ctu_error(const char* fn, const int e, const char* fmt, ...) {
    va_list argv;
    CTU_VFPRINTF(CTU_ErrorStream, "E", fn, fmt, argv);
    exit(e);
}

/*----------------------------------------------------------------------------*/

/**
 * Output warning.
 * @param fn	Function name string
 * @param fmt	Format string
 * @param ...	Format string arguments
 */

void
ctu_warn(const char* fn, const char* fmt, ...) {
    va_list argv;
    CTU_VFPRINTF(CTU_DebugStream, "W", fn, fmt, argv);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
