/*------------------------------------------------------------------------------
 * ctu_timer.c: ctu timer functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/time.h>

#include "ctu_timer.h"
#include "ctu_type.h"

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

uint CTU_NumberOfTimers = 0;
const char** CTU_TimerStrings = NULL;

struct timeval* CTU_StartTime = NULL;
struct timeval* CTU_EndTime   = NULL;

double* CTU_ElapsedTime = NULL;
int*    CTU_TimedRuns   = NULL;

/*------------------------------------------------------------------------------
 * Functions 
**----------------------------------------------------------------------------*/

inline
void
ctu_timer_init(uint timers, const char* timer_strings[])
{
    CTU_StartTime = malloc(sizeof(struct timeval)*timers);
    CTU_EndTime   = malloc(sizeof(struct timeval)*timers);

    CTU_ElapsedTime = malloc(sizeof(double)*timers);
    CTU_TimedRuns   = malloc(sizeof(int)*timers);

    memset(CTU_ElapsedTime, 0, sizeof(double)*timers);
    memset(CTU_TimedRuns, 0, sizeof(double)*timers);

    CTU_NumberOfTimers = timers;
    CTU_TimerStrings   = timer_strings;
}

/*----------------------------------------------------------------------------*/

inline
void
ctu_timer_start(int i)
{
    gettimeofday(&CTU_StartTime[i], NULL);
}

/*----------------------------------------------------------------------------*/

inline
void
ctu_timer_stop(int i)
{
    gettimeofday(&CTU_EndTime[i], NULL);

    CTU_ElapsedTime[i] += (double)(CTU_EndTime[i].tv_sec - 
				   CTU_StartTime[i].tv_sec) + 
			  (double)(CTU_EndTime[i].tv_usec/1000000.0 - 
				   CTU_StartTime[i].tv_usec/1000000.0);
    CTU_TimedRuns[i]++;
}

/*----------------------------------------------------------------------------*/

inline
void
ctu_timer_reset(int i)
{
    CTU_ElapsedTime[i] = 0.0;
    CTU_TimedRuns[i]   = 0;
}

/*----------------------------------------------------------------------------*/

inline
double
ctu_timer_elapsed_time(int i)
{
    return (CTU_ElapsedTime[i]);
}

/*----------------------------------------------------------------------------*/

inline
double
ctu_timer_average_time(int i)
{
    return (CTU_ElapsedTime[i]/CTU_TimedRuns[i]);
}

/*----------------------------------------------------------------------------*/

inline
void
ctu_timer_print_summary(bool print_all)
{
    for (uint i = 0; i < CTU_NumberOfTimers; i++) 
	if (CTU_TimedRuns[i] > 0 || print_all) 
	    printf("[timer] %s = avg(%2.6lf), total(%2.6lf)\n", 
		   CTU_TimerStrings[i], 
		   ctu_timer_average_time(i),
		   CTU_ElapsedTime[i]);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
