#!/usr/bin/env python

#-------------------------------------------------------------------------------
# SConstruct: accelerated image registration
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <pbui@cse.nd.edu>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

import os
import shutil
import sys

#-------------------------------------------------------------------------------
# Configuration
#-------------------------------------------------------------------------------

env = Environment()

env.Tool('nvcc', toolpath = ['#contrib/scons'])

env.build_dir = '#build'
env.build_doc_dir = os.path.join(env.build_dir[1:], 'doc')
env.build_lib_dir = os.path.join(env.build_dir, 'lib')

env.dest_dir	  = ARGUMENTS.get('prefix', os.environ['HOME'])
env.dest_bin_dir  = os.path.join(env.dest_dir, 'bin')

env.Append(LIBS   = ['m'])

env.Append(CPPPATH = [os.path.join(env.dest_dir, 'include')])
env.Append(LIBPATH = [os.path.join(env.dest_dir, 'lib')])
env.Append(RPATH   = [os.path.join(env.dest_dir, 'lib')])

#-------------------------------------------------------------------------------

if int(ARGUMENTS.get('debug', 0)):
    env.Append(CFLAGS = ['-g'])
    
opt_level = int(ARGUMENTS.get('with_opt_level', 0))

if opt_level:
    env.Append(CFLAGS = ['-O' + str(opt_level)])

#-------------------------------------------------------------------------------
# Actions
#-------------------------------------------------------------------------------

def clean(target, source, env):
    os.system('scons -Q -c')
    for d in [env.build_dir, env.build_doc_dir]:
	if '#' in d: d = d[1:]
	if os.path.exists(d):
	    shutil.rmtree(d)
	    print "Removed " + d

def uninstall(target, source, env):                                             
    if hasattr(env, 'uninstall_files'):
	for f in map(str, env.uninstall_files):
	    if os.path.exists(f):
		os.remove(f)
		print 'Removed ' + f
    else:
	print >>sys.stderr, 'No files to uninstall'

#-------------------------------------------------------------------------------
# Targets
#-------------------------------------------------------------------------------

env.targets = BUILD_TARGETS

if len(env.targets) == 0:
    env.targets.append('build')

#-------------------------------------------------------------------------------

env.Alias('build_doc', env.Command(env.build_doc_dir, '#Doxyfile', 'doxygen'))
env.Alias('clean', env.Command('#clean', '#SConstruct', clean))

#-------------------------------------------------------------------------------

Export('env clean uninstall')

#-------------------------------------------------------------------------------

env.SConscript('src/SConscript', build_dir=env.build_dir, duplicate=0)

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
