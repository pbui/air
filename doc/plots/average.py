#!/usr/bin/env python

import sys

if __name__ == '__main__':
    print sum([float(x) for x in sys.argv[1:]])/len(sys.argv[1:])
