%-------------------------------------------------------------------------------
% air_introduction.tex: fast image registration system thesis
%-------------------------------------------------------------------------------

\chapter{INTRODUCTION}

%-------------------------------------------------------------------------------

\section{Overall Problem}

\paragraph{}

Image registration is the process of aligning two or more images in order to
determine the point-by-point correspondence among a set of images
\cite{goshtasby}.  In other words, given two images where the first is called
the {\it source} and the second the {\it target}, the purpose of image
registration is to find the set of transformation coefficients that map the
{\it target} to the {\it source} image.  This technique is used in a variety of
scientific applications such as remote sensing, map updating, weather
forecasting, and computer vision to provide an integrated view of the image
data \cite{zitova}.  In the medical field, image registration is used in
clinical tasks such as diagnosis, radiotherapy, and image-guided surgery.
Because of the widespread use of this procedure and the immense computational
workload required, image registration has been a popular target for
acceleration in recent studies.

\begin{figure}[htb!]
\centering
\begin{enumerate}

\item{\bf Image Generation:} Generate a {\it temporary} image using the {\it
source} image and a proposed set of transformation coefficients.

\begin{singlespace}\centering $\downarrow$ \end{singlespace}

\item{\bf Similarity Measurement:} Compare this {\it temporary} image to the
{\it target} image to test for similarity.

\begin{singlespace}\centering $\downarrow$ \end{singlespace}

\item{\bf Optimization:} Update the transformation coefficients using the
similarity measurement as a cost function in an optimization algorithm.

\end{enumerate}
\caption{Image Registration Pipeline}
\label{fig:in_irp}
\end{figure}

One form of image registration is rigid registration, which primarily deals
with global image alignment and shifting.  This is in contrast to elastic
registration which can account for changes in local morphology over time
\cite{plishker}.  In this investigation, we focus on 2-D rigid image
registration where the objective of the registration procedure is to find a
transformation to apply to the \textit{source} image that best aligns it with
the \textit{target} image (or visa versa).  To accomplish this task, the
general pipeline in Figure \ref{fig:in_irp} is followed.

In this general image registration pipeline, the process of image generation,
similarity measurement, and optimization is repeated until an acceptable level
of tolerance is achieved.   Normally, this tolerance level equates to a high
similarity between the {\it target} and {\it temporary} images as determined by
the similarity measurement function.  The resulting transformation coefficients
from this procedure maps the relationship between the {\it target} and {\it
source} and is used for integrated analysis such as motion detection, feature
extraction, and other image processing tasks.

Because of the widespread use of image registration and its importance to
different scientific and medical tasks, it is necessary to have a fast and
accurate implementation.  In order to speed up an image registration
application, it is necessary to accelerate the critical sections of code that
creates a bottleneck in the whole system.  One possible means of doing this is
by taking advantage of emerging parallel architectures such as the graphics
processing unit (GPU) to produce a fast and accurate 2-D rigid image
registration system.

%-------------------------------------------------------------------------------

\section{Objective}

\paragraph{}

The primary objective of this thesis is to speed up a 2-D rigid body image
registration program by taking advantage of the processing power of a GPU and
compare the results to that of our reference CPU implementation.  Using a GPU
for scientific computing involves employing General-Purpose computation on GPUs
(GPGPU) techniques that have only recently been studied and explored.
Normally, programming the GPU requires extensive knowledge of graphics
programming and the use of graphics systems such as OpenGL or DirectX.
However, with the introduction of stream processors to GPUs, new frameworks
such as NVIDIA's Compute Unified Device Architecture (CUDA) enable programmers
to accelerate applications on the GPU without the need to use these
conventional graphics layers.  This project takes advantage of the CUDA
framework, compares the results of our CPU-based implementation and our
GPU-based implementation, and provides an analysis of utilizing a GPU in
accelerating an image registration application.

%-------------------------------------------------------------------------------

\section{Approach}

\paragraph{}

In most registration implementations, the image generation and similarity
measurement are the most computationally intensive aspects of the registration
process since they involve performing convolutions or computations on each
image pixel.  Accelerating an image registration application, then would
involve speeding up these two critical sections.  For this study, we accelerate
our 2-D rigid image registration application by implementing these
computationally intensive components using the CUDA programming environment to
take advantage of the parallel processing capabilities of the NVIDIA Tesla C870
GPU.  The intrinsic data parallelism in image registration makes the GPGPU
(general-purpose computing on graphics processing units) approach a suitable
technique for accelerating our image registration system.

%-------------------------------------------------------------------------------

\section{Thesis Organization}

\paragraph{}

This thesis examines the process of porting the CPU implementation to the CUDA
framework and analyzes the resulting performance gains along with any potential
pitfalls.  Although a sizable amount of speedup is obtained, the implementation
still falls short of the maximum potential speed increase.  Our study focuses
on understanding the obstacles encountered in developing for CUDA that inhibit
maximum performance and on possible strategies and methods that can be used
overcome these problems.

The remainder of the thesis is as follows: The next section reviews related
work involving accelerating image registration using GPGPU techniques.  Next,
we explain general image registration and the particular algorithm used in our
application.  After this, an initial profiling and analysis of the CPU
implementation is examined along with the compiler optimizations used in
building the software.  These CPU results provide the motivation for exploring
the GPU as an acceleration platform.  Once this is established, the CUDA
implementation of the algorithm is presented with an examination of potential
programming traps and the use of key practices to achieve good speedup.
Following this, we compare the CUDA implementation's performance and accuracy
versus the fastest CPU version and use profiling data to analyze the
experimental results.  Finally, we discuss possible improvements to our
implementation, summarize the general lessons learned from this project, and
suggest possible improvements to the CUDA programming environment to obtain
further performance and productivity gains.

%-------------------------------------------------------------------------------
% vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
