%-------------------------------------------------------------------------------
% air_results.tex:
%-------------------------------------------------------------------------------

\chapter{GPGPU RESULTS AND ANALYSIS}

%-------------------------------------------------------------------------------

\section{Test Environment}

\paragraph{}

As noted in Chapter \ref{ch:cpu_results}, the host system has a Intel Quad-Core
Q6700 2.66 GHz CPU and 8.0 GB of RAM and has Ubuntu Linux 8.04 (kernel 2.4.20).
The graphics processing unit on the system is the NVIDIA Tesla C870 which has
128 stream processors and 1.5 GB of on-chip memory \cite{tesla} and runs as a
standalone PCI-Express accelerator.  To compile the program codes we used GCC
4.1.2 with an optimization level of 3 (ie. {\tt -O3}) as this was determined
the optimal level in Section \ref{sec:cpu_results_performance}.  For the CUDA
specific software components, we utilized NVIDIA's CUDA 1.1 software
development kit to build the necessary source code. 

For these experiments, we compared the performance and accuracy of the CUDA
implementation against our fast CPU version on the data set described earlier.
Once again, Table \ref{tbl:images} contains the complete listing of the
grayscale images used to test our image registration codes.  We used the exact
same sequence of application invocations and sets of images used in the CPU
experiments to measure the performance of our accelerated GPU implementation.
Moreover, the experimental tests were run ten times across a period of about a
week and the results are summarized in Table \ref{tbl:all_images_table}.  Since
both bilinear and bicubic interpolation kernels were implemented, we tested
both methods in these experiments to determine the performance and accuracy of
each.

\begin{table}[htb!]
\centering
\begin{tabular}{|r|c|c|c|c|}
\hline
\textbf{lenna}	& Version   & Run-time	& Speedup   & PSNR  \\
\hline
		& CPU	    & 5.19	& 1.00	    & 56.32 \\
		& Bilinear    & 0.69	& 7.48	    & 51.60 \\
		& Bicubic    & 0.80	& 6.50	    & 55.43 \\
\hline
\textbf{ndbuntu}& Version   & Run-time	& Speedup   & PSNR  \\
\hline
		& CPU	    & 12.49	& 1.00	    & 56.35 \\
		& Bilinear    & 0.74	& 16.80	    & 47.05 \\
		& Bicubic    & 0.98	& 12.64	    & 51.27 \\
\hline
\textbf{halo}	& Version   & Run-time	& Speedup   & PSNR  \\
\hline
		& CPU	    & 23.73	& 1.00	    & 44.19 \\
		& Bilinear    & 0.83	& 28.40	    & 31.70 \\
		& Bicubic    & 1.30	& 18.25	    & 34.60 \\
\hline
\textbf{jump}	& Version   & Run-time	& Speedup   & PSNR  \\
\hline
		& CPU	    & 48.09	& 1.00	    & 55.86 \\
		& Bilinear    & 1.04	& 46.41	    & 47.92 \\
		& Bicubic    & 2.07	& 23.22	    & 48.34 \\
\hline
\textbf{victory}& Version   & Run-time	& Speedup   & PSNR\\
\hline
		& CPU	    & 92.50	& 1.00	    & 52.83 \\
		& Bilinear    & 1.36	& 67.89	    & 42.31 \\
		& Bicubic    & 3.16	& 29.17	    & 42.85 \\
\hline
\textbf{crabnebula}& Version   & Run-time	& Speedup   & PSNR\\
\hline
		& CPU	    & 205.31	& 1.00	    & 54.26 \\
		& Bilinear    & 2.24	& 91.47	    & 44.86 \\
		& Bicubic    & 6.24	& 32.92	    & 45.80 \\
\hline
\end{tabular}
\caption{SUMMARY OF EXPERIMENTAL RESULTS FOR EACH IMAGE}
\label{tbl:all_images_table}
\end{table}

%-------------------------------------------------------------------------------

\section{Performance}

Figure \ref{fig:thesis_all_images_average_runtime} shows the average execution
time for each image registration on all the test images which are displayed
along the \textit{X} axis in sorted order (by image size).  It is clear from
this chart that the running time of the CPU version is much higher than that of
the CUDA versions.  In fact, for the smaller images such as {\it lenna}, the
bars for the GPU implementations are barely visible.  Moreover, this gap
appears widen as the size of the input images grows.  Additionally, the running
time for the bicubic version is slightly higher than the bilinear, manifesting
that it is slower than its simpler counterpart. 

\begin{figure}[htb!]
\centering
\includegraphics[width=4.75in]{../plots/thesis_all_images_average_runtime}
\caption{Average running time for CPU and GPU implementations}
\label{fig:thesis_all_images_average_runtime}
\end{figure}

The graph in Figure \ref{fig:thesis_all_cuda_size_vs_su3} presents the speedup
of the CUDA versions over the CPU implementation as the size of the image
increases from left to right.  The bilinear implementation was able to achieve
between $7.5\times$ - $91.5\times$ speedup while the bicubic implementation was
able to obtain between $6.5\times$ - $33\times$ speedup.  This difference in
speedup is as expected since bicubic interpolation is not only more
computationally complex than bilinear filtering, but it also requires 16 memory
fetches while bilinear only needs 4.  These additional memory fetches, in
effect, stall the threads in the bicubic kernel, increasing the running time.
Because of the increased memory accesses the bicubic kernel is much less
efficient than the bilinear implementation.  Another factor to consider is that
the bilinear kernel was implemented using the built-in texture unit's bilinear
filtering capabilities, while the bicubic kernel required manual software
implementation.

\begin{figure}[htb!]
\centering
\includegraphics[width=4.75in]{../plots/thesis_all_cuda_size_vs_su3}
\caption{Average speedup over CPU version as image size increases}
\label{fig:thesis_all_cuda_size_vs_su3}
\end{figure}

These graphs also reveal that the bilinear version scales well as image size
increases while the bicubic version levels off much more quickly.  Once again,
this is mainly due to the increase in memory accesses leading to less efficient
kernels.  This inefficiency meant that the bilinear kernel was between 1.2 to
2.7 times faster than the bicubic version.  Overall, though, both the bilinear
and bicubic CUDA kernels performed well and demonstrated significant speedup
over the CPU version and scaled as the image size grew.  

\section{Accuracy}

Once again, as explained in Section \ref{sec:cpu_results_accuracy}, we measured
the accuracy of the image registrations by computing the
Peak-Signal-to-Noise-Ratios.  In this case, numbers between $30 - 50$ denote
high fidelity between the output transformed image and the target image.

The chart in Figure \ref{fig:thesis_all_images_average_psnr} summarizes the
PSNR results from our experiments with the CUDA image registration
implementation.  On the {\it Y} axis are the Peak-Signal-to-Noise-Ratios for
the CPU and CUDA implementations.  The PSNR bars are clustered together
according to the associated image  with the smallest on the left and the
largest on the right of the {\it X} axis.

Figure \ref{fig:thesis_all_images_average_psnr} shows that across the board the
CPU version had a higher PSNR than the CUDA implementations.  This means that
the standard CPU implementation generated more accurate transformation
coefficients and thus produced the best registrations.  This is particularly
true as the size of the image increases, and the accuracy of the GPGPU
implementations drop off.

\begin{figure}[htb]
\centering
\includegraphics[width=4.75in]{../plots/thesis_all_images_average_psnr}
\caption{Average Peak-Signal-to-Noise Ratio for All Images}
\label{fig:thesis_all_images_average_psnr}
\end{figure}

However, the CUDA versions are still relatively accurate.  As expected, the
bicubic interpolation kernel produced a higher PSNR than the bilinear kernel,
although not by much.  In fact, the experimental results show that the bilinear
kernel is generally 96\% as accurate as the bicubic kernel.  For the smaller
images, the bicubic definitely produces better results than the bilinear, but
as the size of the image increases, this difference in accuracy levels off
until both are similar.

Overall, all of the implementations produced PSNRs in the range $35 - 55$,
which denotes high similarity to the actual {\it target} image.  The
discrepancy between the CPU and CUDA versions is most likely due to the
non-standard floating point implementation in the GPU which leads to subtle but
possibly damaging errors as noted previously \cite{obukhov} \cite{hillesland}.
This may also explain the narrowing gap between the bilinear and bicubic
kernels as the size of the image increases.  With increasing data sizes, more
and more small floating point errors can accumulate over time and negatively
affect our application. 

Looking back at the previous performance graphs, it is clear that there is
trade-off between performance and accuracy which highly favors the bilinear
kernel since we can get much more speedup and still have a relatively decent
amount of accuracy.  Unless absolute accuracy is required, it would appear
reasonable to take advantage of the faster bilinear implementation over the
bicubic which is slower, and does not produce much better results, particularly
for larger image sets.

\section{Profiling}

In order to understand which parts of the image registration pipeline the CUDA
implementations were spending most of their time in, we profiled each test run
by timing each relevant function call.  Figure \ref{fig:gpgpu_2_profiling}
shows the break down of the registration functions and their percentage of the
running time for the smallest ({\it lenna}) and largest ({\it crabnebula})
images with each of the CUDA implementations and the CPU version. 

The smallest image our data set is {\it lenna} and it had a total running time
of 0.7 - 0.8 seconds for both CUDA versions.  As shown in Figure
\ref{fig:gpgpu_2_profiling}, the vast majority of the execution time is spent
in the pyramid construction function, which is odd since the pyramid function
is only called once and it is not nearly as computationally intensive as the
transform function.  What happened was that the since the pyramid construction
is the first CUDA function to be called, it triggers the device initialization
routine in the CUDA drivers and the program stalls as it waits for the drivers
and device to setup and perform some initial configuration.

\begin{figure}[htb]
\centering
\includegraphics[width=4.75in]{../plots/gpgpu_2_profiling}
\caption{Running Time Profiling for {\it lenna} and {\it crabnebula}}
\label{fig:gpgpu_2_profiling}
\end{figure}

We determined that this was the cause of the stalling after looking at the
device startup micro-benchmark explained in the Section \ref{sec:gpgpu_micro}.
The results revealed that the CUDA device takes about $0.5-0.6$ seconds to
initialize before any execution on the GPU can occur.  Since the total run-time
for {\it lenna} is only $0.7 - 0.8$ seconds, this $0.5 - 0.6$ second startup
cost completely dominates the running time, distorts the run-time cost of the
pyramid component, and prevents an overall larger speedup in the application.  

Note that this this micro-benchmark does not refute the claims of Volkov and
Demmel \cite{volkov}, as they measured the cost to launch a kernel (which we
confirmed using our empty kernel micro-benchmark) and not the startup time of
the CUDA device.  Before any kernel can be called, the CUDA driver must perform
some configuration such as moving the kernel code data to the device and
setting up the memory bookkeeping data structures.  This is why many of the
examples in the NVIDIA software developers' kit include sections of code to
``warm up'' the device, masking away this startup cost from the final timing
information.  Our micro-benchmark measured this time to be around $0.5 - 0.6$
seconds and it imposes a significant penalty in the performance of the smaller
images such as {\it lenna}.

The running time percentage profiling for the largest image, the {\it
crabnebula} data file also shown in Figure \ref{fig:gpgpu_2_profiling},
manifests a different set of issues.  In this case, the transformation kernel
execution dominates the running time which is desirable because this means that
we are mainly occupied in the data parallel section of our code rather than the
serial portion.  Since the CUDA kernel dominates the running time, we are able
to obtain a large amount of speedup.

However, there are a few other interesting trends in Figure
\ref{fig:gpgpu_2_profiling} that need to be pointed out.  First, the
``Everything Else'' portion of the algorithm grew in percentage for the {\it
crabnebula} tests relative to the {\it lenna} profiling.  This portion of the
code is mainly reading the image from the filesystem and storing it in host
memory.  Due to the large size of the image, this serial portion became a
bigger factor in terms of running time percentage.  Likewise, the pyramid
component remains a big factor in the speed of the program.  Once again, the
startup cost of the CUDA device is a serious bottleneck and limits overall
speedup.

The remainder of the micro-benchmark results deal with memory bandwidth and
performance do not provide much interesting insight other than to confirm what
is already documented in the NVIDIA programming guide \cite{nvidia} and Tesla
specifications \cite{tesla}.  They can briefly be summarized as follows:

\begin{itemize}

\item{\bf Memory Transfers:} Copying data from the host to the device yielded
an average bandwidth of 1552.0 megabytes per second, while copying from the
device to the host achieved an average of 1615.2 megabytes per second.  These
results are inline with the official specifications and the standard NVIDIA
bandwidth testing tool included in the SDK.

\item{\bf Sequential and Random Access:} These micro-benchmarks were a bit
tougher to implement because the NVCC compiler performs optimizations that
remove unnecessary instructions.  In the case of these tests, we could not
simply just read a range of memory address; we had to write to global memory to
prevent the kernel from optimizing out our memory reads.  Because of this, the
micro-benchmarks cannot provide absolute timing results.  

However, the micro-benchmarks do provide insight into the relative performance
capabilities of the various memory types.  For instance, the micro-benchmarks
show that sequential and random reading of constant and shared memory are the
fastest and are about the same, which is as expected.  Likewise, the results
also show that global memory is the slowest and that texture memory is slightly
faster than global memory (about 40\% faster).  Moreover, random access to
texture memory was slightly slower than sequential access (6\%), which makes
sense considering that texture memory is spatially cached.

\end{itemize}

The key result demonstrated by our profiling and micro-benchmarks is that in
porting our CPU implementation to the GPU, we introduced new issues and reveal
potential bottlenecks such as the startup time of the CUDA device and the
increased role of serial portions of the implementation.  

Although a GPU core is very different from a CPU core, it is reasonable to
consider that since the Tesla C870 has 128 stream processors, a perfect linear
speedup would mean that the maximum theoretical speedup on the CUDA device is
$128\times$.  As shown above, our code is achieving up to $90\times$ speedup
with the bilinear kernel and $33\times$ with the bicubic, which is about 71\%
and 26\% of the total possible speedup, respectively.  As noted, the addition
of unanticipated bottlenecks and sources of serialization such as the device
initialization time contribute in limiting our speedup.  Furthermore, the high
cost of memory fetching greatly reduced the efficiency of the kernels and also
prohibited performance gains.  Overall, we were able to achieve significant
speedup, though not the maximum theoretical.

%-------------------------------------------------------------------------------
% vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
