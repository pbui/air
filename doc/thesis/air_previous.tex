%-------------------------------------------------------------------------------
% air_previous.tex: previous work
%-------------------------------------------------------------------------------

\chapter{PREVIOUS WORK}
\label{ch:previous}

%-------------------------------------------------------------------------------

The growing popularity and use of graphics processing units in scientific
computing has been summarized by Owens, et al.\ in their overview of GPU
computing \cite{owens}.  They note that because of the increasing amount of
memory bandwidth and computational horsepower on graphics processing units,
GPUs substantially outpace their CPU counterparts, thus making them attractive
acceleration platforms.  For instance, the NVIDIA programming guide
\cite{nvidia} states that current GPUs such as NVIDIA's Tesla C870 have up to
128 concurrent processors capable of over 300+ giga-floating point operations
per second (in aggregate) while providing high memory bandwidth (80+ GB/s) to
data stored locally on the graphics units.  According to Fung and Mann
\cite{fung}, these attributes make GPUs attractive platforms for accelerating
image processing applications such as image registration which exhibit
data-level parallelism and have high computational costs.

In the past, applications using the GPGPU approach for application acceleration
have employed the method of mapping their computations onto the shader and
vertex processors of the graphics unit.  This is done by programming in various
shading languages such as Cg, GLSL, or HLSL, along with graphics libraries such
as OpenGL or DirectX and loading those programs directly into the graphics
rendering pipeline \cite{harris}.  Ino et al.\ used this technique to present a
fast rigid registration method in their OpenGL-based implementation \cite{ino}
that demonstrated between $5.0\times$ and $9.6\times$ speedup while maintaining
a tolerable level of accuracy.  Kubias et al.\ were able to post similar
results in their implementation of rigid image registration \cite{kubias} with
the added twist that they implemented eight similarity measurements on the GPU
and used these in aggregate to optimize the registration.  Both of these
studies took advantage of not only the parallel processing abilities of the
GPU, but also the hardware supported bilinear filtering provided by textures.
Of course, this technique requires programmers to manipulate their algorithms
and data structures to fit the graphics programming model, which is not always
straightforward.

The introduction of NVIDIA's CUDA, however, allows programmers to avoid using
graphics programming libraries and instead work in a stream processing
environment \cite{nvidia}.  In this framework the vertex or fragment processors
in the graphics pipeline are unified and abstracted as a set of programmable
concurrent stream processors.  Rather than use shading languages, programmers
use a C/C++-like programming environment with common programming constructs
such as arrays, pointers, and variables to program the GPU.  This increase in
user programmability allows developers without a deep graphics programming
background to quickly and effectively take advantage of the parallel processing
abilities of the graphics processing unit.  Recently, Sugiura et al.
\cite{sugiura} have taken advantage of this programming environment to
accelerate rigid image registration used in bronchoscope tracking by a factor
of $16\times$ with moderate levels of accuracy.  Likewise,
Muyan-\"{O}z\c{c}elik et al.\ use CUDA to implement fast deformable (elastic)
image registration with a factor of $55\times$ speedup \cite{ozcelik}.  They
also provide an exhaustive and detailed accounting of their implementation and
experimental results.

In this study, we implement a 2-D rigid image registration system similar to
Ino \cite{ino} and Kubias \cite{kubias} using the CUDA programming environment.
Generally, we use a pyramid-based algorithm outlined by Th{\'{e}}venaz
\cite{thevenaz} as the basis of our image registration application, but we omit
cubic spline representations and use the Nelder-Mead simplex optimizer function
from the GNU Scientific Library \cite{gsl} rather than the proposed
Marquardt-Levenberg algorithm.  Moreover, unlike previous studies we implement
the more computationally intensive bicubic interpolation scheme in addition to
the hardware supported bilinear interpolation method in order to increase
registration accuracy which is needed in some applications such as motion
tracking.  Furthermore, we follow the methodology of Muyan-\"{O}z\c{c}elik
\cite{ozcelik} in providing a thorough analysis of the implementation of our
algorithm, noting possible CUDA traps and how to optimize for maximum speedup.
To aid in this analysis we examine the efficiency of our implementation and
profiling data to identify limitations of the CUDA platform.  At the end, we
review the obstacles and issues present in the CUDA framework and propose
potential means of addressing them.

%-------------------------------------------------------------------------------
% vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
