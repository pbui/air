%-------------------------------------------------------------------------------
% air_image.tex: image registration
%-------------------------------------------------------------------------------

\chapter{IMAGE REGISTRATION}

%-------------------------------------------------------------------------------

\section{General Image Registration}

As noted in the introduction, the purpose of image registration is to
geometrically align two or more images to enable in-depth data analysis such as
feature detection, or motion sensing.  Image registration is especially
necessary to compare and integrate image data that comes from different
sources, times, or perspectives as would be in the case of medical imaging,
aerial mapping, computer vision, and many other applications.  

\begin{figure}[htp!]
\centering 
\includegraphics[width=3.0in]{../diagrams/image_registration_workflow_thesis}
\caption{Image Registration Work-flow}
\label{fig:ir_irw}
\end{figure}

The image registration workflow is illustrated in Figure \ref{fig:ir_irw}.
Overall, the process takes the form of a global optimization or search
algorithm.  In this iterative process, the application starts with a {\it
source} and {\it target} image (also known as the {\it reference} and {\it
sensed} images).  The purpose of the program is to find a way to transform the
{\it source} image to align with the {\it target}, although the opposite
mapping may also be computed using the same registration procedure.  In either
case, one image is held constant while the other is repeatedly transformed
until it matches the former image.   Once this is accomplished, the goal of
finding the transformation parameters that maps the one image to the other is
achieved and this information is then returned to the user who can then utilize
it in integrated analysis of the image data.

The remainder of this chapter will explore this general image registration
workflow and its various components before detailing the specifics of the
particular design and implementation used in the thesis.  As a note, although
we focus mainly on the registration of 2-D images, the concepts presented in
this chapter apply equally to 3-D volumes with slight modifications.

\subsection{Image Generation}

\paragraph{}The first step in the general image registration process is to
transform the {\it target} image using a proposed set of transformation
coefficients to generate a new {\it temporary} image which will be compared to
the {\it source} image in the next step.  By transform, we mean perform a
mathematical computation on the {\it target} image data to deterministically
generate a new {\it temporary} image.  This mathematical operation provides a
mapping between the images, providing a means to relate the two images in a
well-defined manner.  The nature of this operation is specified by the proposed
set of transformation coefficients, which is usually a set of parameters in the
form of a matrix that governs how to generate the {\it temporary} image.

\begin{figure}[htp!]
\centering 
\includegraphics[width=3.75in]{../diagrams/image_generation_thesis}
\caption{Image Generation}
\label{fig:ir_ig}
\end{figure}

Figure \ref{fig:ir_ig} shows an example of the image generation process.  As
shown in the diagram, the image generation component is given the {\it source}
image and an transformation parameter as input.  The component computes its
output by using the coefficients as specified by the input parameters to
transform the {\it source} image to produce a new {\it temporary} image.

Normally, the image generation component is composed of two internal functions:

\begin{enumerate}

\item{\bf Transformation:} The first stage in the image generation process is
to compute how image location {\it (x, y)} from the {\it source} image will map
into the new {\it temporary} image.  This is usually done by applying the
coefficients to the current location and performing a mathematical computation
to produce a new set of coordinates {\it (x', y')}.

\item{\bf Interpolation:} After the new coordinates have been calculated, it is
necessary to then compute the new pixel value at the new location.  To provide
a smooth image (i.e. without jagged edges or blocky regions) some sort of
weighted function is used to average neighboring intensities.  This method is
referred to as interpolation which uses known pixel values from neighboring
locations to produce a new value for the computed location.

\end{enumerate}

To produce the new {\it temporary} image, a technique known as inverse
transform is used.  This method incorporates both the transformation and
interpolation functions described above in the following manner: 

\vspace{12pt}

\lstset{language=python, numbers=left, numberstyle=\tiny, basicstyle=\small, 
        frame=single, caption={Inverse Transform}, label={lst:ir_it},
	captionpos=b, float=h,
	showstringspaces=false,
	emph={transform, interpolate}, emphstyle=\it}
\begin{lstlisting}
for each pixel (x, y) in Temporary:
    x', y'          = transform(x, y, coefficients)
    Temporary[x, y] = interpolate(x', y', Source)
\end{lstlisting}

As shown in the pseudo-code in Listing \ref{lst:ir_it}, the image generation
module performs a convolution over every pixel in the {\it temporary} image.
This means that for each pixel location {\it (x, y)} in the {\it temporary}
image, we first compute the new set of coordinates {\it (x', y')} based on the
proposed set of coefficients.  This new location is the mapping from the {\it
temporary} image back to the {\it source} image.  Next, the new pixel value for
the location {\it (x, y)} in the {\it temporary} image is calculated by
performing an interpolation on the pixel values from the {\it source} image at
the location computed by the transformation function.  

Since the inverse transform method iterates over every pixel location in the
{\it temporary} image, our final output result never contains any holes, which
is important for an accurate registration.  When this inverse transform process
is completed, the new {\it temporary} image data will be ready to be passed on
to the next step in the image registration workflow, the similarity
measurement.

\subsection{Similarity Measurement}

\paragraph{} Since the goal of the image registration process is to find the
mapping between the {\it target} and {\it source} images, there must be a way
to computationally compare the similarity of the two sets of data.  This
deterministic comparison usually takes the form of an error calculation that
measures the difference between the two images as a numerical value.  

\begin{figure}[htp!]
\centering 
\includegraphics[width=3.75in]{../diagrams/similarity_measurement_thesis}
\caption{Similarity Measurement}
\label{fig:ir_sm}
\end{figure}

Similar to the inverse transform technique described in the previous section,
the similarity measurement operation usually involves iterating over each pixel
in both the {\it temporary} and {\it target} images and computing an error
calculation function at that particular location.  These errors are then summed
and then divided by the total number of pixels in one image to determine the
average similarity between the {\it temporary} and the {\it target} images.
Figure \ref{fig:ir_sm} demonstrates the inputs and outputs of this component of
the image registration workflow.

Once the similarity has been measured, the image registration program tests if
an acceptable level of tolerance (as determined by the user) has been reached.
If such a level has been reached, then the registration process ends and the
proposed set of coefficients is returned as the mapping between the {\it
source} and {\it target} images.  Otherwise, this error value is passed on to
the optimization module to be used in calculating the a new proposed
transformation.

\subsection{Optimization}

\paragraph{} The final step in the image registration workflow is the
optimization step.  This function uses the error calculated by the similarity
measurement module as the cost function to an optimization algorithm.  Usually,
these optimization algorithms work by minimizing or maximizing a numerical
function, which means that given a set of parameters and a function, the
algorithm tries to either minimize or maximize the output of the function by
searching through the parameter space.  This search process is done by
modifying the set of parameters in a controlled fashion to converge at a
minimum or maximum.

\begin{figure}[htp!]
\centering 
\includegraphics[width=3.75in]{../diagrams/optimization_thesis}
\caption{Optimization}
\label{fig:ir_op}
\end{figure}

In the case of image registration, the numerical function to be optimized is
actually the similarity measurement and the parameters to this function are the
proposed transformation coefficients.  As shown in Figure \ref{fig:ir_op}, the
optimization module uses the similarity measurement along with the previous set
of coefficients to produce a new proposed transformation estimate which is then
sent to the image generation component and another registration iteration is
performed.  

This whole process of image generation, similarity measurement, and
optimization is repeated until a tolerable level of similarity has been
reached.  Once this occurs, the image registration is complete and the proposed
transformation coefficients are then returned to the end user as the mapping
from the {\it source} image to the {\it target} image.  The resultant
coefficients can then be used in integrated analysis such as motion detection,
feature selection, etc.

%-------------------------------------------------------------------------------

\section{Pyramid-based Rigid Body Image Registration}
\label{sec:ir_pbrbir}

\paragraph{}The specific image registration implementation used in this thesis
employs a pyramid-based rigid image registration algorithm as shown in Figure
\ref{fig:ir_acf}.  Overall, this algorithm follows the image registration
workflow described previously with a few additional details and modifications:

\begin{figure}[htp!]
\centering 
\includegraphics[width=4.5in]{../diagrams/algorithm_cpu_flow_thesis}
\caption{Pyramid-based Rigid Image Registration}
\label{fig:ir_acf}
\end{figure}

\begin{enumerate}

\item{\bf Affine Rigid Transformations:} Our algorithm is only concerned with
global rigid transformations such as translation, scaling, and rotation which
are all considered affine transformations.

\item{\bf Pyramid Search:} In order to algorithmically speedup our image
registration application, we employ a pyramid-based search technique to reduce
the total number of iterations required to perform the registration.

\item{\bf Interpolation Methods:} For our implementation, we consider two
methods of interpolation: bilinear and bicubic.  The former is fast and used in
most image registration systems, while the latter is generally more accurate
but more computationally intensive and thus slower.

\item{\bf Error Calculation:} To measure similarity, the design used in this
project uses the mean squared error metric commonly used in image registration.

\item{\bf Optimization:} The Nelder-Mead Simplex optimizer from the GNU
Scientific Library\cite{gsl} is used to update the proposed transformation
coefficients.

\end{enumerate}

Clearly, as seen in Figure \ref{fig:ir_acf}, the general image registration
workflow and the image generation, similarity measurement, and optimization
loop remains at the core of the pyramid-based algorithm used in this project.
The only major component added to the workflow is the use of pyramid search,
which allows us to algorithmically speed up the image registration application.

The following sections explore each of the modifications to the general image
registration workflow, examine their workings, and explain how they fit into
the overall design of the 2-D image registration system.

\subsection{Affine Transformations}

\paragraph{}

As noted previously, the image registration system used in this thesis is only
concerned with global rigid transformations such as translation, scaling, and
rotation.  These operations are categorized as affine transformations because
they preserve both the collinearity relation between points and the ratios of
distances of points along a line.  This means that any points that were
collinear before the transformation will remain so after the transformation,
and that all distance ratios will be maintained following the transformation.
The main task of our image registration program is to detect these affine
transformations in order to build a coherent mapping between the {\it source}
and {\it target} images.

In order to construct a mathematical relationship between the two images, it is
necessary to have an appropriate means of controlling and representing the
transformation operations.  Because the affine transformations are global in
nature, they can be encapsulated in structures referred to as transformation
matrices, which are applied to each pixel location in an image to compute the
desired transformation effect using matrix multiplication.  This technique is
commonly used in image processing and computer graphics and in our
implementation takes the form of equation \ref{eqn:ir_afm}, which can be
simplified into equation \ref{eqn:ir_afs}.

\begin{equation}
\left[\begin{array}{c}x'\\y'\\1\end{array}\right] = 
\left[\begin{array}{ccc}A & B & E\\C & D &F\\0 & 0 & 1\end{array}\right]
\left[\begin{array}{c}x\\y\\1\end{array}\right]
\label{eqn:ir_afm}
\end{equation}

\begin{equation}
\left[\begin{array}{c}x'\\y'\end{array}\right] = 
\left[\begin{array}{cc}A & B\\C & D\end{array}\right]
\left[\begin{array}{c}x\\y\end{array}\right] + 
\left[\begin{array}{c}E\\F\end{array}\right]
\label{eqn:ir_afs}
\end{equation}

In both equation \ref{eqn:ir_afm} and \ref{eqn:ir_afs}, the symbols {\it x'}
and {\it y'} represent the new pixel locations we wish to compute while {\it x}
and {\it y} represent the current pixel location in the image.  The symbols
{\it A}, {\it B}, {\it C}, {\it D}, {\it E}, and {\it F} are the transformation
factors or coefficients that describe the amount of translation, rotation, and
scaling.  To explain the exact nature of these coefficients and the how matrix
mathematics are used in image transformation, we will further elaborate on what
we mean by translation, rotation, and scaling and how a matrix convolution is
composed to execute these operations.  The bulk of the mathematics regarding
the transformations is derived from the background provided by Bentum, Samsom,
and Slump \cite{bentum}.

\begin{figure}[htp!]
\centering 
\includegraphics[width=4.5in]{../diagrams/translation}
\caption{Translation Example}
\label{fig:ir_translation}
\end{figure}

\subsubsection{Translation}

\paragraph{}A translation transformation involves moving the whole image in a
given {\it x}, {\it y} direction.  In terms of two dimensional space, the image
plane is moved up, down, left, or right (or any combination of these
directions).  Equation \ref{eqn:ir_translation} is the mathematical
representation of the translation operation in the context of our general
affine transformation equation.

\begin{equation}
\left[\begin{array}{c}x'\\y'\end{array}\right] = 
\left[\begin{array}{cc}1 & 0\\0 & 1\end{array}\right]
\left[\begin{array}{c}x\\y\end{array}\right] + 
\left[\begin{array}{c}D_x\\D_y\end{array}\right]
\label{eqn:ir_translation}
\end{equation}

In this equation, the variables $D_x$ and  $D_y$ represent the amount of
displacement in the respective {\it x}, {\it y} directions.  Figure
\ref{fig:ir_translation} demonstrates an example of a translation operation.

\begin{figure}[htp!]
\centering 
\includegraphics[width=4.5in]{../diagrams/rotation}
\caption{Rotation Example}
\label{fig:ir_rotation}
\end{figure}

\subsubsection{Rotation}

\paragraph{}The rotation transformation is slightly more complex than the
translation operation.  In this type of transformation, the image is rotated
clockwise or counterclockwise about a fixed position.  For our image
registration program, we set the fixed position as the center of the image
plane (i.e. {\it ImageWidth/2}, {\it ImageHeight/2}).  This operation requires
the use of trigonometry to calculate the coordinate positions and is
mathematically represented in Equation \ref{eqn:ir_rotation}.  Figure
\ref{fig:ir_rotation} shows an example of rotation.

\begin{equation}
\left[\begin{array}{c}x'\\y'\end{array}\right] = 
\left[\begin{array}{cc}cos(\alpha) & sin(\alpha)\\-sin(\alpha) & cos(\alpha)\end{array}\right]
\left[\begin{array}{c}x\\y\end{array}\right] + 
\left[\begin{array}{c}0\\0\end{array}\right]
\label{eqn:ir_rotation}
\end{equation}

To effectively combine the translation and rotation matrix equations shown in
Equations \ref{eqn:ir_translation} and \ref{eqn:ir_rotation}, we must take the
origin into consideration and adjust our translation factors.  That is, we must
adjust the original $D_x$, $D_y$ variables to account for a translation from
the origin ($O_x$, $O_y$).  As noted before, our system considers the origin to
be the center of the image, so $(O_x, O_y) = (ImageWidth/2, ImageHeight/2)$.
The end result is a combined transformation matrix equation shown in Equation
\ref{eqn:ir_translation_rotation}: 

\begin{equation}
\left[\begin{array}{c}x'\\y'\end{array}\right] = 
\left[\begin{array}{cc}cos(\alpha) & sin(\alpha)\\-sin(\alpha) & cos(\alpha)\end{array}\right]
\left[\begin{array}{c}x\\y\end{array}\right] + 
\left[\begin{array}{c}O_x(1 - cos(\alpha)) - D_xcos(\alpha) + D_Ysin(\alpha) - O_ysin(\alpha)\\O_y(1 - cos(\alpha)) - D_xsin(\alpha) + D_ycos(\alpha) - O_xsin(\alpha)\end{array}\right]
\label{eqn:ir_translation_rotation}
\end{equation}

As can be seen, the right-most vector, which was once simply
$\left[\begin{array}{c}D_x\\D_y\end{array}\right]$, is expanded to be relative
to the origin $(O_x, O_y)$.  When this equation is applied to every pixel in an
image, it will perform the specified translation and rotation operations on the
whole image.

\begin{figure}[htp!]
\centering 
\includegraphics[width=4.5in]{../diagrams/scaling}
\caption{Scaling Example}
\label{fig:ir_scaling}
\end{figure}

\subsubsection{Scaling}

\paragraph{}The final affine transformation our image registration system
considers is scaling, which is used to make objects bigger or smaller.  The
scaling operation can be considered as zooming in and out of the image plane.
Figure \ref{fig:ir_scaling} demonstrates this transformation.  Adding the
scaling operation to Equation \ref{eqn:ir_translation_rotation} yields Equation
\ref{eqn:ir_translation_rotation_scaling}:

\begin{equation}
\left[\begin{array}{c}x'\\y'\end{array}\right] = 
\left[\begin{array}{cc}\frac{cos(\alpha)}{S_x} & \frac{sin(\alpha)}{S_y}\\\frac{-sin(\alpha)}{S_x} & \frac{cos(\alpha)}{S_y}\end{array}\right]
\left[\begin{array}{c}x\\y\end{array}\right] + 
\left[\begin{array}{c}O_x\frac{(1 - cos(\alpha))}{S_x} - D_x\frac{cos(\alpha)}{S_x} + D_y\frac{sin(\alpha)}{S_y} - O_y\frac{sin(\alpha)}{S_y}\\O_y\frac{(1 - cos(\alpha))}{S_y} - D_x\frac{sin(\alpha)}{S_x} + D_y\frac{cos(\alpha)}{S_y} - O_x\frac{sin(\alpha)}{S_x}\end{array}\right]
\label{eqn:ir_translation_rotation_scaling}
\end{equation}

Note, the scaling factors $S_x$ and $S_y$ counter-intuitively appear in the
denominators because the matrix equation is for performing the inverse
transformation.  Since we are working backwards from the original coordinates
to compute the new inverse transformed positions, we divide by the scaling
factors, rather than multiply.  Moreover, while it is possible to have
non-uniform scaling (i.e. $S_x$ is not equal to $S_y$), we only consider the
uniform case in our experiments since non-uniform scaling is rare in real world
data.

Now that we have detailed the construction of a transformation matrix equation
that performs translation, rotation, and scaling, we can return to to Equation
\ref{eqn:ir_afs} and define values for the symbols {\it A}, {\it B}, {\it C},
{\it D}, {\it E}, and {\it F}.  

\begin{table}[htb]
\centering
\begin{tabular}{|r|c|}
\hline
Symbol & Value\\
\hline
A & $\frac{cos(\alpha)}{S_x}$\\
B & $\frac{sin(\alpha)}{S_y}$\\
C & $\frac{-sin(\alpha)}{S_x}$\\
D & $\frac{cos(\alpha)}{S_y}$\\
E & $O_x(1 - A) - D_xA - O_yB + D_yB$\\
F & $O_y(1 - D) + D_yD - O_xC - D_xC$\\
\hline
\end{tabular}
\caption{TRANSFORMATION SYMBOLS}
\label{tbl:ir_symbols}
\end{table}

The mapping from the symbols in Equation \ref{eqn:ir_afs} to Equation
\ref{eqn:ir_translation_rotation_scaling} is summarized in Table
\ref{tbl:ir_symbols}.  In our image registration program, these six symbols are
used to define the transformation coefficients and to configure the
transformation matrix equation.  In terms of implementation, we store and
manipulate the actual $D_x$, $D_y$, $S_x$, $S_y$, and $\alpha$ variables and
present them to the user as the final output of the image registration process.
However, whenever a transformation is required, we compute the six symbols
above using Table \ref{tbl:ir_symbols} and use the calculated values to
configure the transformation operation and thus setup the requested
translation, rotation, and scaling factors in a deterministic and well-defined
manner.  To perform the transformation operation, these configured matrices are
applied to every pixel in the image dataset to form a new {\it Temporary}
image.  

As can be seen, the use of matrix multiplication and transformation matrix
representation is a critical part of our algorithm and is the mathematical
grounding for the image registration process. 

\subsection{Pyramid}

\paragraph{}To algorithmically speed up the image registration process, the
design in this thesis utilizes a pyramid-based optimization scheme.  The idea
behind this technique is that instead of simply running the image registration
process on the original {\it source} and {\it target} images, the program first
performs a registration on a scaled down version of the images to get a rough
estimate of what the transformation should be, and then uses this initial guess
to register proceeding versions of the images until the originals are
processed.

For instance, if the original images are $1024\times1024$, the registration
application would first run a registration on $256\times256$ sized versions of
the images (a quarter-sized reduction).  Since the smaller images are less
detailed due to the fact that they incorporate less pixels and because they
have been smoothed during the scaling, this initial registration is much faster
than processing the original images and produces a coarse estimate of the
actual transformation coefficients.  

\begin{figure}[htp!]
\centering 
\includegraphics[width=4.5in]{../diagrams/pyramid}
\caption{Pyramid-based Image Registration}
\label{fig:ir_py}
\end{figure}

As shown in Figure \ref{fig:ir_py}, this technique can be extended to involve
multiple pyramid levels where each preceeding level is a quarter of the size of
the current.  In our implementation, we chose to limit ourselves to a pyramid
with 4 levels as this was the number of levels used by Th{\'{e}}venaz et.
al\cite{thevenaz}.  After each registration of a pyramid level, the resulting
transformation estimate is fed to the next pyramid registration step.  This
means that the result of the first registration on the smallest images is used
as the starting point for optimization in the second registration of the next
set of images.  This process continues until we reach the final pyramid level,
which is composed of the original images.  The output of this final
registration is the mapping from the {\it source} to the {\it target} images.

The reason for doing this is that instead of doing many slower iterations of
the image registration on the original images, we can perform many fast
iterations on the smaller pyramid levels first.  Only when we are relatively
close to the final transformation coefficients and thus have converged towards
the solution, do we perform iterations on the original images to produce the
final result.  This coarse-to-fine grained optimization method reduces the
total number of iterations involving the large original images and thus speeds
up our performance.

\subsection{Interpolation}

\paragraph{}In image processing there are a multiple ways to perform
interpolation (calculate the pixel intensity at location $(x, y)$).  For the
implementation used in this thesis, we focus on two popular techniques:
bilinear interpolation and bicubic interpolation.  These two methods differ in
the number of neighboring pixels required for the interpolation and the
mathematical weights used to compute the final intensity.

\begin{figure}[htp!]
\centering 
\includegraphics[width=2.0in]{../diagrams/bilinear_thesis}
\caption{Bilinear Interpolation}
\label{fig:ir_bilinear}
\end{figure}

\subsubsection{Bilinear Interpolation}

\paragraph{}For bilinear interpolation, we require the immediate four
surrounding pixel values.  In Figure \ref{fig:ir_bilinear}, each square in the
diagram represents a pixel location and value in the image.  The dark square
represents the target pixel location determined by the transformation function,
while the lighter boxes represent surrounding pixel values.

In most cases, the target location generated by the transformation component is
some fractional position such as $(x, y) = (1.24, 1.35)$.  Since the pixel
intensities are only defined at integer positions such as $(1, 1)$ or $(1, 2)$,
we must perform some calculation to determine the pixel intensity of the
factional transformed pixel location.  In bilinear filtering, this is done by
performing a weighted average of the neighboring four pixel intensities using
the following formula:

\begin{align}
i_x &= floor(P_x)\\
i_y &= floor(P_y)\\
d_x &= P_x - i_x\\
d_y &= P_y - i_y\\
I_{UL}(x, y) &=  d_x \times d_y \times I(i_x, i_y)\\
I_{UR}(x, y) &= (1 - d_x) \times d_y \times I(i_x + 1, i_y)\\
I_{LL}(x, y) &= d_x \times (1 - d_y) \times I(i_x, i_y + 1)\\
I_{LR}(x, y) &= (1 - d_x) \times (1 - d_y) \times I(i_x + 1, i_y + 1)\\
I(x, y) &= I_{UL}(x, y) + I_{UR}(x, y) + I_{LL}(x, y) +I_{LR}(x, y)\label{eqn:ir_bilinear}
\end{align}

In the formula above, $P_x, P_y$ represents the {\it x} and {\it y} portions of
the target pixel location, while $i_x, i_y$ are the integer components and
$d_x$, $d_y$ represent the {\it x} and {\it y} fractional components.  For
instance, if the target pixel location $(x, y) = (1.24, 1.35)$, then $P_x =
1.24$, $P_y = 1.35$, $i_x = 1$, $i_y = 1$, $d_x = 0.24$, and $d_y = 0.35$.  The
equations for $I_{UL}$, $I_{UR}$, $I_{LL}$, and $I_{LR}$ calculate the weighted
intensities for the upper left, upper right, lower left, and lower right
neighboring pixels respectively.  Note, the pixel intensities are being fetched
from the source image (i.e. the input image we are transforming), and not the
current image we are generating.  The total intensity value for the target
pixel at $(x, y)$ is the summation of all of the weighted intensities as shown
in Equation \ref{eqn:ir_bilinear}.  

This formula is applied to all of the target pixel locations generated by the
transformation module to compute the pixel intensities in the output image.

\label{sec:ir_bicubic}
\subsubsection{Bicubic Interpolation}

\paragraph{}Bicubic interpolation requires more pixels and more complicated
mathematics than bilinear interpolation.  Rather than only using the immediate
four neighbors, the bicubic method requires a halo of sixteen pixel intensities
as shown by the dark and lightly shaded squares in Figure \ref{fig:ir_bicubic}.
Additionally, instead of using simple linear weighted averages to compute the
interpolated pixel value, bicubic interpolation employs more complex cubic
equations to calculate the output intensities.  The bicubic interpolation
method utilizes the following formula:

\begin{align}
C(t, I_0, I_1, I_2, I_3) &= I_0 \times (-(t^3  + t)/2 + t^2)\\
			 &+ I_1 \times ((3t^3 - 5t^2)/2 + 1)\\
			 &+ I_2 \times ((-3t^3 + t)/2 + 2t^2)\\
			 &+ I_3 \times ((t^3 - t^2)/2)\label{eqn:ir_cubic}\\
C_0(x, y) &= C(d_y, I(i_x - 1, i_y - 1), I(i_x - 1, i_y), I(i_x - 1, i_y + 1), I(i_x - 1, i_y + 2)\\
C_1(x, y) &= C(d_y, I(i_x, i_y - 1), I(i_x, i_y), I(i_x, i_y + 1), I(i_x, i_y + 2)\\
C_2(x, y) &= C(d_y, I(i_x + 1, i_y - 1), I(i_x + 1, i_y), I(i_x + 1, i_y + 1), I(i_x + 1, i_y + 2)\\
C_3(x, y) &= C(d_y, I(i_x + 2, i_y - 1), I(i_x + 2, i_y), I(i_x + 2, i_y + 1), I(i_x + 2, i_y + 2)\\
I(x, y) &= C(d_x, C_O(x, y), C_1(x, y), C_2(x, y), C_3(x, y))\label{eqn:ir_bicubic}
\end{align}

In the formula above, Equation \ref{eqn:ir_cubic} computes the 1-D cubic
interpolation of the four input intensities $I_0$, $I_1$, $I_2$, and $I_3$,
where $t$ represents the fractional offset between $I_1$ and $I_2$.  This
function is called four times to compute the cubic of each of the four columns
in our $4\times4$ neighborhood of pixels and stored in the variables $C_0$,
$C_1$, $C_2$, and $C_3$ as shown above.  The final pixel intensity value for
our target position is calculated by performing the cubic interpolation of each
of the cubic interpolated columns as shown in Equation \ref{eqn:ir_bicubic}.

\begin{figure}[htp!]
\centering 
\includegraphics[width=2.0in]{../diagrams/bicubic_thesis}
\caption{Bicubic Interpolation}
\label{fig:ir_bicubic}
\end{figure}

As can been seen, a series of five 1-D cubic interpolations are performed to
compute the 2-D cubic interpolation.  First, each of the columns are
interpolated, and then these output values ($C_0$, $C_1$, $C_2$, $C_3$) are
used to perform a row-wise 1-D cubic interpolation.  This final interpolation
is the pixel value of our target location $(x, y)$.  The source of this
construction and formulation of bicubic interpolation is the book, ``2-D and
3-D Image Registration,'' by A. Ardeshir Goshtasby \cite{goshtasby}.

The advantage of using a bicubic interpolation scheme over a bilinear one is
that the former tends to provide smoother (and thus usually, but not always,
more accurate) outputs.  Unfortunately, because of the larger neighborhood of
pixels required and additional mathematical complexity, bicubic interpolation
implementations are usually significantly slower than bilinear versions and
thus the technique is normally avoided in image processing unless extreme
accuracy is absolutely required.  For our project, we consider both methods for
our GPU implementation, but only employ the bicubic version for the CPU
implementation.  

\label{sec:ir_ec}
\subsection{Error Calculation}

\paragraph{} While there are a variety of methods to perform similarity
measurement, this project determines the similarity between two images by
calculating the mean squared error (MSE) metric defined by following formula:

\begin{equation}
\text{MSE} = \frac{\sum\sum{(\text{Temporary}(x, y) - \text{Target}(x, y))^2}}{\text{NumberOfPixels}}
\end{equation}

\noindent where $\text{Temporary}(x, y)$ and $\text{Target}(x, y)$ are
intensity values in the {\it temporary} and {\it target} images at coordinates
$(x, y)$, and $\text{NumberOfPixels}$ is the total size of one image.  This
means that the lower the mean square error, the more similar the two images
are.  Thus, the final result of the similarity measurement should be a
relatively low number around zero.  For our implementation, we used a tolerance
level of $0.01$ as the limit, which is considered a reasonable level of
accuracy.

\subsection{Optimize}

\paragraph{}Because the transformation, interpolation, and error calculation
components of the image registration workflow are the most computationally
intensive routines, we decided to use an external optimization implementation
and concentrate of those critical bottlenecks.  This means that rather than
programming our own optimizer, we employed the Nelder-Mead Simplex optimizer
from the GNU Scientific Library\cite{gsl} for the optimization component and
focused our development on accelerating the transformation, interpolation, and
error calculation portions of the application.

The Nelder-Mead Simplex algorithm is a simple optimizer that only requires a
vector of parameters and a cost function.  In the case of our implementation,
the vector of parameters is our set of transformation coefficients, and the
cost function is the out of our mean square error calculation.  For each
iteration of the Simplex algorithm, the optimizer attempts to improve the
vector of parameters by performing geometric transformation such as reflection,
expansion, and contraction.  Using these linear programming techniques, the
Simplex method attempts to move through the parameter space towards a minimum.
As noted in the previous section, we set this error limit to $0.01$.  Once this
limit is reached, the optimizer returns with the updated set of transformation
coefficients.

Although it is not the fastest or most sophisticated optimization algorithm,
the Nelder-Mead Simplex optimizer was chosen because of its availability in the
GNU Scientific Library, and because it only required the transformation
coefficients and the mean square error calculation as inputs.  Many other
algorithms, including some of the more advanced optimizers in the GNU
Scientific Library require computing gradients or hessians, but we decided to
avoid such complications and simply focus on the core performance portions of
the image registration algorithm.  As will be shown in the proceeding sections,
the optimization portion of the image registration workflow only occupied a
small fraction of the total running time.

%-------------------------------------------------------------------------------
% vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
