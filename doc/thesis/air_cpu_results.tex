%-------------------------------------------------------------------------------
% air_cpu_results.tex:
%-------------------------------------------------------------------------------

\chapter{CPU RESULTS AND ANALYSIS}
\label{ch:cpu_results}

%-------------------------------------------------------------------------------

\section{Implementation and Test Environment}

\paragraph{}

The image registration algorithm was first implemented in $C$ on a host system
featuring a Intel Quad-Core Q6700 2.66 GHz CPU and 8.0 GB of RAM and running
Ubuntu Linux 8.04 (kernel 2.4.20).  To compile the program codes we used the
GNU Compiler Collection (GCC) 4.1.2. 

Table \ref{tbl:images} contains a complete listing of the images we used to
test both the CPU and GPU implementations.  These image files are stored as
8-bit grayscale images in the Netpbm grayscale image format (PGM).  For each of
these images, we registered the original source version of the images against
transformed target images.  To generate these test images, we applied various
amounts of rotations, translations, and scaling to the original source image
and generated a set of transformed target images.  In total we ran ten complete
registrations for each transformation permutation of each image set. 

Overall, the CPU implementation followed the design described in the previous
chapter without modifications and so the specifics of the CPU version will not
be discussed other than to mention that we performed a side experiment with the
compiler optimization flags.  As with most compilers, GCC allows for a variety
of compiler optimizations and we decided to test levels {\it 0} through {\it 3}
to investigate what performance increases are possible from just compiler
optimizations.  

\begin{table}[htb!]
\centering
\begin{tabular}{|c|V|c|}
\hline
Name& Image & Dimensions (Pixels)\\
\hline
lenna	& \includegraphics[width=1.0in]{../diagrams/lenna} & $512\times512$\\
\hline
ndbuntu & \includegraphics[width=1.0in]{../diagrams/ndbuntu} & $768\times768$\\
\hline
halo	& \includegraphics[width=1.0in]{../diagrams/halo} & $1024\times1024$\\
\hline
jump	& \includegraphics[width=1.0in]{../diagrams/jump} & $1536\times1536$\\
\hline
victory & \includegraphics[width=1.0in]{../diagrams/victory} & $2048\times2048$\\
\hline
crabnebula & \includegraphics[width=1.0in]{../diagrams/crabnebula} & $3072\times3072$\\
\hline
\end{tabular}
\caption{LISTING OF TEST IMAGES}
\label{tbl:images}
\end{table}

Moreover, we also profiled our experimental test runs to determine the
bottlenecks and critical sections of the CPU implementation.  By identifying
the computationally intensive portions of the run-time, this profiling analysis
allowed us to focus on the most important parts of the image registration
workflow in our construction of the GPU version.

The remainder of this chapter, then, focuses on the effect compiler
optimizations levels have on execution time and provides a performance and
accuracy analysis along with a profiling of the functional components of the
CPU implementation.  At the end of the chapter we will have established a clear
baseline standard for performance and accuracy and will have a better
understanding of the performance critical bottlenecks in the image registration
workflow.

%-------------------------------------------------------------------------------

\section{Compiler Flags}

\paragraph{}Before porting the image registration program to the GPU, we first
experimented with the optimization levels provided by the GCC compiler to
determine which level provided the best performance for our image registration
application.  The particular optimization levels considered were as follows:

\begin{itemize}

\item{\bf O0:} This first level actually involves no compiler optimizations.

\item{\bf O1:} This second level turns on many common optimizations that
attempt to reduce the output code size and the execution time.  However, it
avoids any optimizations that may greatly increase the compilation time.

\item{\bf O2:} The third optimization level turns on almost all supported
optimizations that do not require a space-speed tradeoff and includes
techniques such as instruction scheduling and reordering, alignment of
functions and loops, and a second peephole pass.

\item{\bf O3:} The final optimization level performs all of the optimizations
in the previous level along with a few more advance optimizations such as
function inlining, and auto-vectorization of loops.

\end{itemize}

For a more in-depth listing of the specific optimizations at each level please
refer to the description in the Optimize Options section of the GCC manual
\cite{gcc}.

%-------------------------------------------------------------------------------

\label{sec:cpu_results_performance}
\section{Performance}

\paragraph{}In our initial CPU experiments, we performed all of the image
registrations on the test image sets described earlier using each of the
compiler optimization levels mentioned above to determine the most effective
level which would later be used to compare with our GPU implementation.   The
following plots and charts will explore the performance characteristics of the
CPU image registration system with varying degrees of compiler optimization.

\begin{figure}[htb!]
\centering
\includegraphics[width=4.50in]{../plots/thesis_all_images_cpu_vs_runtime}
\caption{Average running time for all CPU levels for each test image sets}
\label{fig:thesis_all_images_cpu_vs_runtime}
\end{figure}

Figure \ref{fig:thesis_all_images_cpu_vs_runtime} shows average running time of
all the CPU levels for each of the test image sets.  As would be expected, the
smaller images such as {\it lenna} took less time to complete the registration
process than larger ones such as {\it crabnebula}.  It is clear from the plot
that the second level of optimization {\tt O1} greatly improves the performance
of the application.  By simply activating the first level, the running time is
cut roughly in half for the larger images.  For the smaller images, this
performance increase is less, but still evident.  Subsequent optimization
levels {\tt O2, 03} further reduce the amount of running time, but to a much
lesser extent.  

\begin{figure}[htb!]
\centering
\includegraphics[width=4.50in]{../plots/thesis_all_cpu_size_vs_runtime}
\caption{Average running time for all CPU levels as number of image pixels increases}
\label{fig:thesis_all_cpu_size_vs_runtime}
\end{figure}

Figure \ref{fig:thesis_all_cpu_size_vs_runtime} provides another view of the
same experimental results.  In this diagram, the lines represent the running
time of each CPU optimization level as the size of the image increases (where
$\text{megapixels} = 1024\times1024$ $\text{pixels}$).  It is clear from this
graph that {\tt O0} produces the slowest application, while {\tt O3} produced
the fastest.  This is to be expected since the former utilizes no
optimizations, while the latter performs full optimizations.  Moreover, the
difference between {\tt O2} and {\tt O3} is quite minimal and less evident as
both appear to be the same line in the graph.

These results suggest that any level of optimization is an improvement over the
baseline {\tt O0} optimization level where no optimizations are performed.
Moreover, subsequent optimization levels do in fact increase image registration
performance, but not by a drastic amount, while negatively impacting
compilation time (i.e. increases build time due to the use of sophisticated
optimization algorithms).  Since build time is not a primary concern and
maximum performance is our goal, we will use the {\tt O3} compiler optimization
level to build the CPU version used to compare our GPU implementation.

%-------------------------------------------------------------------------------

\label{sec:cpu_results_accuracy}
\section{Accuracy}

\paragraph{}To measure the quality of the image registrations we calculated the
mean square error between the image produced by the registration and the actual
{\it target} image as explained in Section \ref{sec:ir_ec}.  The MSE was then
used to compute the Peak-Signal-to-Noise Ratio ($\text{PSNR}$).  As explained
in \cite{obukhov}, the $\text{PSNR}$ is commonly used to evaluate image
reconstruction quality.  Generally values between $30$ and $50$ denote high
fidelity, while anything above $50$ denotes that the results are nearly
identical.  The $\text{PSNR}$ was computed using the following formula:

\begin{equation}
\text{PSNR} = 10*\text{log}_{10}(\frac{\text{MaxIntensity}^2}{\text{MSE}})
\end{equation}

\noindent where $\text{MaxIntensity}$ is the maximum pixel intensity value.
Since we used portable grayscale pixmaps to store our image data, this value
was $255$.  Generally, the higher the Peak-Signal-to-Noise-Ratio, the more
accurate the registration process was, and thus the better the results.

\begin{figure}[htb!]
\centering
\includegraphics[width=4.50in]{../plots/thesis_all_cpu_images_average_psnr}
\caption{Average Peak-Signal-to-Noise Ratio of each CPU optimization level for all images}
\label{fig:thesis_all_cpu_images_average_psnr}
\end{figure}

As can be seen in Figure \ref{fig:thesis_all_cpu_images_average_psnr}, the
resultant $\text{PSNR}$ levels from the CPU image registration program are
relatively high.  For all of the images except for {\it halo}, the CPU version
yields a $\text{PSNR}$ above $50$, which means the resulting transformation
coefficients produces an image nearly identical to the target.  Even the
$\text{PSNR}$ result for {\it halo} is within the bounds for high fidelity.
Moreover, the chart also reveals that the difference in optimization levels
produces no noticeable effect in the level of accuracy.  The $\text{PSNR}$
levels across all optimization levels for a particular image remained the same.
Because of this, it is safe for us to only use the {\tt O3} version of the CPU
implementation to compare against the GPU version.

%-------------------------------------------------------------------------------

\label{sec:cpu_profiling}
\section{Profiling}

\paragraph{}The final portion of the CPU experiments was to profile the
execution of the CPU implementation to determine the bottlenecks and most time
critical sections of the image registration workflow.

The stacked cluster diagram in Figure \ref{fig:thesis_cpu_profiling} and the
data in Table \ref{tbl:cpu_profiling} show the profiling results of the
smallest ({\it lenna}) and largest {\it crabnebula}) images for the various
levels of CPU optimizations.  As the diagram and table show, the transform
component of the image registration workflow absolutely dominates the running
time.  This is to be expected as a transformation and interpolation must be
performed for each pixel in the image and thus would take up the bulk of the
computation time.  Likewise, the mean square error calculation also plays a
significant role and is the second most time intensive portion of the image
registration pipeline.  

Furthermore, the relative amount of time spent in pyramid construction and
everything else is so insignificant that these components are not visible in
Figure \ref{fig:thesis_cpu_profiling} at all.  As shown in Table
\ref{tbl:cpu_profiling}, the pyramid construction and everything else segments
of the program only total around 4\% of the total running time.  This
reinforces the conclusion that the transformation and error calculation
components are the critical bottlenecks in the CPU implementation of image
registration.

\begin{figure}[htb!]
\centering
\includegraphics[width=4.50in]{../plots/thesis_cpu_profiling}
\caption{Profiling of the CPU versions of {\it lenna} and {\it crabnebula}}
\label{fig:thesis_cpu_profiling}
\end{figure}

\begin{table}[htb!]
\centering
\begin{tabular}{|r|r|r|r|r|}
\hline
& \multicolumn{4}{c|}{\it lenna}\\
\hline
{\bf Component}& {\bf O0} & {\bf O1} & {\bf O2} & {\bf O3} \\
\hline
Pyramid Construction	&  0.08\% &  0.07\% &  0.08\% &  0.08\%\\ 
Transform		& 95.94\% & 95.96\% & 95.95\% & 95.95\%\\
Calculate MSE		&  3.83\% &  3.84\% &  3.83\% &  3.82\%\\
Everything Else		&  0.15\% &  0.13\% &  0.14\% &  0.15\%\\ 
\hline
\hline
& \multicolumn{4}{c|}{\it crabnebula}\\
\hline
{\bf Component}& {\bf O0} & {\bf O1} & {\bf O2} & {\bf O3} \\
\hline
Pyramid Construction	&  0.07\% &  0.07\% &  0.07\% &  0.07\%\\ 
Transform		& 95.94\% & 95.93\% & 95.94\% & 95.94\%\\
Calculate MSE	        &  3.84\% &  3.85\% &  3.84\% &  3.85\%\\ 
Everything Else		&  0.15\% &  0.15\% &  0.15\% &  0.14\%\\
\hline
\end{tabular}
\caption{CPU IMPLEMENTATION PROFILING}
\label{tbl:cpu_profiling}
\end{table}

The results in the diagram and table also manifest that the relative running
time percentage of each of the image registration components remain relatively
constant as the size of the image increases.  There is little to no difference
between the profiling for {\it lenna} and {\it crabnebula} image sets as the
experimental results show that the percentage of running time occupied by each
of the components remain about the same throughout the tests.  

Moreover, changing the CPU optimization level also does not have much if any
effect on the execution profiling.  This means that the relative amount of time
occupied by the various image registration components (transformation, error
calculation, etc.) remain constant regardless of image size and CPU
optimization level.

As noted previously, we proposed focusing only on accelerating the
transformation and error calculation components of the image registration
workflow since they appeared to be the most computationally intensive, while
leaving the optimizer to an external library.  The results in the above
profiling support this direction as the transformation and error calculation
components take up more than 97\%  of the running time and thus would provide
the biggest opportunity for speedup.

%-------------------------------------------------------------------------------
% vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
