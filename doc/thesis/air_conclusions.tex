%-------------------------------------------------------------------------------
% air_conclusions.tex: 
%-------------------------------------------------------------------------------

\chapter{CONCLUSIONS}

\paragraph{}

Overall, the study presented in this thesis presents a thorough performance
analysis of our CUDA 2-D rigid image registration implementation.  The
investigation reveals some of the problems encountered in the converting the
CPU implementation to the CUDA framework and identifies key techniques and
practices to overcoming these obstacles.  Likewise, we also examine some of the
limiting factors that prevent obtaining maximum performance on the CUDA device
and discuss a few recommendations on how to not only speed up the image
registration application, but also to improve the CUDA platform as a whole.  

In particular, we highlight the need to manage the computational and memory
resources on the graphics processing unit carefully to obtain performance
increases and propose the following guiding principles:

\begin{enumerate}

\item{\bf Minimize kernel calls and memory transfers:}\\This means that one
should encapsulate as much functionality as reasonable in a single kernel to
avoid invoking many kernels while limiting the number of data transfers between
the host and the GPU.  In our project, this was accomplished by performing the
transform, interpolation, and error calculation all in one CUDA kernel and by
computing the partial sum of the mean square errors.  This meant we only had to
invoke a single kernel to perform our computation and that we greatly reduce
the amount of data transferred between the host and device.  Furthermore, by
constructing the pyramid levels on the GPU and storing them there, we avoided
having to send each level from the CPU to the GPU every time we moved from one
level to another.

\item{\bf Minimize global memory accesses:}\\This principle means that careful
use of the memory hierarchy on the GPU device is critical for performance
increases.  In particular, global memory should be avoided unless absolutely
necessary.  For instance, constant memory provides fast access to a limited
number of data to each thread and is cached, while texture memory allows for
cached reads to larger data sets.  Additionally, shared memory should be used
to collect data before writing to global memory.  All of these techniques were
employed in our GPU implementation and allowed us to achieve the performance
gains demonstrated earlier in the results section.

\end{enumerate}

In addition to formulating these guidelines for achieving maximum performance,
we also provided in-depth profiling though the use of run-time analysis and
micro-benchmarks.  The profiling revealed the time critical sections of the
code that we should focus our efforts on and helped in understanding the
limitations and obstacles presented by the CUDA platform.  In the same fashion,
the micro-benchmarks aided in further investigating the performance
characteristics of the CUDA device and system and confirmed many of the
specified properties of the graphics processing unit.  More importantly, the
device startup cost micro-benchmark revealed a costly and performance limiting
obstacle in the CUDA system.

As mentioned in the Chapter \ref{ch:future}, there are a variety of
improvements possible and areas for future consideration such as implementing
different algorithms or extending the application to perform registration on a
stream of images.  Likewise, more in-depth comparisons with existing
implementations or competing acceleration platforms such as FPGAs would also be
interesting.  Overall, however, the project has been successful in
accomplishing the goal of accelerating the image registration program by
porting key computationally intensive portions of the workflow to a graphical
processing unit.  

In summary, our CUDA implementation of a 2-D rigid image registration system
was able to achieve up to $90\times$ speedup with the bilinear kernel and
$33\times$ with the bicubic version, demonstrating the viability of using CUDA
as a acceleration platform and a successful accelerated image registration
system.

%-------------------------------------------------------------------------------
%vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
