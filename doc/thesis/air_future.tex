%-------------------------------------------------------------------------------
% air_future.tex: 
%-------------------------------------------------------------------------------

\chapter{FUTURE WORK}
\label{ch:future}

%-------------------------------------------------------------------------------

\section{Improvements to the Image Registration Implementation}

\paragraph{}

We would like to increase the performance of our implementation by moving
remaining portions of the algorithm to the GPU, improving the current CUDA
kernels, extending the functionality of the image registration application and
investigating alternative optimization algorithms.  To get a better idea of how
the CUDA platform compares to the traditional OpenGL and DirectX methods, it
may be prudent to implement a version of the image registration algorithm using
the older GPGPU techniques and test it on the Tesla C870.  

Likewise, it would also be interesting to test our implementation against other
CPU implementations that use different algorithms to see how our version stands
up to other image registration applications.  However, it should be noted that
the core focus of this thesis is to understand the principles involved in
porting a CPU implementation of an algorithm to a graphics processing unit and
achieving a performance increase.

In terms of improving our current CUDA code, we may want to try to reduce the
memory bottleneck in the bicubic kernel.  As of this moment, our bicubic kernel
performs 16 texture references to perform a precise bicubic interpolation.  It
is possible to take advantage of the bilinear texture hardware to perform an
imprecise but fast higher order interpolation with less memory accesses as
explained by Sigg and Hadwiger \cite{sigg}.  This would perhaps close the
performance gap between the bicubic and bilinear kernels, while maintaining a
high level of accuracy.

Additionally, one possible means of amortizing the startup cost of the CUDA
device is if we considered an image registration system that worked on a stream
of input images such as from a video source or a time lapsed camera.  In this
instance, we would be interested in the registration of a series of related
images.  Such an application could perhaps pre-fetch the images into memory
ahead of time and possibly take advantage of the use of multiple streams in
CUDA to further increase concurrency and parallelism.  Although this would not
reduce the amount of latency for one registration, it would improve the overall
throughput of the system.

Another possible improvement would be to revisit the mean square error
calculation and fully implement it with a parallel prefix algorithm, although
the payoff for this seems limited as indicated by our profiling data.
Currently, we only do one level of parallel summation in calculating the mean
square errors and a small speedup would be possible if we performed the whole
calculation on the GPU.  As manifested in the profiling, however, the running
time of the image registration is not really dominated by this function, so the
total amount of speedup is not significant.  Moreover, the parallel prefix
summation of a large amount of data such as that in the larger images can
become quite complex and requires careful memory layout to avoid bank conflicts
and to perform optimal memory access patterns.

Rather than using the mean square error metric, it may be beneficial to
consider alternative similarity metrics and cost functions or even the
combination of such similarity measurements to provide a more accurate
optimization metric.  A more accurate metric could lead to an overall reduction
in the number of transformations and thus increase the speed of our image
registration system.

Further performance improvements can come from fine-tuning the programming
environment itself.  For instance, it is possible that the fixed start-up cost
manifested in our results could be due to the the manner in which the CUDA
device drivers are loaded.  In our test environment, the NVIDIA Tesla GPU runs
on its own board as a co-processor and is not used to drive any active display.
This means that every time the image registration program is ran that the
operating system must load all of the NVIDIA and CUDA drivers if they are not
already in memory.  It would be interesting to see what the startup times would
be on a system that already had the NVIDIA and CUDA drivers loaded (such as a
system that has both the Tesla and a regular NVIDIA GPU as the active display
driver).

Along the same vein, it would be interesting to experiment with the CUDA
implementation on a more recent version of the NVIDIA SDK and to run some tests
on more recent graphics cards.  This would allow us to test how well the code
scales across different graphic processing units and possible expose different
problems or obstacles.  More significantly, moving to a more recent GPU and SDK
would allow us to experiment with double precision floating point.  Although
performance would drop in moving from single precision to double precision, it
would be interesting to see if accuracy improves, particularly as image size
increases.

Finally, we can further improve our implementation by using a more advanced
optimizer.  We mainly chose the Simplex optimizer because it was readily
available in the GNU Scientific Library, allowing us to focus on the CUDA
portion of the application.  However, previous research studies have performed
steepest descent and gradient optimizers on the GPU and these could be viable
candidates for porting to CUDA.  Perhaps, even the Marquardt-Levenberg
algorithm \cite{thevenaz} would be a viable candidate for implementation.  The
reason to explore alternative optimization algorithms is that these more
advanced optimizers can perform the minimizations (i.e. transformation
estimations) in less iterations than the Nelder-Mead Simplex algorithm and thus
algorithmically reduce the running time of the image registration application.

%-------------------------------------------------------------------------------

\section{Improvements to the CUDA Environment}

\paragraph{}

In developing this application, it became quite apparent that the key to
achieving the most performance from the CUDA device is to effectively manage
the program's memory access patterns.  Developers must minimize the amount of
global memory accesses and take full advantage of the texture, constant, and
shared memories.  Our initial naive CUDA implementation simply passed the
transformation matrix in as kernel parameter and we were only able to achieve
$3\times$ to $8\times$ speedup on all of the test images.  It was only after we
reorganized our memory access pattern in our final implementation did we get
drastically improved performance.  

Although graphic processing units offer a vast amount of computational power,
they still face the age old problem of the \textit{memory wall}: meaning there
are cases where the processors idle because data cannot be streamed in fast
enough.  In the case of image registration, because of the nature of
interpolation, it is difficult to structure accesses that benefit from memory
coalescing (reading a coherent block of memory with a block of threads) and
thus the processors are required to wait for data to be fetched.
Unfortunately, GPUs are designed for high throughput and not low latency, so
these memory accesses, particularly to global memory, are quite costly (several
magnitudes slower).  It is up to the programmer to effectively group the data
access patterns and take advantage of the available memory resources.  

In regards to programming in CUDA, there are a variety of things NVIDIA can do
to improve the platform.  For instance, a profiling tool that can provide
timing information amount the memory access patterns would be quite beneficial
for developers.  It would allow programmers to structure their programs to take
full advantage of the GPU's memory hierarchy which, as shown in this study, is
vital to achieving full performance.  Although the version of CUDA includes
some basic profiling information such as processor occupancy, it does not
provide enough information to determine if the stream processors' resources are
being used effectively and efficiently.  Additional profiling and debugging
tools may be available in more recent versions of the software development kit
or from external third parties, but we are not currently aware of them and thus
did not not employ them.

Moreover, NVIDIA should consider exposing more of the built-in hardware through
the CUDA API.  For instance, although the GPU contains mipmap hardware units to
provide support for texture scaling, this feature is only exposed through the
traditional graphics programming interfaces (OpenGL and DirectX).  This would
allow us to implement pyramid construction using hardware accelerated functions
rather than having to write our own scaling kernel and parallel-prefix
reduction routines.  One possible means of accomplishing this is by utilizing
the existing OpenGL bridge included CUDA to provide a friendly interface to the
more graphics specific hardware functions in a more general manner.

Likewise, some of the application programming interface needs to be refined or
improved.  For instance, currently it is not possible to have an array of
texture references, nor is it possible to pass in a texture reference to a
kernel thus forcing messy kludges to select the appropriate textures.  These
last two issues were particularly relevant to our implementation of the pyramid
component where we had to implement our own parallel reduction and had to
perform a conditional check to determine if we should store a \textit{source}
or \textit{target} image.  

Finally, although CUDA is relatively accessible programming framework, general
programmability would be greatly improved with the introduction of general
recipes or abstractions.  For instance the pyramid construction function is
basically a parallel-prefix reduction and can be generalized to incorporate
different types of binary operators.  Some work on such abstractions are
already underway in project such as Mars \cite{he}, which implements the
MapReduce abstractions in CUDA.  Such high level programming constructs would
enable for rapid development and increase programming accessibility.

Although addressing some of these issues may not lead to huge performance
gains, it would increase programmer productivity and making utilizing NVIDIA's
CUDA platform easier.  Reducing the complexity of the programming framework and
reducing other barriers to entry are key to encouraging more CUDA development
and building GPU accelerated applications.

%-------------------------------------------------------------------------------
% vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
