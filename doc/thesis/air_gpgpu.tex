%-------------------------------------------------------------------------------
% air_gpgpu.tex: 
%-------------------------------------------------------------------------------

\chapter{GPGPU IMPLEMENTATION}

%-------------------------------------------------------------------------------

\section{CUDA Architecture and Programming Model}

\paragraph{}As noted in the introduction, NVIDIA's Compute Unified Device
Architecture (CUDA) framework is used in this thesis to take advantage of the
graphical processing unit in the host machine to accelerate our image
registration application.  Before examining the details of the GPGPU image
registration implementation, it is first necessary to provide an overview of
the CUDA architecture and programming model.  

In CUDA, the programmable units of the GPU expose a single-program
multiple-data (SPMD) programming model.  The user passes a program to the
device in the form of a kernel.  The GPU in turn processes many elements
(threads) in parallel using the specified kernel on each thread.  This is
similar to the traditional single instruction, multiple data (SIMD) model
provided by Intel's SSE and MMX CPU instructions.  CUDA, however, allows for
limited branching within the kernel, permitting different elements to take
divergent code paths and provides a much broader instruction set.  These
threads are organized into {\it warps} or groups of 32 parallel scalar threads,
where each {\it warp} executes one kernel instruction at a time.

\begin{figure}[htb]
\centering
\includegraphics[width=4.50in]{../diagrams/smsp}
\caption{CUDA Stream Multiprocessors}
\label{fig:gpgpu_smsp}
\end{figure}

The user programmable kernels are executed by an array of concurrent stream
processors (SPs) which support 32-bit integer and single-precision floating
point operations.  These SPs are clustered together in groups of 8 to form a
single stream multi-processor (SM) core with each SP executing one thread as
shown in the simplified architectural diagram in Figure \ref{fig:gpgpu_smsp}.
The SPs within a SM share a local store referred to as shared memory.
Collections of {\it warps} are known as {\it thread blocks} and these threads
all run on the same MP and share a part of the local store.  The number of
warps in such a {\it thread block} is defined by the user when calling the
kernel.

\begin{figure}[htb]
\centering
\includegraphics[width=4.50in]{../diagrams/cuda_memory_organization}
\caption{CUDA Memory Organization}
\label{fig:gpgpu_cuda_memory_organization}
\end{figure}

In addition to the shared memory, the CUDA programming environment exposes a
limited memory hierarchy in the form of registers, constant memory, global
memory, and textures as shown in Figure
\ref{fig:gpgpu_cuda_memory_organization}.  Overall, the register file is the
fastest data storage unit in this on-chip hierarchy but only supports a limited
amount of space (32-64KB).  After this, shared memory is provided that is just
as fast as the registers, but smaller (16KB).  Similarly, Constant memory is a
subset of device memory (64KB) that is shared by all the SPs and cannot be
modified at run-time by the device, but can be configured by the host.  This
portion of memory provides fast read-only cached access.

Other device memory is generally grouped as global memory that permits both
read and write operations from all threads, but is uncached and has long
latencies (up to 400 cycles).  The amount of global memory available depends on
the specific hardware configuration of the GPU.  Texture memory is also a large
subset of the device memory similar to the constant memory in that it provides
read-only cached access to data stored on the device, but has latencies similar
to global memory accesses.  Moreover, it allows addressing through a
specialized texture unit that allows for filtering and clamping.

Accelerating an application using CUDA, then, requires identifying program
bottlenecks and mapping the application into this programming model.  This
involves careful structuring of the user defined kernels to ensure that all the
SPs are executing at peak performance and strict management of data access
patterns to fit into the limits imposed by the memory hierarchy.  For data
intensive programs such as image registration, this latter issue of memory
management is key to obtaining good speedup.

In the remainder of this chapter, we will first discuss the design of the GPGPU
image registration algorithm and explain the differences between this new
design with the standard CPU algorithm.  Next, we provide a thorough
explanation about how the GPGPU algorithm is implemented in the CUDA framework,
emphasizing a few core principles that were applied to maximize performance and
efficiency.  Finally, we briefly summarize the construction of a few
micro-benchmarks that were used to test and analyze the final implementation.

%-------------------------------------------------------------------------------

\section{GPGPU Image Registration Algorithm}

After profiling the CPU version as demonstrated in Section
\ref{sec:cpu_profiling}, it was observed that the image generation and
similarity measurement components composed $95-99\%$ of the run-time and thus
were prime targets for acceleration.  Since both procedures involve scanning
the image pixels and performing the same computation for each element there is
a high level of data parallelism.  This means that since the processing of one
pixel data is not dependent on procedural output of another, all the pixels can
be scanned and processed concurrently without need for synchronization. Because
of this data parallelism both image registration components easily map into the
GPGPU programming model, making the GPU a suitable platform for accelerating
these functions.  

\begin{figure}[htb]
\centering
\includegraphics[width=4.50in]{../diagrams/algorithm_gpu_flow}
\caption{GPGPU Image Registration Work-Flow}
\label{fig:gpgpu_algorithm_gpu_flow}
\end{figure}

Moving the image generation and similarity measurement to the graphics
processing unit produces the new image registration workflow in Figure
\ref{fig:gpgpu_algorithm_gpu_flow}.  As can be seen in this figure, the image
generation (the transform and interpolate functions) and similarity measurement
(calculate error) components are moved to the graphics processing unit.
Likewise, we also implement the pyramid on the GPU, since the computations
performed by the pyramid component map well to the architecture.
Implementating these data intensive portions of the image registration workflow
on the GPU should yield good performance increases since they are data parallel
activities and the graphics processing unit excels at computing such tasks.

The optimization component of the image registration workflow, however,
remains on the CPU as it does not affect the run-time that much (less than
$1-5\%$ of the total run-time) as shown in the CPU profiling.  Furthermore, the
optimization algorithm used in our implementation is a short running
computation with no data parallelism and thus is unsuitable for accelerating on
the graphics unit.

%-------------------------------------------------------------------------------

\section{CUDA Image Registration Implementation}

As noted in previous GPGPU studies \cite{ino, kubias, ozcelik} referenced in
Chapter \ref{ch:previous}, two keys to achieving performance increases and
optimal efficiency while accelerating an application with a graphics processing
unit are:

\begin{enumerate}
\item{}Minimize the number of GPU kernel invocations and
\item{}Limit the number and size of data transfers to and from the host
(CPU) and the device (GPU).  
\end{enumerate}

To accomplish this, we moved the pyramid construction component to the GPU and
generated the different pyramid levels on the device.  This allowed us to store
all the images on the GPU and removed the need to transfer whole images to and
from the host and device.  Additionally, we implemented the image generation
and part of the similarity measurement all in one kernel rather than two
separate modules.  This means we do not have to pass image data between
separate kernels and only need to invoke one kernel per optimization iteration,
thus reducing costly memory transfers costs and avoiding having to scan through
the image twice (once for generation, and again for similarity measurement).  

To examine how we implemented this single kernel, the CUDA bilinear kernel is
shown in Listing \ref{lst:gpgpu_bilinear}.  Each pixel/thread executes the following:

\begin{itemize}

\item{\bf \tt Lines 04 - 08:} Calculate the location of current thread with
respect to the local thread block and the {\it temporary} image.  

\item{\bf \tt Lines 11 - 16:} Compute the coordinates that map the {\it
temporary} image and the {\it source} image using the transformation matrix
stored in constant memory.

\item{\bf \tt Lines 19 - 21:} Get the pixel intensity for the current pixel
location by performing a texture fetch and store it in a local shared memory
array.  Then compute the squared error and save it in the same location.

\item{\bf \tt Lines 24 - 29:} If this is the first thread in the block, then
compute the partial sum of mean squared errors and save it to the destination
array in global memory.

\end{itemize}

\newpage

\lstset{language=C, numbers=left, numberstyle=\tiny, basicstyle=\small, 
        frame=single, caption={Bilinear Kernel}, label={lst:gpgpu_bilinear},
	captionpos=b, float=h}
\begin{lstlisting}
__global__ void
bilinear_kernel(float* dp, size_t dstride, uint rows, uint columns)
{
    /* Thread column, row */
    int ic = threadIdx.x; int ir = threadIdx.y;

    int tc = (blockIdx.x*blockDim.x)+ic; /* Target column */
    int tr = (blockIdx.y*blockDim.y)+ir; /* Target row */
    
    /* Map source column, row using matrix */
    float sc = (matrix_a(ConstantMatrix)*tc) + 
	       (matrix_b(ConstantMatrix)*tr) + 
	        matrix_e(ConstantMatrix);
    float sr = (matrix_c(ConstantMatrix)*tc) + 
	       (matrix_d(ConstantMatrix)*tr) + 
	        matrix_f(ConstantMatrix);

    /* Compute error, store in local memory */
    local_element(ic,ir)  = get_src_texel(sc,sr);
    local_element(ic,ir) -= get_tgt_texel(tc,tr);
    local_element(ic,ir) *= local_element(ic,ir); __syncthreads();

    /* If first thread, compute partial mse */
    if ((ic + ir) == 0) {
	float sum = 0.0;                                                        
	for (ir = 0; ir < BLOCK_SIZE; ir++)
	    for (ic = 0; ic < BLOCK_SIZE; ic++)
		sum += local_element(ic,ir); 
	dp[blockIdx.y*dstride+blockIdx.x] = sum;
    } 
}
\end{lstlisting}

For the bicubic kernel, the first $16$ lines are the same.  After line $16$, we
decompose the coordinates \texttt{sc} and \texttt{sr} into their integer and
fractional parts and compute the cubic interpolation of the each the columns in
the $4\times4$ neighborhood as explained in Section \ref{sec:ir_bicubic}.
Then, we replace line $19$ with a row-wise cubic interpolation of the
previously calculate column values.  As in the bilinear kernel, once the final
pixel intensity is computed, we then continue with the rest of kernel by
calculating the squared error and if appropriate, the partial sum for the block
and store this value back into global memory as the return value of our kernel
computation.

These kernels fit into the complete CUDA image registration workflow in the
following manner:

\begin{enumerate}

\item{\bf Pyramid Construction:} Both the {\it source} and {\it target} images
are transferred to the GPU and a pyramid of images is generated using a
parallel reduction CUDA kernel as shown in Listing \ref{lst:gpgpu_pyramid}. 

\item{\bf Registration:} For each level of the pyramid, starting with the
smallest level (i.e. most coarse image), we do the following:

\begin{enumerate}

\item{\bf CUDA Kernel:} Invoke the appropriate CUDA kernel for each pixel
location in the {\it target} image.  After each thread has executed the kernel
as explained above, the partial sum of mean squared errors for all the thread
blocks are stored in global memory.

\item{\bf Similarity Measurement:}  Download the partial sums created by the
CUDA kernel to the host CPU and complete the summation and division to produce
the mean squared error.

\item{\bf Optimization:} Use the previously calculated mean square error as the
cost function to the Nelder-Mead Simplex optimizer and update the
transformation coefficients estimate.

\end{enumerate}

Once the registration for a particular pyramid level is complete, use the
computed transformation estimate for the registration of the next pyramid
level.  After we register the final pyramid level (i.e. the full sized {\it
source} and {\it target} images), we have found our transformation coefficients
and return those values to the user.

\end{enumerate}

\lstset{language=C, numbers=left, numberstyle=\tiny, basicstyle=\small, 
        frame=single, caption={Pyramid Kernel}, label={lst:gpgpu_pyramid},
	captionpos=b, float=h}
\begin{lstlisting}
__global__ void
pyramid_kernel(int psize, float* tp, int is_tgt)
{
    int ic  = threadIdx.x;
    int ir  = threadIdx.y;
    int tc  = (blockIdx.x * blockDim.x) + ic; /* Target column */
    int tr  = (blockIdx.y * blockDim.y) + ir; /* Target row    */
    int sc  = tc + tc;
    int sr  = tr + tr;
    int sum = 0;

    if (!is_tgt) {
	sum = get_src_texel(sc,     sr)     + 
	      get_src_texel(sc,     sr + 1) +
	      get_src_texel(sc + 1, sr)     +
	      get_src_texel(sc + 1, sr + 1);
    } else {
	sum = get_tgt_texel(sc,     sr)     + 
	      get_tgt_texel(sc,     sr + 1) +
	      get_tgt_texel(sc + 1, sr)     +
	      get_tgt_texel(sc + 1, sr + 1);
    }
    tp[tr * psize + tc] = roundf(sum/4.0);
}
\end{lstlisting}

There are a few key ideas to note in this CUDA image registration
implementation.  First, we only the send the \textit{source} and
\textit{target} images once to the GPU where they are stored as CUDA arrays and
bound to texture references.  A pyramid kernel is called on each image for
every pyramid level and the output is stored on the GPU.  The use of textures
is important because they provide the CUDA threads cached reads to our image
data.  Moreover, using the texture memory gives us hardware support for
bilinear interpolation as shown in Listing \ref{lst:gpgpu_bilinear} and
automatic clamping, which removes the need to explicitly check the computed
intensities for overflow and underflow.

Second, originally there were explicit instructions to load the transformation
matrix into a local shared memory array used by all the threads in the block.
This was important because the kernel received the transformation matrix as a
pointer argument rather than explicit arguments, which means that the values
are stored in global memory rather than registers in the local frame.  To
compute the mapping, each thread would have been forced to fetch the same 6
floating point numbers for each kernel execution if this pre-fetching was not
done.  Not only would this have led to contention on the memory bus amongst the
threads, but it would also mean that each thread was performing memory fetches
from the slow uncached global memory.  To prevent this, the technique of
locally caching frequently used data was employed.  Storing the transformation
matrix in the shared memory allows all the threads within a block to fetch the
data from the much faster shared memory space.  This idea was also applied to
the storing of the pixel intensities.  Rather than write to global memory, each
thread saves its computed intensity to a local shared array, which is later
summed by the first thread of each group.  

In a later revision of our implementation, we replaced the shared
transformation matrix array with a constant memory array.  As noted earlier,
constant memory provides limited cached read-only memory to the threads on the
GPU device.  Rather than manually loading the transformation matrix in, we can
simply read the values in from constant memory once it has been configured by
the host.  This simplified the kernel code as shown in Listing
\ref{lst:gpgpu_bilinear} and provided slightly better performance than manually
pre-fetching the matrix (since we no longer needed to synchronize the threads
after loading the data).

Third, we implemented two image generation kernels: ({\bf 1}) the first one
uses the built-in bilinear filtering provided by the texture map, and ({\bf 2})
the second one uses our own implementation of bicubic interpolation.  This was
done because there are some applications that require more accuracy than what
bilinear interpolation can provide, and so the bicubic interpolation scheme is
provided in a separate CUDA kernel as described above.  Moreover, since we only
implemented bicubic interpolation in the CPU version, we wanted to provide as
much accuracy as possible in our CUDA implementation and have a fair
comparison.

Finally, it must be noted that we only compute partial sums of the squared
error on the GPU and finish the similarity measurement on the CPU.  Initially,
we implemented a parallel prefix version of the mean squared error calculation,
but perhaps because of poor implementation on our part, the calculations
accumulated too much error and thus produced poor results (i.e. low accuracy).
This floating point inaccuracy is not a new phenomenon and is discussed in
recent GPU papers \cite{hillesland}.  To limit our exposure to error and due to
ease of implementation, we only computed partial sums on the GPU and performed
the rest of the computation on the CPU.

%-------------------------------------------------------------------------------

\label{sec:gpgpu_micro}
\section{CUDA Microbenchmark Suite}

\paragraph{}To further analyze the performance characteristics of our CUDA
implementation, we also developed a small suite of microbenchmarks to test
certain aspects of the CUDA device and programming environment.  Specifically,
we tested the following device properties:

\begin{itemize}

\item{\bf Device Startup Time:} This measured the time it took for the CUDA
device to initialize.  Before any executation or memory transfers can take
place, the CUDA device must first be initialized and setup its bookkeeping.  To
perform this test, we called the function {\tt cudaGetDeviceCount} and kept
track of the amount of time required for it to return.

\item{\bf Empty Kernel:} This measured the time it takes to perform an empty
kernel (one that does no computation) by calling an empty kernel many times and
dividing the total amount of time by the number kernel invocations.

\item{\bf Memory Transfers:} This tested the memory bandwidth between the host
and device by transfer a large amount of data back and forth and dividing the
number of bytes by the elasped transfer time.

\item{\bf Sequential and Random Access:} These microbenchmarks measured the
speed of the various types of memory (shared, constant, texture, and global)
using both sequential and random access patterns.  To perform these tests we
created a kernel for each type of memory and computed the sum of all the items
in the block of memory.  An example of this kernel is shown in
\ref{lst:gpgpu_shared_kernel}

\end{itemize}

These microbenchmarks are relatively simple and straightforward but give a us a
better picture of the performance characteristics of the CUDA device, and thus
allowed us to form a better understanding of the resources and challenges
imposes by the hardware.  This information proved critical in our profiling
analysis explained in the next chapter and helped in identify new bottlenecks
and obstacles.

\paragraph{}

\noindent\lstset{language=C, numbers=left, numberstyle=\tiny, basicstyle=\small, 
        frame=single, caption={Shared Memory Kernel}, label={lst:gpgpu_shared_kernel},
	captionpos=b, float=h}
\begin{lstlisting}
__global__ void
cub_shared_kernel(bool seq, uint seed, uint fold, uint naccesses)
{
    uint  seq_index = 0;
    uint  rnd_index = seed;
    float sum = 0.0;

    for (int i = 0; i < naccesses; i++) {
	seq_index = i % fold;
	rnd_index = cub_cuda_mod(cub_cuda_rand(rnd_index), fold);
	sum += SharedMemoryBlock[(seq ? seq_index : rnd_index)];
    }
    __syncthreads();

    if (threadIdx.x == 0) 
	GlobalSum = sum;
}
\end{lstlisting}

%-------------------------------------------------------------------------------
% vim: sts=4 sw=4 ts=8 ft=tex
%-------------------------------------------------------------------------------
