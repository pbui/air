%-------------------------------------------------------------------------------
% slides.tex: 
%-------------------------------------------------------------------------------

\documentclass{beamer}

\usepackage{color, colortbl}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{subfigure}

%-------------------------------------------------------------------------------

\definecolor{darkgreen}{rgb}{0, 0.5, 0}
\definecolor{lightgray}{rgb}{0.8, 0.8, 0.8}

\definecolor{pc}{rgb}{0.56, 0.93, 0.56}
\definecolor{tc}{rgb}{1.00, 0.96, 0.53}
\definecolor{ec}{rgb}{0.68, 0.85, 0.90}
\definecolor{oc}{rgb}{0.95, 0.60, 0.91}
\definecolor{dc}{rgb}{0.95, 0.75, 0.80}

\setbeamertemplate{navigation symbols}{}
\setbeamercovered{dynamic}

%-------------------------------------------------------------------------------

\title{\textbf{AIR: Accelerated Image Registration}}

\author{Peter Bui \texttt{{\textless}pbui@cse.nd.edu{\textgreater}}}

\institute{University of Notre Dame, IN, USA}

\date[]{Master's Defense, November 2, 2009}

%-------------------------------------------------------------------------------

\begin{document}

%-------------------------------------------------------------------------------

\begin{frame}[t]
\titlepage
\end{frame}

%-------------------------------------------------------------------------------

\section{Outline}

\begin{frame}[t]{Outline}

\begin{enumerate}

\item{\bf Introduction}
\begin{itemize}
\item{Image Registration}
\item{Previous Work}
\end{itemize}

\item{\bf Pyramid-based Rigid Body Image Registration}
\begin{itemize}
\item{Affine Transformations}
\item{Interpolation}
\end{itemize}

\item{\bf CPU}
\begin{itemize}
\item{Experimental Setup}
\item{Results and Analysis}
\end{itemize}

\item{\bf GPU}
\begin{itemize}
\item{Design and Implementation}
\item{Results and Analysis}
\end{itemize}

\item{\bf Conclusions}
\begin{itemize}
\item{Contributions}
\item{Future Work}
\end{itemize}

\end{enumerate}

\end{frame}

%-------------------------------------------------------------------------------
\section{Introduction}
%-------------------------------------------------------------------------------

\begin{frame}[t]{Image Registration (Overview)}

\begin{figure}
\includegraphics[height=1.35in]{../diagrams/image_registration}
\end{figure}

\begin{itemize}

\item{\bf Objective:} Find transformation coefficients that map source image to
target image.

\item{\bf Applications:} Remote sensing, computer vision, image-guided surgery,
etc.

\end{itemize}
\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Image Registration (Workflow)}

\begin{figure}
\includegraphics[height=3.00in]{../diagrams/image_registration_workflow}
\end{figure}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Previous Work}

\begin{block}{OpenGL/DirectX}
\begin{itemize} 

\item{\bf Ino, Gomita, Kawasaki, Hagihara ({\it ISPA}, 2006)}\\2-D/3-D rigid
image registration speedup by \textcolor{darkgreen}{$5.0\times$} to
\textcolor{darkgreen}{$9.6\times$}.

\item{\bf Kubias, Deinzer, Feldmann, Paulus ({\it PRIA}, 2008)}\\Speedup rigid
image registration by \textcolor{darkgreen}{$3\times$} to
\textcolor{darkgreen}{$6\times$} and experimented with different similarity
measurements. 

\end{itemize}
\end{block}

\begin{block}{CUDA}
\begin{itemize}

\item{\bf Sugiura, Deguichi, Kitasaka, Mori, Suenaga ({\it AMI-ARCS},
2008)}\\Accelerated rigid image registration used in bronchoscope tracking by
a factor of \textcolor{darkgreen}{$16\times$}. 

\end{itemize}
\end{block}

\end{frame}

%-------------------------------------------------------------------------------
\section{Pyramid-based Rigid Body Image Registration}
%-------------------------------------------------------------------------------

\begin{frame}[t]{Pyramid-based Rigid Body Image Registration}

\begin{figure}
\includegraphics[height=1.75in]{../diagrams/algorithm_cpu_flow}
\end{figure}

\begin{itemize}
\item{}Requires more memory to store pyramid images.
\item{}Allows for course-to-fine grain optimization.
\item{}Default to using 4 pyramid levels.
\end{itemize}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Affine Transformations}

Scaling, Rotation, Translation

\begin{figure}
\includegraphics[width=2.25in]{../diagrams/scaling}
\end{figure}
\begin{figure}
\includegraphics[width=2.25in]{../diagrams/rotation}
\end{figure}
\begin{figure}
\includegraphics[width=2.25in]{../diagrams/translation}
\end{figure}
\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Interpolation}

\begin{figure}
\subfigure{\includegraphics[height=1.0in]{../diagrams/bilinear}}
\subfigure{\includegraphics[height=1.0in]{../diagrams/bicubic}}
\end{figure}

\begin{itemize}

\item{\bf Bilinear:} Requires access to 4 neighbors, and is supported by
texture hardware.  Normally used because it is fast.

\item{\bf Bicubic:} Requires access to 16 neighbors, and is not supported by
texture hardware.  Used when fidelity is priority.

\end{itemize}

\end{frame}

%-------------------------------------------------------------------------------
\section{CPU}
%-------------------------------------------------------------------------------

\begin{frame}[t]{Experimental Setup}

\begin{block}{System Configuration}
\begin{itemize}

\item{\bf Hardware:} 

\begin{itemize}
\item{}Intel Quad-Core Q6700 2.66 GHz CPU, 8.0 GB 
\item{}NVIDIA Tesla C870, 128 Stream Processors, 1.0GB 
\end{itemize}

\item{\bf Software:}
\begin{itemize}
\item{}Ubuntu 8.04 (kernel 2.6.20)
\item{}GCC 4.1.2 
\item{}NVIDIA CUDA 1.1 SDK
\end{itemize}

\end{itemize}
\end{block}


\begin{block}{Test Images}

\begin{table}[htb]
\centering
\scriptsize
\begin{tabular}{|r|c|}
\hline
\rowcolor{lightgray}{\bf Image} & {\bf Dimensions (Pixels)}\\
\hline
lenna	& $512\times512$\\
ndbuntu & $768\times768$\\
halo	& $1024\times1024$\\
jump	& $1536\times1536$\\
victory & $2048\times2048$\\
crabnebula & $3072\times3072$\\
\hline
\end{tabular}
\label{tbl:images}
\end{table}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{CPU Results (Performance)}

\begin{block}{Average Running Time}

\begin{figure}[htp]
\centering
\includegraphics[width=4.0in]{../plots/thesis_all_cpu_size_vs_runtime}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{CPU Results (Accuracy)}

\begin{block}{Average Peak Signal To Noise Ratio (Higher is Better)}

\begin{figure}[htp]
\centering
\includegraphics[width=4.0in]{../plots/thesis_all_cpu_images_average_psnr}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{CPU Results (Profiling)}

\begin{block}{Profiling of Smallest and Largest Images}

\begin{figure}[htp]
\centering
\includegraphics[width=4.0in]{../plots/thesis_cpu_profiling}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------
\section{GPU}
%-------------------------------------------------------------------------------

\begin{frame}[t]{GPU Architecture}

\begin{figure}[htp]
\centering
\includegraphics[height=1.5in]{../diagrams/smsp}
\end{figure}

\begin{itemize}
\item{} SPMD programming model.

\item{} User kernels are executed by array of concurrent stream processors (SP)
organized into stream multi-processor (SM) cores.

\item{} Exposes a limited memory hierarchy of registers, constant memory,
global memory, texture memory, and shared memory.

\end{itemize}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{GPU Implementation (Software Organization)}

Minimize kernel calls and memory transfers.

\begin{itemize}

\item{} Construct \fcolorbox{black}{pc}{Pyramid} image stacks on GPU.

\item{} Perform \fcolorbox{black}{tc}{Transform}, 
\fcolorbox{black}{tc}{Interpolate}, 
\fcolorbox{black}{ec}{Calculate Error} in one CUDA kernel.

\item{} Compute partial sum of the mean square errors. 

\item{} Keep \fcolorbox{black}{oc}{Optimize} on CPU.

\end{itemize}

\begin{figure}[htp]
\centering
\includegraphics[height=1.5in]{../diagrams/algorithm_gpu_flow}
\end{figure}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{GPU Implementation (Memory Organization)}

Minimize global memory accesses.

\begin{columns}[T]

\column{.55\textwidth}

\begin{figure}[htp]
\centering
\includegraphics[width=2.5in]{../diagrams/cuda_memory_organization}
\end{figure}

\column{.45\textwidth}

\begin{itemize}

\item{}Store images in \fcolorbox{black}{ec}{texture} memory.

\item{}Read transformation matrix from \fcolorbox{black}{tc}{constant} memory.

\item{}Build partial sums in from \fcolorbox{black}{dc}{shared} memory.

\end{itemize}

\end{columns}
\end{frame}

%-------------------------------------------------------------------------------
\section{GPU Results and Analysis}
%-------------------------------------------------------------------------------

\begin{frame}[t]{GPU Results (Performance)}

\begin{block}{Average Speedup Over CPU}

\begin{figure}[htp]
\centering
\includegraphics[width=4.0in]{../plots/thesis_all_cuda_size_vs_su3}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{GPU Results (Accuracy)}

\begin{block}{Average Peak Signal To Noise Ratio (Higher is Better)}

\begin{figure}[htp]
\centering
\includegraphics[width=4.50in]{../plots/thesis_all_images_average_psnr}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{GPU Results (Profiling)}

\begin{block}{Profiling of Smallest and Largest Images}

\begin{figure}[htp]
\centering
\includegraphics[width=4.15in]{../plots/gpgpu_2_profiling}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Summary}

\begin{block}{Performance}
\begin{itemize}

\item{}\textcolor{darkgreen}{$7.5\times$} to \textcolor{darkgreen}{$91.5\times$}
speedup for the \textcolor{red}{bilinear} version.

\item{}\textcolor{darkgreen}{$6.5\times$} to
\textcolor{darkgreen}{$33.0\times$} speedup for the \textcolor{blue}{bicubic}
version.

\item{}Speedup limited by CUDA device initialization time.

\end{itemize}
\end{block}

\begin{block}{Accuracy}
\begin{itemize}

\item{}CUDA kernels yield PSNRs in the range of $35 - 55$.

\item{}Overall, \textcolor{blue}{bicubic} interpolation more accurate than
\textcolor{red}{bilinear}.

\item{}Accuracy affected by GPU floating point implementation.

\end{itemize}
\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Conclusions}

\begin{block}{Lessons Learned}
\begin{itemize}

\item{Profiling and micro-benchmarking useful.}
\item{Minimize kernel calls and memory transfers.}
\item{Minimize global memory accesses.}

\end{itemize}

\end{block}

\begin{block}{Contributions}
\begin{itemize}
\item{}``Performance Analysis of Accelerated Image Registration Using GPGPU'',
GPGPU-2, Washington, D.C., 2008.
\item{}Source code distributed to Intel for possible use on Larrabee.
\end{itemize}
\end{block}

\begin{block}{Future Work}
\begin{itemize}
\item{}Amortize CUDA device initialization time.

\item{}Investigate streaming applications.

\item{}Consider alternative optimization algorithms.
\end{itemize}
\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{}
\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Experimental Results (Summary)}

\begin{table}[htb!]
\centering
\footnotesize
\begin{tabular}{|c|r|c|c|c|}
\hline
\rowcolor{lightgray}
{\bf Image} & {\bf Version}   & {\bf Run-time}  & {\bf Speedup}   & {\bf PSNR}\\
\hline
		& CPU	    & 5.19	& 1.00	    & 56.32 \\
{\bf lenna}	& Bilinear    & 0.69	& 7.48	    & 51.60 \\
		& Bicubic    & 0.80	& 6.50	    & 55.43 \\
\hline
		& CPU	    & 12.49	& 1.00	    & 56.35 \\
{\bf ndbuntu}	& Bilinear    & 0.74	& 16.80	    & 47.05 \\
		& Bicubic    & 0.98	& 12.64	    & 51.27 \\
\hline
		& CPU	    & 23.73	& 1.00	    & 44.19 \\
{\bf halo}	& Bilinear    & 0.83	& 28.40	    & 31.70 \\
		& Bicubic    & 1.30	& 18.25	    & 34.60 \\
\hline
		& CPU	    & 48.09	& 1.00	    & 55.86 \\
{\bf jump}	& Bilinear    & 1.04	& 46.41	    & 47.92 \\
		& Bicubic    & 2.07	& 23.22	    & 48.34 \\
\hline
		& CPU	    & 92.50	& 1.00	    & 52.83 \\
{\bf victory}	& Bilinear    & 1.36	& 67.89	    & 42.31 \\
		& Bicubic    & 3.16	& 29.17	    & 42.85 \\
\hline
		& CPU	    & 205.31	& 1.00	    & 54.26 \\
{\bf crabnebula}& Bilinear    & 2.24	& 91.47	    & 44.86 \\
		& Bicubic    & 6.24	& 32.92	    & 45.80 \\
\hline
\end{tabular}
\end{table}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}[t]{Experimental Setup (Test Cases)}

\begin{table}[htb!]
\centering
\begin{tabular}{|c|c||c|c|}
\hline
\rowcolor{lightgray}
Name& Image & Name& Image\\
\hline
lenna   & \includegraphics[width=0.9in]{../diagrams/lenna} &
ndbuntu & \includegraphics[width=0.9in]{../diagrams/ndbuntu} \\
\hline
halo    & \includegraphics[width=0.9in]{../diagrams/halo} &
jump    & \includegraphics[width=0.9in]{../diagrams/jump} \\
\hline  
victory & \includegraphics[width=0.9in]{../diagrams/victory} &
crabnebula & \includegraphics[width=0.9in]{../diagrams/crabnebula} \\
\hline
\end{tabular}
\end{table}

\end{frame}

%-------------------------------------------------------------------------------

\end{document}

%-------------------------------------------------------------------------------
