%-------------------------------------------------------------------------------
% slides.tex: 
%-------------------------------------------------------------------------------

\documentclass{beamer}

\usepackage{graphicx}
\usepackage{subfigure}

%-------------------------------------------------------------------------------

\title{\textbf{AIR: Accelerated Image Registration}}

\author{Peter Bui {\textless}pbui@cse.nd.edu{\textgreater}}

\date{December 8, 2008}

%-------------------------------------------------------------------------------

\begin{document}

\frame{\titlepage}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Problem Description}

\begin{block}{Image Registration}

The process of aligning a set of images so that corresponding features can be
easily related. 

\begin{itemize}

\item{}2D rigid image registration involves finding the transformation matrix
that will convert the source image to the target image.

\item{}Perform image registration on a set of images of a variety of sizes:
$512^2$, $768^2$, $1024^2$, $1536^2$, $2048^2$, $3072^2$ pixels.

\begin{figure}[htp]
\centering
\includegraphics[width=3.50in]{image_registration}
\end{figure}
\end{itemize}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{General Algorithm}

\begin{block}{Overview}

Continously transform source image and compare to target image.

\begin{figure}[htp]
\centering
\includegraphics[width=1.75in]{algorithm_cpu}
\end{figure}

\end{block}

\end{frame}

%\begin{block}{Key Components}

%\begin{itemize}

%\item{\textbf{Pyramid}:} Construct a stack of images where $Image_{n + 1}$ is
%a quarter of the size of $Image_{n}$.

%\item{\textbf{Transformation}:} Compute the new coordinates based on the
%transformation matrix.

%\item{\textbf{Interpolation}:} Use the new coordinates to produce the pixel
%values using an interpolation scheme (bilinear or bicubic).

%\item{\textbf{Error Calculation}:} Compute the error (ie. mean square error)
%between the transformed image and the target image.

%\item{\textbf{Optimization:}} Based on the error calculated, adjust
%transformation matrix.

%\end{itemize}

%\end{block}
%\end{comment}

%-------------------------------------------------------------------------------

%\begin{frame}

%\frametitle{CPU Implementation}

%\begin{columns}[T]

%\column{.5\textwidth}

%\begin{block}{Program Flow}

%\begin{figure}[htp]
%\centering
%\includegraphics[width=2.0in]{algorithm_cpu}
%\end{figure}

%\end{block}

%\column{.5\textwidth}

%\begin{block}{Notes}

%\begin{itemize}

%\item{} Functions are implemented on the CPU in C.

%\item{} Images stored as 1D arrays in CPU memory.

%\item{} Use compiler optimizations, and various tricks such as inline
%functions.

%\item{} Initial profiling showed that the transform function occupied about
%$95%\%$ of the running time.

%\end{itemize}

%\end{block}

%\end{columns}

%\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Project Goals}

\begin{block}{Primary Objective}

Accelerate image registration program by moving {\tt Transform}, {\tt
Interpolate}, and {\tt Calculate Error} functions onto the GPU by using CUDA.

\end{block}

\begin{block}{Secondary Objective}

Develop a small test suit of microbenchmarks and use data to analyze the
performance of accelerated image registration program.

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{GPU Implementation}

\begin{columns}[T]

\column{.5\textwidth}

\begin{block}{Program Flow}

\begin{figure}[htp]
\centering
\includegraphics[width=1.75in]{algorithm_gpu}
\end{figure}

\end{block}

\column{.5\textwidth}

\begin{block}{Notes}

\begin{itemize}

\item{} Source and target images are stored as textures.

\item{} Implemented both bilinear and bicubic interpolation.

\item{} Minimize kernel call and memory transfer overhead by performing all
three functions in one kernel.

\item{} Each thread block computes a partial sum of the mean square errors
which is then summed on the CPU. 

\end{itemize}

\end{block}

\end{columns}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Average Peak Signal To Noise Ratio}

\begin{figure}[htp]
\centering
\includegraphics[angle=90,width=4.25in]{all_images_average_psnr}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Average Running Time}

\begin{figure}[htp]
\centering
\includegraphics[angle=90,width=4.25in]{all_images_average_runtime}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Average Running Time as Image Size Increases}

\begin{figure}[htp]
\centering
\includegraphics[width=4.00in]{all_implementations_size_vs_runtime}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Average Speedup Over Fastest CPU}

\begin{figure}[htp]
\centering
\includegraphics[angle=90,width=4.25in]{all_images_average_su3}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Average Speedup Over Fastest CPU as Image Size Increases}

\begin{figure}[htp]
\centering
\includegraphics[width=4.00in]{all_implementations_size_vs_su3}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Halo Image Runtime Percentage Breakdown}

\begin{figure}[htp]
\centering
%\includegraphics[angle=90,width=4.75in]{halo_runtime_percentage}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Karp-Flatt Metric as Image Size Increases}

\begin{figure}[htp]
\centering
\includegraphics[width=4.25in]{all_cuda_size_vs_karp_flatt}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Experimental Results}

\begin{block}{Efficiency as Image Size Increases}

\begin{figure}[htp]
\centering
\includegraphics[width=4.25in]{all_cuda_size_vs_efficiency3}
\end{figure}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Microbenchmarks Results}

\begin{block}{Summary}

\begin{itemize}

\item{}CUDA device takes about $0.524$ seconds to initialize.

\item{}An empty kernel takes about $13$ microseconds to run.

\item{}CPU to GPU Memory bandwidth: $1552.041992$ $MBps$

\item{}GPU to CPU Memory bandwidth: $1615.182617$ $MBps$

\item{}Constant and Shared memory have roughly the same latency.

\item{}Global memory is $50\%$ slower than constant and shared memory.

\item{}Texture memory is $40\%$ slower than constant and shared memory.

\item{}Sequential and random access only affects texture memory where there is
a $3-4\%$ difference.

\end{itemize}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Analysis}

\begin{block}{Amdahl's Law}
\begin{tabular}{r l}
&\\
Maximum Speedup & = $1 / ((1 - P) + P/N)$\\
	        & = $1 / ((1 - 0.95) + 0.95/128)$\\
	        & = $17.42x$\\
&\\
\end{tabular}

Currently getting $3.5x - 7.0x$ speedup, which is about $40\%$ of the maximum
possible speedup.  However, Karp-Flatt metric indicates that $95\%$ maybe an
overestimate of the serial portion.

\end{block}

\begin{block}{Observations}

\begin{itemize}

\item{} Efficiency is horribly low.

\item{} Speedup improved as we increased in image size.

\item{} Smaller images could be limited by startup delay.

\item{} Due to nature of interpolation, memory coalescing schemes difficult to
implement.

\end{itemize}

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\begin{frame}

\frametitle{Conclusions}

\begin{block}{Summary}

\begin{itemize}

\item{} We were able to achieve decent speedup, comparable to recent
publications, and keep good accuracy.

\item{} Our problem seems to be memory bound rather than compute bound on the
GPU and this probably accounts for the lack of speedup and low efficiency.

\item{} Some accuracy problems with single-precision floating point on the GPU.

\end{itemize}

\end{block}

\begin{block}{Future Work}

We can implement the optimizer on the GPU, possibly using more advanced
algorithms such as gradient descent or Marquardt-Levenburg.  

\end{block}

\end{frame}

%-------------------------------------------------------------------------------

\end{document}

%-------------------------------------------------------------------------------
