#!/usr/bin/env python

#-------------------------------------------------------------------------------
# SConstruct: slides
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <pbui@cse.nd.edu>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

import os
import shutil
import sys

#-------------------------------------------------------------------------------
# Configuration
#-------------------------------------------------------------------------------

env = Environment()

toolpath = os.path.join(os.environ['HOME'], 'lib', 'scons', 'SCons', 'Tool')

env.Tool('Dia2Eps', toolpath = [toolpath])
env.Tool('Eps2Pdf', toolpath = [toolpath])

#-------------------------------------------------------------------------------
# Actions
#-------------------------------------------------------------------------------

def clean(target, source, env):
    os.system('scons -Q -c')

#-------------------------------------------------------------------------------
# Targets
#-------------------------------------------------------------------------------

slides_tex = env.PDF(['slides.tex'])

slides_dia = [env.Dia2Eps('algorithm_cpu.dia'),
	      env.Dia2Eps('algorithm_gpu.dia')]

slides_eps = [env.Eps2Pdf('algorithm_cpu.eps'),
	      env.Eps2Pdf('algorithm_gpu.eps')]

Depends(slides_eps, slides_dia)
Depends(slides_tex, slides_eps)

env.Alias('build', slides_tex)
env.Alias('clean', env.Command('#clean', '#SConstruct', clean))

#-------------------------------------------------------------------------------

env.targets = BUILD_TARGETS

if len(env.targets) == 0:
    env.targets.append('build')

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
