#!/usr/bin/env python

#-------------------------------------------------------------------------------
# plot.py: plot
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <peter.j.bui@gmail.com>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

from __future__ import with_statement

import os
import pickle
import shutil
import sys
import tempfile

from config import *
from common import *

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------

# Plot using bargraph script

def plot_bargraph(dfile, pfile):
    os.system('bargraph -pdf "%s" > "%s.pdf"' % (dfile, pfile))
    #os.system('bargraph -pdf "%s" > "%s.tmp.pdf"' % (dfile, pfile))

    #os.system('pdf90 --outfile "%s.pdf" "%s.tmp.pdf"' % (pfile, pfile))
    #os.system('pdf90 --outfile "%s.tmp.pdf" "%s.pdf"' % (pfile, pfile))
    #os.system('pdf90 --outfile "%s.pdf" "%s.tmp.pdf"' % (pfile, pfile))

    #os.remove('%s.tmp.pdf' % pfile)

#-------------------------------------------------------------------------------

# Plot using gnuplot

def plot_gnuplot(dfile, pfile):
    os.system('gnuplot < "%s" && epstopdf "%s.tmp.eps"' % (dfile, pfile))
    
    os.system('pdf90 --outfile "%s.pdf" "%s.tmp.pdf"' % (pfile, pfile))
    os.system('pdf90 --outfile "%s.tmp.pdf" "%s.pdf"' % (pfile, pfile))
    os.system('pdf90 --outfile "%s.pdf" "%s.tmp.pdf"' % (pfile, pfile))

    os.remove('%s.tmp.eps' % pfile)
    os.remove('%s.tmp.pdf' % pfile)

#-------------------------------------------------------------------------------

# Plot images field

def plot_images_field(dm, pdir, field, xlabel=None, ylabel=None, 
	yformat=None, implementations=AIR_IMPLEMENTATIONS):

    for image in AIR_IMAGES:
	pfile     = os.path.join(pdir, '%s_%s' % (image, field))
	__, dfile = tempfile.mkstemp(dir=AIR_TMP_DIR)
	dlines    = []

	dlines.append(';'.join(['=cluster'] + implementations))
    
	if xlabel:
	    dlines.append('xlabel=%s' % xlabel)

	if ylabel:
	    dlines.append('ylabel=%s' % ylabel)

	if yformat:
	    dlines.append('yformat=%s' % yformat)

	dlines.append('=norotate')
	dlines.append('=noupperright')
	dlines.append('legendx=7150')
	dlines.append('legendy=2000')
	dlines.append('=table')
	    
	for transformation in AIR_TRANSFORMATIONS:
	    dline = ['%s_%s' % (image, transformation.replace(' ', '_'))]

	    for implementation in implementations:
		dm_it = dm[image]['transformations'][transformation]

		dline.append(str(dm_it[implementation][field]))

	    dlines.append(' '.join(dline))
    
	with open(dfile, 'w+t') as fs:
	    fs.writelines([dline + '\n' for dline in dlines])

	plot_bargraph(dfile, pfile)

	os.remove(dfile)

#-------------------------------------------------------------------------------

def plot_images_flops(dm, pdir):
    plot_images_field(dm, pdir, 'flops',
	xlabel='Image Transformations', 
	ylabel='Pixels Per Second')

def plot_images_psnr(dm, pdir):
    plot_images_field(dm, pdir, 'psnr',
	xlabel='Image Transformations', 
	ylabel='Peak Signal to Noise Ratio')

def plot_images_runtime(dm, pdir):
    plot_images_field(dm, pdir, 'runtime',
	xlabel='Image Transformations',
	ylabel='Running Time (Seconds)')

def plot_images_su0(dm, pdir):
    plot_images_field(dm, pdir, 'su0',
	xlabel='Image Transformations', 
	ylabel='Average Speedup Relative to CPU 0 (No Optimizations)', 
	yformat='%gx')

def plot_images_su3(dm, pdir):
    plot_images_field(dm, pdir, 'su3',
	xlabel='Image Transformations', 
	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)',
	yformat='%01.01fx')

def plot_images_transform0(dm, pdir):
    plot_images_field(dm, pdir, 'transform0',
	xlabel='Image Transformations', 
	ylabel='Average Speedup Relative to CPU 0 (No Optimizations)', 
	yformat='%gx')

def plot_images_transform3(dm, pdir):
    plot_images_field(dm, pdir, 'transform3',
	xlabel='Image Transformations', 
	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)',
	yformat='%01.01fx')

def plot_images_transform_time(dm, pdir):
    plot_images_field(dm, pdir, 'transform_time',
	xlabel='Image Transformations',
	ylabel='Average Running Time (Seconds)',
	yformat='%01.01f')

#-------------------------------------------------------------------------------

# Plot images field averages

def plot_images_average_field(dm, pdir, field, 
	prefix='all_images_average_%s', xlabel=None, ylabel=None, yformat=None, 
	aliases=AIR_ALIASES, implementations=AIR_IMPLEMENTATIONS):

    pfile     = os.path.join(pdir, prefix % field)
    __, dfile = tempfile.mkstemp(dir=AIR_TMP_DIR)
    dlines    = []

    dlines.append(';'.join(['=cluster'] + aliases)) 
    dlines.append('=patterns')
    
    if xlabel is None:
	xlabel = 'Images (From Smallest to Largest)'

    dlines.append('xlabel=%s' % xlabel)

    if ylabel:
	dlines.append('ylabel=%s' % ylabel)

    if yformat:
	dlines.append('yformat=%s' % yformat)

    dlines.append('=norotate')
    dlines.append('=noupperright')
    dlines.append('legendx=7150')
    dlines.append('legendy=2000')
    dlines.append('colors=red,dark_green,dark_blue,magenta,cyan,yellow,black')
    dlines.append('=table')

    for image in get_images_by_size(dm):
	dline = [image]

	for implementation in implementations:
	    total = 0

	    for transformation in AIR_TRANSFORMATIONS:
		dm_it = dm[image]['transformations'][transformation]
		total = total + dm_it[implementation][field]

	    dline.append(str(total / len(AIR_TRANSFORMATIONS)))

	dlines.append(' '.join(dline))
    
    with open(dfile, 'w+t') as fs:
	fs.writelines([dline + '\n' for dline in dlines])

    plot_bargraph(dfile, pfile)

    os.remove(dfile)

#-------------------------------------------------------------------------------

def plot_images_average_flops(dm, pdir):
    plot_images_average_field(dm, pdir, 'flops',
	ylabel='Pixels Per Second')

def plot_images_average_psnr(dm, pdir):
    plot_images_average_field(dm, pdir, 'psnr',
	ylabel='Peak Signal to Noise Ratio')

def plot_cpu_images_average_psnr(dm, pdir):
    plot_images_average_field(dm, pdir, 'psnr',
	prefix='all_cpu_images_average_%s', 
	ylabel='Peak Signal to Noise Ratio',
	aliases=AIR_CPU_ALIASES, implementations=AIR_CPU_IMPLEMENTATIONS)

def plot_cuda_images_average_runtime(dm, pdir):
    plot_images_average_field(dm, pdir, 'runtime',
	prefix='all_cuda_images_average_%s', 
	ylabel='Running Time (Seconds)',
	aliases=['CPU'] + AIR_CUDA_ALIASES, 
	implementations=[AIR_CPU_IMPLEMENTATIONS[-1]]+AIR_CUDA_IMPLEMENTATIONS)

def plot_cuda_images_average_psnr(dm, pdir):
    plot_images_average_field(dm, pdir, 'psnr',
	prefix='all_cuda_images_average_%s', 
	ylabel='Peak Signal to Noise Ratio',
	aliases=['CPU'] + AIR_CUDA_ALIASES, 
	implementations=[AIR_CPU_IMPLEMENTATIONS[-1]]+AIR_CUDA_IMPLEMENTATIONS)

def plot_images_average_runtime(dm, pdir):
    plot_images_average_field(dm, pdir, 'runtime',
	ylabel='Running Time (Seconds)')

def plot_images_average_su0(dm, pdir):
    plot_images_average_field(dm, pdir, 'su0',
	ylabel='Average Speedup Relative to CPU 0 (No Optimizations)', 
	yformat='%gx')

def plot_images_average_su3(dm, pdir):
    plot_images_average_field(dm, pdir, 'su3',
	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)',
	yformat='%01.01fx')

def plot_images_average_transform0(dm, pdir):
    plot_images_average_field(dm, pdir, 'transform0',
	ylabel='Average Speedup Relative to CPU 0 (No Optimizations)', 
	yformat='%gx')

def plot_images_average_transform3(dm, pdir):
    plot_images_average_field(dm, pdir, 'transform3',
	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)',
	yformat='%01.01fx')

def plot_images_average_transform_time(dm, pdir):
    plot_images_average_field(dm, pdir, 'transform_time',
	ylabel='Average Running Time (Seconds)',
	yformat='%01.01f')

#-------------------------------------------------------------------------------

# GNU Plot of CPU optimization levels vs runtime

def plot_images_cpu_vs_runtime(dm, pdir):
    pfile      = os.path.join(pdir, 'all_images_cpu_vs_runtime')
    __, dfile1 = tempfile.mkstemp(dir=AIR_TMP_DIR)
    __, dfile2 = tempfile.mkstemp(dir=AIR_TMP_DIR)

    #---------------------------------------------------------------------------

    dlines1 = []

    dlines1.append('set term postscript enhanced color "Times-Roman" 16 lw 2')
    dlines1.append('set output "%s.tmp.eps"' % pfile)
    dlines1.append('set xlabel "CPU Optimization Level"')
    dlines1.append('set ylabel "Average Running Time (Seconds)"')

    for i in range(1, 7):
	if i == 6:
	    dlines1.append('set style line %d lt %d lw 2' % (i, i+1)) 
	else:
	    dlines1.append('set style line %d lt %d lw 2' % (i, i)) 

    image0 = get_images_by_size(dm)[0]
    dline1 = "plot '%s' using 1:2 title '%s' w lp ls 1" % (dfile2, image0)

    for i, image in enumerate(get_images_by_size(dm)):
	if i == 0: 
	    continue

	dline1 += ", '%s' using 1:%d title '%s' w lp ls %d" % \
		  (dfile2,i+2,image, i+1)

    dlines1.append(dline1)
    
    #---------------------------------------------------------------------------

    dlines2 = []

    for i, cpuN in enumerate(AIR_CPU_IMPLEMENTATIONS):
	dline2 = '%d' % i

	for image in get_images_by_size(dm):
	    dm_i = dm[image]['transformations']
	    dsum = sum([dm_i[t][cpuN]['runtime'] for t in AIR_TRANSFORMATIONS])

	    dline2 += ' %f' % (dsum/len(AIR_TRANSFORMATIONS))

	dlines2.append(dline2)

    #---------------------------------------------------------------------------

    for dfile, dlines in [(dfile1, dlines1), (dfile2, dlines2)]:
	with open(dfile, 'w+t') as fs:
	    fs.writelines([dline + '\n' for dline in dlines])
    
    plot_gnuplot(dfile1, pfile)

    for dfile in [dfile1, dfile2]:
	os.remove(dfile)

#-------------------------------------------------------------------------------

def plot_images_runtime(dm, pdir, type=None):
    if type is None:
	type = 'percentage'

    for image in AIR_IMAGES:
	pfile     = os.path.join(pdir, '%s_runtime_%s' % (image, type))
	__, dfile = tempfile.mkstemp(dir=AIR_TMP_DIR)
	dlines    = []

	dlines.append(';'.join(['=stackcluster', 
				'CUDA Transform Init',
				'CUDA Transform Kernel',
				'Pyramid Construction',
				'Transform',
				'Calculate MSE',
				'Everything Else']))
	dlines.append('xlabel=Image Transformations')

	if type == 'percentage':
	    dlines.append('ylabel=Percentage of Running Time')
	    dlines.append('yformat=%g%%')
	    dlines.append('max=100')
	else:
	    dlines.append('ylabel=Running Time (Seconds)')

	dlines.append('=noupperright')
	dlines.append('legendx=8350')
	dlines.append('legendy=2150')
	dlines.append('extraops=set size 1.2,1')
	dlines.append('=table')
	dlines.append('fontsz=12')
	    
	for transformation in AIR_TRANSFORMATIONS:
	    suffix = transformation.replace(' ', '_')

	    dlines.append('multimulti=%s_%s' % (image, suffix))

	    for implementation in AIR_IMPLEMENTATIONS:
		dm_it  = dm[image]['transformations'][transformation]
		dm_iti = dm_it[implementation]

		drt = dm_iti['runtime']

		if dm_iti.has_key('air_cpu_transform'):
		    dpt  = dm_iti['air_cpu_pyramid'][AIR_TIME_TOTAL]
		    dtt  = dm_iti['air_cpu_transform'][AIR_TIME_TOTAL]
		    dcti = 0
		    dctk = 0
		else:
		    dpt  = dm_iti['air_cuda_pyramid'][AIR_TIME_TOTAL]
		    dtt  = dm_iti['air_cuda_transform'][AIR_TIME_TOTAL]
		    dcti = dm_iti['air_cuda_transform_init'][AIR_TIME_TOTAL]
		    dctk = dm_iti['air_cuda_transform_kernel'][AIR_TIME_TOTAL]
		    dtt -= dctk

		if dm_iti.has_key('air_cpu_calculate_mse'):
		    dmse = dm_iti['air_cpu_calculate_mse'][AIR_TIME_TOTAL]
		else:
		    dmse = dm_iti['air_cuda_calculate_mse'][AIR_TIME_TOTAL]

		dee = drt - dtt - dcti - dctk - dmse - dpt

		dline = [implementation.replace(' ', '_')]
		
		for f in [dcti, dctk, dpt, dtt, dmse, dee]:
		    if type == 'percentage':
			f *= 100.0 / drt
		    dline.append(str(f))

		dlines.append(' '.join(dline))
    
	with open(dfile, 'w+t') as fs:
	    fs.writelines([dline + '\n' for dline in dlines])

	plot_bargraph(dfile, pfile)

	os.remove(dfile)

#-------------------------------------------------------------------------------

# Plot implementations field averages

def plot_implementations_average_field(dm, pdir, field, 
	xlabel=None, ylabel=None, yformat=None):

    pfile     = os.path.join(pdir, 'all_implementations_average_%s' % field)
    __, dfile = tempfile.mkstemp(dir=AIR_TMP_DIR)
    dlines    = []

    dlines.append(';'.join(['=cluster'] + get_images_by_size(dm))) 
    
    if xlabel:
	dlines.append('xlabel=%s' % xlabel)

    if ylabel:
	dlines.append('ylabel=%s' % ylabel)

    if yformat:
	dlines.append('yformat=%s' % yformat)

    dlines.append('=norotate')
    dlines.append('=noupperright')
    dlines.append('legendx=7150')
    dlines.append('legendy=2000')
    dlines.append('=table')

    for implementation in AIR_IMPLEMENTATIONS:
	dline = [implementation.replace(' ', '_')]

	for image in get_images_by_size(dm):
	    total = 0

	    for transformation in AIR_TRANSFORMATIONS:
		dm_it = dm[image]['transformations'][transformation]
		total = total + dm_it[implementation][field]

	    dline.append(str(total / len(AIR_TRANSFORMATIONS)))

	dlines.append(' '.join(dline))
    
    with open(dfile, 'w+t') as fs:
	fs.writelines([dline + '\n' for dline in dlines])

    plot_bargraph(dfile, pfile)

    os.remove(dfile)

#-------------------------------------------------------------------------------

def plot_implementations_average_flops(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'flops',
	xlabel='Implementations',
	ylabel='Pixels Per Second')

def plot_implementations_average_psnr(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'psnr',
	xlabel='Implementations',
	ylabel='Peak Signal to Noise Ratio')

def plot_implementations_average_runtime(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'runtime',
	xlabel='Implementations',
	ylabel='Running Time (Seconds)')

def plot_implementations_average_su0(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'su0',
	xlabel='Implementations',
	ylabel='Average Speedup Relative to CPU 0 (No Optimizations)', 
	yformat='%gx')

def plot_implementations_average_su3(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'su3',
	xlabel='Implementations',
	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)',
	yformat='%01.01fx')

def plot_implementations_average_transform0(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'transform0',
	xlabel='Implementations',
	ylabel='Average Speedup Relative to CPU 0 (No Optimizations)', 
	yformat='%gx')

def plot_implementations_average_transform3(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'transform3',
	xlabel='Implementations',
	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)',
	yformat='%01.01fx')

def plot_implementations_average_transform_time(dm, pdir):
    plot_implementations_average_field(dm, pdir, 'transform_time',
	xlabel='Implementations',
	ylabel='Average Running Time (Seconds)',
	yformat='%01.01f')

#-------------------------------------------------------------------------------

# GNU Plot of cpu image sizes vs field

def plot_cpu_size_vs_field(dm, pdir, field, ylabel=None):
    pfile      = os.path.join(pdir, 'all_cpu_size_vs_%s' % field)
    __, dfile1 = tempfile.mkstemp(dir=AIR_TMP_DIR)
    __, dfile2 = tempfile.mkstemp(dir=AIR_TMP_DIR)
    
    #---------------------------------------------------------------------------

    dlines1 = []

    dlines1.append('set term postscript enhanced color "Times-Roman" 16 lw 2')
    dlines1.append('set output "%s.tmp.eps"' % pfile)

    if field is 'runtime':
	dlines1.append('set key left top')

    dlines1.append('set xlabel "Image Size (MegaPixels)"')

    for i in range(1, 7):
	dlines1.append('set style line %d lt %d lw 2' % (i, i)) 
    
    if ylabel:
	dlines1.append('set ylabel "%s"' % ylabel)

    alias  = AIR_CPU_ALIASES[0]
    dline1 = "plot '%s' using 1:2 title '%s' w lp ls 1" % (dfile2, alias)

    for i, imp in enumerate(AIR_CPU_IMPLEMENTATIONS[1:]):
	alias = AIR_CPU_ALIASES[i+1]

	dline1 += ", '%s' using 1:%d title '%s' w lp ls %d" % \
		  (dfile2, i+3, alias, i+2)

    dlines1.append(dline1)
    
    #---------------------------------------------------------------------------

    dlines2 = []

    for image in get_images_by_size(dm):
	dm_i = dm[image]['transformations']

	dline2 = '%f' % (dm[image]['size']*dm[image]['size']/1024.0/1024.0)

	for imp in AIR_CPU_IMPLEMENTATIONS:
	    dm_i = dm[image]['transformations']
	    dsum = sum([dm_i[t][imp][field] for t in AIR_TRANSFORMATIONS])

	    dline2 += ' %f' % (dsum/len(AIR_TRANSFORMATIONS))

	dlines2.append(dline2)
    
    #---------------------------------------------------------------------------

    for dfile, dlines in [(dfile1, dlines1), (dfile2, dlines2)]:
	with open(dfile, 'w+t') as fs:
	    fs.writelines([dline + '\n' for dline in dlines])
    
    plot_gnuplot(dfile1, pfile)

    for dfile in [dfile1, dfile2]:
	os.remove(dfile)

#-------------------------------------------------------------------------------

def plot_cpu_size_vs_runtime(dm, pdir):
    plot_cpu_size_vs_field(dm, pdir, 'runtime',
	ylabel='Running Time (Seconds)')

#-------------------------------------------------------------------------------

# GNU Plot of cuda image sizes vs field

def plot_cuda_size_vs_field(dm, pdir, field, ylabel=None):
    pfile      = os.path.join(pdir, 'all_cuda_size_vs_%s' % field)
    __, dfile1 = tempfile.mkstemp(dir=AIR_TMP_DIR)
    __, dfile2 = tempfile.mkstemp(dir=AIR_TMP_DIR)
    
    #---------------------------------------------------------------------------

    dlines1 = []

    dlines1.append('set term postscript enhanced color '
	           'solid "Times-Roman" 16 lw 2')
    dlines1.append('set output "%s.tmp.eps"' % pfile)

    dlines1.append('set xlabel "Image Size (Pixels)"')

    dlines1.append('set style line 1 lt 2 lw 4') # Thick solid green
    dlines1.append('set style line 2 lt 3 lw 4') # Thick solid blue
    
    if ylabel:
	dlines1.append('set ylabel "%s"' % ylabel)

    alias  = AIR_CUDA_ALIASES[0]
    dline1 = "plot '%s' using 1:2 title '%s' w l ls 1" % (dfile2, alias)

    for i, imp in enumerate(AIR_CUDA_IMPLEMENTATIONS[1:]):
	alias = AIR_CUDA_ALIASES[i+1]

	dline1 += ", '%s' using 1:%d title '%s' w l ls %d" % \
		  (dfile2, i+3, alias, i+2)

    dlines1.append(dline1)
    
    #---------------------------------------------------------------------------

    dlines2 = []

    for image in get_images_by_size(dm):
	dm_i = dm[image]['transformations']

	dline2 = '%f' % (dm[image]['size']*dm[image]['size'])

	for imp in AIR_CUDA_IMPLEMENTATIONS:
	    dm_i = dm[image]['transformations']
	    dsum = sum([dm_i[t][imp][field] for t in AIR_TRANSFORMATIONS])

	    dline2 += ' %f' % (dsum/len(AIR_TRANSFORMATIONS))

	dlines2.append(dline2)
    
    #---------------------------------------------------------------------------

    for dfile, dlines in [(dfile1, dlines1), (dfile2, dlines2)]:
	with open(dfile, 'w+t') as fs:
	    fs.writelines([dline + '\n' for dline in dlines])
    
    plot_gnuplot(dfile1, pfile)

    for dfile in [dfile1, dfile2]:
	os.remove(dfile)

#-------------------------------------------------------------------------------

def plot_cuda_size_vs_efficiency3(dm, pdir):
    plot_cuda_size_vs_field(dm, pdir, 'efficiency3',
	ylabel='Efficiency (Percentage)')

def plot_cuda_size_vs_karp_flatt(dm, pdir):
    plot_cuda_size_vs_field(dm, pdir, 'karp_flatt',
	ylabel='Karp-Flatt Metric (Percentage)')

def plot_cuda_size_vs_runtime(dm, pdir):
    plot_cuda_size_vs_field(dm, pdir, 'runtime',
	ylabel='Running Time (Seconds)')

def plot_cuda_size_vs_su3(dm, pdir):
    plot_cuda_size_vs_field(dm, pdir, 'su3',
	ylabel='Average Speedup Relative to CPU')
#	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)')

#-------------------------------------------------------------------------------

# GNU Plot of image sizes vs running time

def plot_implementations_size_vs_field(dm, pdir, field, ylabel=None):
    pfile      = os.path.join(pdir, 'all_implementations_size_vs_%s' % field)
    __, dfile1 = tempfile.mkstemp(dir=AIR_TMP_DIR)
    __, dfile2 = tempfile.mkstemp(dir=AIR_TMP_DIR)
    
    #---------------------------------------------------------------------------

    dlines1 = []

    dlines1.append('set term postscript enhanced color '
	           'solid "Times-Roman" 16 lw 2')
    dlines1.append('set output "%s.tmp.eps"' % pfile)
    dlines1.append('set xlabel "Image Size (N^2 Pixels)"')
    
    if ylabel:
	dlines1.append('set ylabel "%s"' % ylabel)

    imp0   = AIR_IMPLEMENTATIONS[0]
    dline1 = "plot '%s' using 1:2 title '%s' with lines" % (dfile2, imp0)

    for i, imp in enumerate(AIR_IMPLEMENTATIONS):
	if i == 0: 
	    continue

	dline1 += ", '%s' using 1:%d title '%s' with lines" % (dfile2, i+2, imp)

    dlines1.append(dline1)
    
    #---------------------------------------------------------------------------

    dlines2 = []

    for image in get_images_by_size(dm):
	dm_i = dm[image]['transformations']

	dline2 = '%f' % dm[image]['size']

	for imp in AIR_IMPLEMENTATIONS:
	    dm_i = dm[image]['transformations']
	    dsum = sum([dm_i[t][imp][field] for t in AIR_TRANSFORMATIONS])

	    dline2 += ' %f' % (dsum/len(AIR_TRANSFORMATIONS))

	dlines2.append(dline2)
    
    #---------------------------------------------------------------------------

    for dfile, dlines in [(dfile1, dlines1), (dfile2, dlines2)]:
	with open(dfile, 'w+t') as fs:
	    fs.writelines([dline + '\n' for dline in dlines])
    
    plot_gnuplot(dfile1, pfile)

    for dfile in [dfile1, dfile2]:
	os.remove(dfile)

#-------------------------------------------------------------------------------

def plot_implementations_size_vs_runtime(dm, pdir):
    plot_implementations_size_vs_field(dm, pdir, 'runtime',
	ylabel='Average Running Time (Seconds)')

def plot_implementations_size_vs_su3(dm, pdir):
    plot_implementations_size_vs_field(dm, pdir, 'su3',
	ylabel='Average Speedup Relative to CPU 3 (Max Optimizations)')

#-------------------------------------------------------------------------------
# Main Execution
#-------------------------------------------------------------------------------

if __name__ == '__main__':
    df = AIR_DATA_FILE + '.average'
    dm = None

    if len(sys.argv) >= 2:
        df = sys.argv[1]

    with open(df, 'r') as fs:
	dm = pickle.load(fs)
    
    if os.path.exists(AIR_PLOT_DIR):
	shutil.rmtree(AIR_PLOT_DIR)
    
    os.mkdir(AIR_PLOT_DIR)
    
    #plot_images_flops(dm, AIR_PLOT_DIR)
    #plot_images_psnr(dm, AIR_PLOT_DIR)
    #plot_images_runtime(dm, AIR_PLOT_DIR)
    #plot_images_runtime(dm, AIR_PLOT_DIR, 'stack')
    #plot_images_su0(dm, AIR_PLOT_DIR)
    #plot_images_su3(dm, AIR_PLOT_DIR)
    #plot_images_transform0(dm, AIR_PLOT_DIR)
    #plot_images_transform3(dm, AIR_PLOT_DIR)
    #plot_images_transform_time(dm, AIR_PLOT_DIR)

    plot_images_average_flops(dm, AIR_PLOT_DIR)
    plot_images_average_psnr(dm, AIR_PLOT_DIR)
    plot_images_average_runtime(dm, AIR_PLOT_DIR)
    plot_images_average_su0(dm, AIR_PLOT_DIR)
    plot_images_average_su3(dm, AIR_PLOT_DIR)
    plot_images_average_transform0(dm, AIR_PLOT_DIR)
    plot_images_average_transform3(dm, AIR_PLOT_DIR)
    plot_images_average_transform_time(dm, AIR_PLOT_DIR)
    
    plot_images_cpu_vs_runtime(dm, AIR_PLOT_DIR)
    plot_cpu_size_vs_runtime(dm, AIR_PLOT_DIR)
    plot_cpu_images_average_psnr(dm, AIR_PLOT_DIR)
    
    plot_cuda_images_average_psnr(dm, AIR_PLOT_DIR)
    plot_cuda_images_average_runtime(dm, AIR_PLOT_DIR)
    plot_cuda_size_vs_karp_flatt(dm, AIR_PLOT_DIR)
    plot_cuda_size_vs_efficiency3(dm, AIR_PLOT_DIR)
    plot_cuda_size_vs_su3(dm, AIR_PLOT_DIR)
    plot_cuda_size_vs_runtime(dm, AIR_PLOT_DIR)
    
    plot_implementations_average_flops(dm, AIR_PLOT_DIR)
    plot_implementations_average_psnr(dm, AIR_PLOT_DIR)
    plot_implementations_average_runtime(dm, AIR_PLOT_DIR)
    plot_implementations_average_su0(dm, AIR_PLOT_DIR)
    plot_implementations_average_su3(dm, AIR_PLOT_DIR)
    plot_implementations_average_transform0(dm, AIR_PLOT_DIR)
    plot_implementations_average_transform3(dm, AIR_PLOT_DIR)
    plot_implementations_average_transform_time(dm, AIR_PLOT_DIR)
    
    plot_implementations_size_vs_runtime(dm, AIR_PLOT_DIR)
    plot_implementations_size_vs_su3(dm, AIR_PLOT_DIR)

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
