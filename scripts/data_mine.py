#!/usr/bin/env python

#-------------------------------------------------------------------------------
# data_mine.py: data mine
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <peter.j.bui@gmail.com>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

from __future__ import with_statement

import pickle
import re
import sys

from copy import deepcopy
from glob import glob
from operator import add

from config import *
from common import *

#-------------------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------------------

LOG_PSNR_REGEX  = re.compile('.*psnr\s+=\s+([0-9.]+)')

LOG_TIMER_REGEX = re.compile('\[timer\]\s+([\w]+)\s+=\s+'
			     'avg\(([0-9.]+)\),\s+'
			     'total\(([0-9.]+)\),\s+'
			     'runs\(([0-9.]+)\)')

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------

def data_mine(log_dir=AIR_LOG_DIR):
    dm = {}

    for image in AIR_IMAGES:
	dm[image] = {}
	dm[image]['size'] = get_image_size(image)
	dm[image]['transformations'] = {}

	for transformation in AIR_TRANSFORMATIONS:
	    dm_it = dm[image]['transformations'][transformation] = {}

	    baseline0  = 0
	    baseline3  = 0
	    transform0 = 0
	    transform3 = 0
	
	    for i, implementation in enumerate(AIR_IMPLEMENTATIONS):
		dm_iti = dm_it[implementation] = {}

		fname = get_log_file(image, 
				     transformation, 
				     implementation, 
				     log_dir)

		with open(fname, 'r') as fs:
		    for line in fs.readlines():
			if 'psnr' in line:
			    m = re.search(LOG_PSNR_REGEX, line)
			    dm_iti['psnr'] = float(m.groups()[0])
			elif 'timer' in line:
			    m = re.search(LOG_TIMER_REGEX, line)
			    dm_iti[m.groups()[0]] = map(float, m.groups()[1:])
		
		if i == 0:
		    baseline0  = dm_iti['air_register'][AIR_TIME_TOTAL]
		    transform0 = dm_iti['air_cpu_transform'][AIR_TIME_AVERAGE]
		elif i == 3:
		    baseline3  = dm_iti['air_register'][AIR_TIME_TOTAL]
		    transform3 = dm_iti['air_cpu_transform'][AIR_TIME_AVERAGE]
	    
	    for implementation in AIR_IMPLEMENTATIONS:
		dm_iti = dm_it[implementation]

		if 'cpu' in implementation:
		    transform_list = dm_iti['air_cpu_transform']
		else:
		    transform_list = dm_iti['air_cuda_transform']

		dm_iti['runtime'] = dm_iti['air_register'][AIR_TIME_TOTAL]

		dm_iti['transform_runs'] = transform_list[AIR_TIME_RUNS]
		dm_iti['transform_time'] = transform_list[AIR_TIME_AVERAGE]

		dm_iti['transform0'] = transform0 / dm_iti['transform_time'] 
		dm_iti['transform3'] = transform3 / dm_iti['transform_time'] 
		 
		dm_iti['flops']  = dm[image]['size'] * dm[image]['size']
		dm_iti['flops'] *= transform_list[AIR_TIME_RUNS]
		dm_iti['flops'] /= dm_iti['runtime'] 

		dm_iti['su0'] = baseline0 / dm_iti['runtime']
		dm_iti['su3'] = baseline3 / dm_iti['runtime']

		dm_iti['karp_flatt']  = 100*(AIR_CUDA_NPROCS/dm_iti['su3'] - 1)
		dm_iti['karp_flatt'] /= (AIR_CUDA_NPROCS - 1)
		
		dm_iti['efficiency3'] = 100*dm_iti['su3'] / AIR_CUDA_NPROCS

    return (dm)

#-------------------------------------------------------------------------------
# Main Execution
#-------------------------------------------------------------------------------

if __name__ == '__main__':
    log_dir_list = glob(AIR_LOG_DIR + '*')
    ldl_size     = len(log_dir_list)
    dmine = {}
    
    for i, log_dir in enumerate(log_dir_list + ['average']):
	if log_dir == 'average':
	    dname = log_dir
	else:
	    dname = os.path.basename(log_dir).strip('log.')
	    dmine[dname] = data_mine(log_dir)

	dfile = AIR_DATA_FILE + '.' + dname

	with open(dfile, 'w') as fs:
	    pickle.dump(dmine[dname], fs)

	if i == 0:
	    dmine['average'] = deepcopy(dmine[dname])

	for image in AIR_IMAGES:
	    dm_i = dmine[dname][image]
	    da_i = dmine['average'][image]

	    for transformation in AIR_TRANSFORMATIONS:
		dm_it = dm_i['transformations'][transformation]
		da_it = da_i['transformations'][transformation]

		for implementation in AIR_IMPLEMENTATIONS:
		    dm_iti = dm_it[implementation]
		    da_iti = da_it[implementation]

		    for key, value in dm_iti.items():
			if i == 0:
			    if type(value) is list:
				da_iti[key] = [0, 0, 0]
			    else:
				da_iti[key] = 0

			if type(value) is list:
			    value       = map(lambda x: x/ldl_size, value)
			    da_iti[key] = map(add, da_iti[key], value)
			else:
			    da_iti[key] += value / ldl_size
	
#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
