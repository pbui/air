#!/usr/bin/env python

#-------------------------------------------------------------------------------
# generate_pgm.py: generate pgms
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <peter.j.bui@gmail.com>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

import os
import pickle
import re
import sys
import time

from glob import glob

from config import *
from common import *

#-------------------------------------------------------------------------------
# Main Execution
#-------------------------------------------------------------------------------

if __name__ == '__main__':
    os.system('cd %s; scons -Q with_opt_level=%d' % 
	     (AIR_BASE_DIR, AIR_CPU_LEVELS[-1]))

    for image in AIR_IMAGES:
	src = image + '.pgm'
	src = os.path.join(AIR_PGM_DIR, src)
	
	for i, transformation in enumerate(AIR_TRANSFORMATIONS):
	    tgt = image + '_' + transformation + '.pgm'
	    tgt = os.path.join(AIR_PGM_DIR, tgt)

	    print 'transforming %s -> (%s)' % (image, transformation)
	    os.system('%s %s %s %s ' % 
		     (AIR_TRANSFORM, AIR_TRANSFORMATIONS_FLAGS[i], src, tgt))

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
