/*------------------------------------------------------------------------------
 * air_cpu.h: air cpu functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**----------------------------------------------------------------------------*/

#ifndef	__AIR_CPU_H__
#define	__AIR_CPU_H__

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <gsl/gsl_multimin.h>

#include "air_error.h"
#include "air_filter.h"
#include "air_image.h"
#include "air_matrix.h"
#include "air_optimize.h"

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

ERR_TYPE air_cpu_pyramid(image*, uint, image**[]);
ERR_TYPE air_cpu_transform(image*, image*, image*, matrix);

float	 air_cpu_bicubic_interpolation(float*, image*, float, float);
float	 air_cpu_cubic(float, float, float, float, float);
float	 air_cpu_bilinear_interpolation(float*, image*, float, float);

double	 air_cpu_calculate_mse(image*, image*);
double	 air_cpu_calculate_psnr(image*, image*, double, double);

double   air_cpu_evaluate(const gsl_vector*, void*);
ERR_TYPE air_cpu_register(image*, image*, int, func_eval);

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
