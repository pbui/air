/*------------------------------------------------------------------------------
 * air_register.c: air register program
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
 *------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>
#include <unistd.h>

#include <ctu.h>

#include "air.h"

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

int  AIR_PyramidLevels  = 4;
int  AIR_Implementation = AIR_CPU_IMPLEMENTATION;

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

void air_register_init(int, char*[], char**, char**);
void air_register_usage(int, char*[]);

/*------------------------------------------------------------------------------
 * Functions
**----------------------------------------------------------------------------*/

int	    
main(int argc, char* argv[]) {
    char*   sf;
    image*  si;
    
    char*   tf;
    image*  ti;

    air_register_init(argc, argv, &sf, &tf);

    if (AIR_Time) ctu_timer_start(AIR_TIMER_REGISTER);

    air_image_read(sf, &si);
    air_image_read(tf, &ti);

    switch (AIR_Implementation) {
	case AIR_CPU_IMPLEMENTATION:
	    air_cpu_register(si, ti, AIR_PyramidLevels, air_cpu_evaluate);
	    break;

#ifdef AIR_WITH_CUDA
	case AIR_CUDA_IMPLEMENTATION:
	    air_cuda_register(si, ti, AIR_PyramidLevels, air_cuda_evaluate);
	    break;
#endif

	default:
	    air_warn("air_register", 
		     "Unknown implementation type(%d)", AIR_Implementation);
	    break;
    }
    
    air_image_free(si);
    air_image_free(ti);

    if (AIR_Time) {
	ctu_timer_stop(AIR_TIMER_REGISTER);
	ctu_timer_print_summary(false);
    }

    return (EXIT_SUCCESS);
}

/*----------------------------------------------------------------------------*/

void
air_register_init(int argc, char* argv[], char** sf, char** tf) {
    char c;

    CTU_DebugStream = CTU_ErrorStream = stderr;

    while ((c = getopt(argc, argv, "atk:i:d:p:h")) != -1) {
	switch (c) {
	    case 'a':
		AIR_Assert = true;
		break;
	    
	    case 'i':
		AIR_Implementation = strtol(optarg, NULL, 10);
		break;
	    
	    case 'k':
		AIR_TransformKernel = strtol(optarg, NULL, 10);
		break;
	    
	    case 't':
		AIR_Time = true;
		break;

	    case 'd':
		AIR_Debug = strtol(optarg, NULL, 10);
		break;
	    
	    case 'p':
		AIR_PyramidLevels = strtol(optarg, NULL, 10);
		break;

	    case 'h':
	    default:
		air_register_usage(argc, argv);
		break;
	}
    }

    if (argc - optind != 2) 
	air_register_usage(argc, argv);

    if (AIR_Time)
	ctu_timer_init(AIR_TIMERS_SIZE, AIR_TIMER_STRINGS);

    *sf = argv[optind];
    *tf = argv[optind+1];
}

/*----------------------------------------------------------------------------*/

void
air_register_usage(int argc, char* argv[]) {
    fprintf(stderr, "usage: %s [options] source target\n", argv[0]);
    fprintf(stderr, "\nGeneral Options:\n");
    fprintf(stderr, "  -a                  Check assertions\n");
    fprintf(stderr, "  -t                  Show timing information\n");
    fprintf(stderr, "  -h                  Show this help message\n");
    fprintf(stderr, "  -d <Level>          Show debug level messages\n");
    fprintf(stderr, "  -k <Kernel>         Transformation kernel (Bilinear = 0, Bicubic = 1)\n");
#ifdef AIR_WITH_CUDA
    fprintf(stderr, "  -i <Implementation> Implementation (CPU = 0, CUDA = 1)\n");
#endif
    fprintf(stderr, "\nRegistration Options:\n");
    fprintf(stderr, "  -p <Level>          Number of pyramid levels\n");
    exit(EXIT_FAILURE);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
