/*------------------------------------------------------------------------------
 * air_matrix.c: air transformation matrix data structure
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
 *------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <math.h>
#include <stdio.h>

#include "air_matrix.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/*------------------------------------------------------------------------------
 * Functions 
**----------------------------------------------------------------------------*/

inline
ERR_TYPE
air_matrix_compute(matrix m, float ox, float oy, parameter p) {
#define AIR_FN "air_matrix_compute"
    float dx = (float)air_parameter_dx(p);
    float dy = (float)air_parameter_dy(p);
    float sx = (float)air_parameter_sx(p);
    float sy = (float)air_parameter_sy(p);
    float angle = (float)air_parameter_angle(p);

    angle = angle*M_PI/180.0;

    air_matrix_a(m) =  cosf(angle) / sx; 
    air_matrix_b(m) =  sinf(angle) / sy; 
    air_matrix_c(m) = -sinf(angle) / sx; 
    air_matrix_d(m) =  cosf(angle) / sy; 

    air_matrix_e(m) = (ox * (1 - air_matrix_a(m))) - 
		      (dx * air_matrix_a(m)) -
		      (oy * air_matrix_b(m)) + 
		      (dy * air_matrix_b(m));

    air_matrix_f(m) = (oy * (1 - air_matrix_d(m))) + 
		      (dy * air_matrix_d(m)) -
		      (ox * air_matrix_c(m)) - 
		      (dx * air_matrix_c(m));

    if (AIR_Debug > 1) {
	fprintf(CTU_DebugStream, "[D] %s: ", AIR_FN);
	air_parameter_write_stream(CTU_DebugStream, p);
	fprintf(CTU_DebugStream, "[D] %s: ", AIR_FN);
	air_matrix_write_stream(CTU_DebugStream, m);
    }

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_matrix_write_stream(FILE* fs, matrix m) {
    fprintf(fs, "a: %2.4f", air_matrix_a(m));
    fprintf(fs, " b: %2.4f", air_matrix_b(m));
    fprintf(fs, " c: %2.4f", air_matrix_c(m));
    fprintf(fs, " d: %2.4f", air_matrix_d(m));
    fprintf(fs, " e: %2.4f", air_matrix_e(m));
    fprintf(fs, " f: %2.4f\n", air_matrix_f(m));
    return (ERR_SUCCESS);
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_parameter_write_stream(FILE* fs, parameter p) {
    fprintf(fs, "dx: %2.4lf", air_parameter_dx(p));
    fprintf(fs, " dy: %2.4lf", air_parameter_dy(p));
    fprintf(fs, " sx: %2.4lf", air_parameter_sx(p));
    fprintf(fs, " sy: %2.4lf", air_parameter_sy(p));
    fprintf(fs, " angle: %2.4lf\n", air_parameter_angle(p));
    return (ERR_SUCCESS);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=cpp 
**----------------------------------------------------------------------------*/
