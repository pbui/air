/*------------------------------------------------------------------------------
 * ctu_timer.c: air timer functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**----------------------------------------------------------------------------*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/time.h>

#include "ctu_timer.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

const char* AIR_TIMER_STRINGS[] =
{
    "air_register             ",
    "air_cpu_register         ",
    "air_cpu_pyramid          ",
    "air_cpu_transform        ",
    "air_cpu_calculate_mse    ",
    "air_optimize             ",
    "air_cuda_register        ",
    "air_cuda_pyramid         ",
    "air_cuda_transform_init  ",
    "air_cuda_transform       ",
    "air_cuda_transform_kernel",
    "air_cuda_calculate_mse   ",
};

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

bool AIR_Time = false;

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
