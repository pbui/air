/*------------------------------------------------------------------------------
 * air_error.c: air error handling
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ctu.h>

#include "air_error.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

const char* AIR_ERROR_STRINGS[] = {
    "Success",
    "Bad input",
    "Null input",
    "No handler for file extension",
    "Errno",
    "Unknown",
};

/*----------------------------------------------------------------------------*/

const bool AIR_ERROR = true;
const bool AIR_WARN  = false;

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

bool AIR_Assert = false;
int  AIR_Debug = 0;

/*------------------------------------------------------------------------------
 * Functions
**----------------------------------------------------------------------------*/

void
air_assert(bool iserr, const char* fn, bool cnd, ERR_TYPE err) {
    char* msg;

    if (!cnd) {
	if (err == ERR_ERRNO) 
	    msg = strerror(errno); 
	else if (err > ERR_UNKNOWN) 
	    msg = (char*)AIR_ERROR_STRINGS[ERR_UNKNOWN]; 
	else 
	    msg = (char*)AIR_ERROR_STRINGS[err];

	if (iserr)
	    air_error(fn, err, msg);
	else
	    air_warn(fn, msg);
    }
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
