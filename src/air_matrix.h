/*------------------------------------------------------------------------------
 * air_matrix.h: air transformation matrix data structure
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#ifndef	__AIR_MATRIX_H__
#define	__AIR_MATRIX_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>

#include "air_error.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

#define AIR_MATRIX_SIZE	    6
#define AIR_PARAMETER_SIZE  5

/*------------------------------------------------------------------------------
 * Type Definitions
**----------------------------------------------------------------------------*/

/**
 *
 * Transformation matrix
 *
 * [ A B C D E F ] 
 */

typedef	float matrix[AIR_MATRIX_SIZE];

/**
 *
 * Transformation parameters
 *
 * [ dx, dy, sx, sy, angle ]
 */
typedef double parameter[AIR_PARAMETER_SIZE];

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

ERR_TYPE air_matrix_compute(matrix, float, float, parameter);

ERR_TYPE air_matrix_write_stream(FILE*, matrix);
ERR_TYPE air_parameter_write_stream(FILE*, parameter);

/*------------------------------------------------------------------------------
 * Macros
**----------------------------------------------------------------------------*/

#define	air_matrix_a(m)	(m)[0]
#define	air_matrix_b(m)	(m)[1]
#define	air_matrix_c(m)	(m)[2]
#define	air_matrix_d(m)	(m)[3]
#define	air_matrix_e(m)	(m)[4]
#define	air_matrix_f(m)	(m)[5]

#define	air_parameter_dx(p)    (p)[0]
#define	air_parameter_dy(p)    (p)[1]
#define	air_parameter_sx(p)    (p)[2]
#define	air_parameter_sy(p)    (p)[3]
#define	air_parameter_angle(p) (p)[4]

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
