/*------------------------------------------------------------------------------
 * air.h: accelerated image registration
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**----------------------------------------------------------------------------*/

#ifndef	__AIR_H__
#define	__AIR_H__

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include "air_cpu.h"
#include "air_cuda.h"
#include "air_error.h"
#include "air_filter.h"
#include "air_image.h"
#include "air_image_pgm.h"
#include "air_image_raw.h"
#include "air_optimize.h"
#include "air_timer.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

enum 
{
    AIR_CPU_IMPLEMENTATION = 0,
    AIR_CUDA_IMPLEMENTATION
};

enum
{
    AIR_BILINEAR_TRANSFORM_KERNEL = 0,
    AIR_BICUBIC_TRANSFORM_KERNEL
};

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

extern int AIR_TransformKernel;

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
