/*------------------------------------------------------------------------------
 * air_optimize.c: air optimize functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <math.h>

#include <gsl/gsl_multimin.h>

#include "air_optimize.h"

#include "air_cpu.h"
#include "air_image_pgm.h"
#include "air_matrix.h"
#include "air_timer.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

const uint   AIR_MAX_OPTIMIZATION_ITERATIONS = 100;
const double AIR_TEST_SIZE = 1e-2;

/*------------------------------------------------------------------------------
 * Functions
**----------------------------------------------------------------------------*/

float
air_optimize(image* si, image* ti, image* di, double** p, func_eval fe)
{
#define AIR_FN "air_optimize"

    const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex;

    gsl_multimin_fminimizer* fm = NULL;  /* Function minimizer */

    gsl_multimin_function fc;	/* Function */
    gsl_vector*	x;		/* Function parameter vector */
    gsl_vector* ss;		/* Function step sizes*/
    
    image* params[3] = { si, ti, di };

    uint   iter = 0;
    int    status;
    double size;
    float  mse;

    /* Start Timer */
    if (AIR_Time) ctu_timer_start(AIR_TIMER_OPTIMIZE);

    /* Initialize function parameters and step sizes */
    x = gsl_vector_alloc(AIR_PARAMETER_SIZE);
    gsl_vector_set(x, 0, air_parameter_dx(*p));
    gsl_vector_set(x, 1, air_parameter_dy(*p));
    gsl_vector_set(x, 2, air_parameter_sx(*p));
    gsl_vector_set(x, 3, air_parameter_sy(*p));
    gsl_vector_set(x, 4, air_parameter_angle(*p));
    
    ss = gsl_vector_alloc(AIR_PARAMETER_SIZE);
    gsl_vector_set_all(ss, 1.0);

    /* Setup function structure */
    fc.n = AIR_PARAMETER_SIZE;
    fc.f = fe;
    fc.params = (void*)&params;

    /* Allocate and set minimizer */
    fm = gsl_multimin_fminimizer_alloc(T, AIR_PARAMETER_SIZE);
    gsl_multimin_fminimizer_set(fm, &fc, x, ss);

    /* Iterate through mininizer until simplex size meets tolerance or we reach
     * arbitrary iteration limit */
    do {
	/* Run one iteration of the minimizer */
	status = gsl_multimin_fminimizer_iterate(fm);
	iter++;
	
	if (status) break;

	/* Check size of simplex */
	size   = gsl_multimin_fminimizer_size(fm);
	status = gsl_multimin_test_size(size, AIR_TEST_SIZE);

	if (AIR_Debug > 1) {
	    air_debug(AIR_FN,
		"%3d %2.3f %2.3f %2.3f %2.3f %2.3f f() = %2.3f size = %2.3f",
		iter,
		gsl_vector_get(fm->x, 0),
		gsl_vector_get(fm->x, 1),
		gsl_vector_get(fm->x, 2),
		gsl_vector_get(fm->x, 3),
		gsl_vector_get(fm->x, 4),
		fm->fval,
		gsl_multimin_fminimizer_size(fm));
	}
    } while (status == GSL_CONTINUE && iter < AIR_MAX_OPTIMIZATION_ITERATIONS);

    /* Copy parameters */
    air_parameter_dx(*p) = gsl_vector_get(fm->x, 0);
    air_parameter_dy(*p) = gsl_vector_get(fm->x, 1);
    air_parameter_sx(*p) = gsl_vector_get(fm->x, 2);
    air_parameter_sy(*p) = gsl_vector_get(fm->x, 3);
    air_parameter_angle(*p) = gsl_vector_get(fm->x, 4);
    mse = fm->fval;
    
    /* Free resources */
    gsl_vector_free(x); gsl_vector_free(ss); gsl_multimin_fminimizer_free(fm);
    
    /* Stop Timer */
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_OPTIMIZE);

    return (mse);
#undef AIR_FN
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
