/*------------------------------------------------------------------------------
 * air_image_raw.c: air raw image support
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "air_error.h"
#include "air_image.h"
#include "air_image_raw.h"

/*------------------------------------------------------------------------------
 * Functions 
**----------------------------------------------------------------------------*/

ERR_TYPE
air_image_raw_write_stream(FILE* fs, image** im) {
#define AIR_FN "air_image_raw_write_stream"
    if (AIR_Assert)
	air_assert(AIR_ERROR, AIR_FN, fs && (*im), ERR_NULL_INPUT);

    fprintf(fs, "%d %d\n", (*im)->columns, (*im)->rows);

    for (uint r = 0; r < (*im)->rows; r++) {
	fprintf(fs, "%3u", air_image_pixel((*im), 0, r));
	
	for (uint c = 1; c < (*im)->columns; c++) 
	    fprintf(fs, " %3u", air_image_pixel((*im), c, r));

	fputc('\n', fs);
    }

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
