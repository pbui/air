/*------------------------------------------------------------------------------
 * air_image.h: air image data structure
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**----------------------------------------------------------------------------*/

#ifndef	__AIR_IMAGE_H__
#define	__AIR_IMAGE_H__

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include "air_error.h"

/*------------------------------------------------------------------------------
 * Type Definitions
**----------------------------------------------------------------------------*/

typedef	struct air_image_t	   image;
typedef struct air_image_handler_t image_handler;

typedef ERR_TYPE (*ir_fio)(const char*, image**);
typedef ERR_TYPE (*ir_sio)(FILE*, image**);

/*------------------------------------------------------------------------------
 * Data Structures
**----------------------------------------------------------------------------*/

struct air_image_t {
    byte* pixels;
    uint  rows;
    uint  columns;
};

struct air_image_handler_t {
    char*  extension;
    ir_fio read;
    ir_fio write;
    ir_sio read_stream;
    ir_sio write_stream;
};

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

image*	 air_image_alloc(uint, uint);
image*	 air_image_free(image*);

image_handler* air_image_get_handler(const char*);

ERR_TYPE air_image_read(const char*, image**);
ERR_TYPE air_image_write(const char*, image**);

ERR_TYPE air_image_read_stream(FILE*, const char*, image**);
ERR_TYPE air_image_write_stream(FILE*, const char*, image**);

float*	 air_image_alloc_plane(int, int);
ERR_TYPE air_image_copy_to_plane(image*, float*);
ERR_TYPE air_image_copy_from_plane(image*, float*);

/*------------------------------------------------------------------------------
 * Macros
**----------------------------------------------------------------------------*/

#define air_image_pixel(i, c, r) \
    (i)->pixels[(r)*(i)->columns + (c)]

#define air_image_plane_value(p, i, c, r) \
    (p)[(r)*(i)->columns + (c)]

#define air_image_is_valid_pixel(i, c, r) \
    (0 <= (c) && (c) < (i)->columns) && (0 <= (r) && (r) < (i)->rows)

#define air_image_get_pixel_from_plane(p, i, c, r) \
    air_image_plane_value((p), (i), \
	((c) < 0 ? 0 : (c) >= (i)->columns ? (i)->columns - 1 : (c)), \
	((r) < 0 ? 0 : (r) >= (i)->rows ? (i)->rows - 1 : (r)))

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
