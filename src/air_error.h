/*------------------------------------------------------------------------------
 * air_error.h: air error handling
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**----------------------------------------------------------------------------*/

#ifndef	__AIR_ERROR_H__
#define	__AIR_ERROR_H__

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>

#include <ctu.h>

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

extern const char* AIR_ERROR_STRINGS[];

extern const bool AIR_ERROR;
extern const bool AIR_WARN;

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

extern bool AIR_Assert;
extern int  AIR_Debug;

/*------------------------------------------------------------------------------
 * Type Definitions
**----------------------------------------------------------------------------*/

typedef	enum ERR_TYPE_T	ERR_TYPE;

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

enum ERR_TYPE_T {
    ERR_SUCCESS,
    ERR_BAD_INPUT,
    ERR_NULL_INPUT,
    ERR_NO_HANDLER,
    ERR_ERRNO,
    ERR_UNKNOWN
};

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

void air_assert(bool, const char*, bool, ERR_TYPE);

/*------------------------------------------------------------------------------
 * Macros
**----------------------------------------------------------------------------*/

#define air_debug(fn, fmt, ...)     ctu_debug((fn), (fmt), ## __VA_ARGS__)
#define air_warn(fn, fmt, ...)      ctu_warn((fn), (fmt), ## __VA_ARGS__) 
#define air_error(fn, er, fmt, ...) ctu_error((fn), (er), (fmt), ##__VA_ARGS__)

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
