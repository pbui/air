/*------------------------------------------------------------------------------
 * air_cpu.c: air cpu functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <math.h>

#include <gsl/gsl_multimin.h>

#include "air.h"
#include "air_cpu.h"

#include "air_image_pgm.h"
#include "air_optimize.h"
#include "air_timer.h"

/*------------------------------------------------------------------------------
 * Functions
**----------------------------------------------------------------------------*/

/**
 * Creates a set of pyramids where each successive level is 4x smaller than the
 * previous.
 *
 * @param si      [IN]  Source image
 * @param levels  [IN]  Number of pyramid levels
 * @param pyramid [OUT] Pointer to array of images representing pyramid
 *
 * @return Error status
 *
 * This function will iteratively quarter each source image to produce the
 * proceeding pyarmid level.  A mean filter is used to interpolate the pixel
 * values.
 **/

ERR_TYPE 
air_cpu_pyramid(image* si, uint levels, image** pyramid[]) 
{
#define AIR_FN "air_cpu_pyramid"
    image* pi; /* Parent image (previous pyramid level) */
    image* ci; /* Child image (current pyramid level) */

    /* Start timer */
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CPU_PYRAMID);

    /* Allocate pyramid levels (+1 for temporary level) */
    *pyramid = malloc(sizeof(image*)*(levels + 1));

    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, *pyramid, ERR_ERRNO);

    /* Set first level to si image */
    (*pyramid)[0] = si;

    /* Produce each pyramid level */
    for (uint i = 1; i < levels; i++) {
	/* Parent image is previous level */
	pi = (*pyramid)[i - 1];

	/* Allocate child image */
	ci = (*pyramid)[i] = air_image_alloc(pi->columns/2, pi->rows/2);

	/* Scale down image using mean-filter */
	for (uint cr = 0; cr < ci->rows; cr++) {
	    for (uint cc = 0; cc < ci->columns; cc++) {
		uint pr = cr + cr;
		uint pc = cc + cc;

		air_image_pixel(ci, cc, cr) = \
		    roundf((float)(air_image_pixel(pi, pc, pr) +
				   air_image_pixel(pi, pc, pr + 1) + 
				   air_image_pixel(pi, pc + 1, pr) + 
				   air_image_pixel(pi, pc + 1, pr + 1))/4.0);
	    }
	}
    }
    
    /* Stop timer */
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CPU_PYRAMID);

    if (AIR_Debug > 1) 
	for (uint i = 0; i < levels; i++)
	    air_image_write_stream(CTU_DebugStream, "raw", &((*pyramid)[i])); 

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

/**
 * Computes inverse transform by applying input matrix to destination image
 * coordinates and uses the computed values as the new coordinates to index
 * into the source image.
 *
 * @param si [IN]  Source image
 * @param ti [IN]  Target image
 * @param di [OUT] Destination image
 * @param m  [IN]  Transformation matrix
 *
 * @return Error status
 *
 * This function uses the coefficients in the transformation matrix (m) to
 * translate the pixels in the source image (si), interpolate them, and store
 * them back in the destination image (di).  The target image (ti) is not used.
 **/

ERR_TYPE
air_cpu_transform(image* si, image* ti, image* di, matrix m) 
{
    float* sp;

    /* Start timer */
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CPU_TRANSFORM);

    /* Allocate float plane */
    sp = air_image_alloc_plane(si->columns, si->rows);

    /* Copy source image to source plane */
    air_image_copy_to_plane(si, sp);

    /* Perform translation and calculate interpoated pixel value */
    for (uint dr = 0; dr < di->rows; dr++) {
	for (uint dc = 0; dc < di->columns; dc++) {
	    /* Compute translate row and column coordinates */
	    float sr = (air_matrix_c(m)*dc) + 
		       (air_matrix_d(m)*dr) + 
		        air_matrix_f(m);
	    float sc = (air_matrix_a(m)*dc) + 
		       (air_matrix_b(m)*dr) + 
		        air_matrix_e(m);
	    /* Computer interpolated pixel value */
	    float sv;
	    if (AIR_TransformKernel == AIR_BILINEAR_TRANSFORM_KERNEL) {
		sv = air_cpu_bilinear_interpolation(sp, si, sc, sr);
	    } else {
		sv = air_cpu_bicubic_interpolation(sp, si, sc, sr);
	    }

	    air_image_pixel(di, dc, dr) = sv;
	}
    }

    /* Free allocated plane */
    free(sp);
    
    /* Stop timer */
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CPU_TRANSFORM);

    return (ERR_SUCCESS);
}

/*----------------------------------------------------------------------------*/

/** 
 * Perform cubic interpolation of as specified on page 147 of "2-D and 3-D
 * Image Registration" by A. Goshtasby.  
 *
 * @param sp [IN] Source image float plane
 * @param si [IN] Source image 
 * @param sc [IN] Source image column
 * @param sr [IN] Source image row 
 *
 * @return The cubic interpolation as a floating point number.
 *
 * This function first calculates the cubic interpolation of all the rows in
 * the 4x4 neighborhood.  It then calculates the cubic interpolation of these
 * row values as a single column and returns the result as a floating point
 * number.
 *
 * Additional design ideas were taken from the following:
 *
 * - "Various Simple Image Processing Techniques" by Paul Bourke
 **/

float
air_cpu_bicubic_interpolation(float* sp, image* si, float sc, float sr) 
{
    int   sc_int  = (int)floor(sc);
    int   sr_int  = (int)floor(sr);
    float sc_frac = sc - sc_int;
    float sr_frac = sr - sr_int;

    /* Perform cubic interpolation of rows in each column */
    float c0 = air_cpu_cubic(sr_frac,
	    air_image_get_pixel_from_plane(sp, si, sc_int-1, sr_int-1),
	    air_image_get_pixel_from_plane(sp, si, sc_int-1, sr_int),
	    air_image_get_pixel_from_plane(sp, si, sc_int-1, sr_int+1),
	    air_image_get_pixel_from_plane(sp, si, sc_int-1, sr_int+2));
    
    float c1 = air_cpu_cubic(sr_frac,
	    air_image_get_pixel_from_plane(sp, si, sc_int,   sr_int-1),
	    air_image_get_pixel_from_plane(sp, si, sc_int,   sr_int),
	    air_image_get_pixel_from_plane(sp, si, sc_int,   sr_int+1),
	    air_image_get_pixel_from_plane(sp, si, sc_int,   sr_int+2));
    
    float c2 = air_cpu_cubic(sr_frac,
	    air_image_get_pixel_from_plane(sp, si, sc_int+1, sr_int-1),
	    air_image_get_pixel_from_plane(sp, si, sc_int+1, sr_int),
	    air_image_get_pixel_from_plane(sp, si, sc_int+1, sr_int+1),
	    air_image_get_pixel_from_plane(sp, si, sc_int+1, sr_int+2));
    
    float c3 = air_cpu_cubic(sr_frac,
	    air_image_get_pixel_from_plane(sp, si, sc_int+2, sr_int-1),
	    air_image_get_pixel_from_plane(sp, si, sc_int+2, sr_int),
	    air_image_get_pixel_from_plane(sp, si, sc_int+2, sr_int+1),
	    air_image_get_pixel_from_plane(sp, si, sc_int+2, sr_int+2));

    /* Perform cubic interpolation of columns as a single row */
    return (air_cpu_cubic(sc_frac, c0, c1, c2, c3));
}

/*----------------------------------------------------------------------------*/

/** 
 * Calculate the cubic interpolation as specified on page 147 of "2-D and 3-D
 * Image Registration" by A. Goshtasby.  
 *
 * @param t  [IN] Offset (generally the fractional part between p1 and p2)
 * @param p0 [IN] Pixel value of the first point
 * @param p1 [IN] Pixel value of the second point
 * @param p2 [IN] Pixel value of the third point
 * @param p3 [IN] Pixel value of the fourth point
 *
 * @return Cubic interpolation of the four points and offset
 *
 * This function returns the cubic interpolation as a floating point number.
 **/

float
air_cpu_cubic(float t, float p0, float p1, float p2, float p3) 
{
    float t2 = t*t;
    float t3 = t2*t;

    return ((p0 * (-(t3 + t)/2.0 + t2)) +
	    (p1 * ((3*t3 - 5*t2)/2.0 + 1)) +
	    (p2 * ((-3*t3 + t)/2.0 + 2.0*t2)) +
	    (p3 * ((t3 - t2)/2.0)));
}

/*----------------------------------------------------------------------------*/

/** 
 * Calculate bilinear interpolation as specified in "A Multi-ASIC Real-Time
 * Implementation of the Two Dimensional Affine Transform with a Bilinear
 * Interpolation Scheme" by Mark J. Bentum, Martin M. Samson, and Cornelis H.
 * Slump.
 *
 * @param sp [IN] Source image float plane
 * @param si [IN] Source image 
 * @param sc [IN] Source image column
 * @param sr [IN] Source image row 
 *
 * @return The bilinear interpolation as a floating point number.
 **/

float
air_cpu_bilinear_interpolation(float* sp, image* si, float sc, float sr) 
{
    int   sc_int  = (int)floor(sc);
    int   sr_int  = (int)floor(sr);
    float sc_frac = sc - sc_int;
    float sr_frac = sr - sr_int;

    return ((1 - sr_frac)*(1 - sc_frac)*
	    air_image_get_pixel_from_plane(sp, si, sc_int, sr_int) +
	    (1 - sr_frac)*(sc_frac)*
	    air_image_get_pixel_from_plane(sp, si, sc_int + 1, sr_int) +
	    (sr_frac)*(1 - sc_frac)*
	    air_image_get_pixel_from_plane(sp, si, sc_int, sr_int + 1) +
	    (sr_frac)*(sc_frac)*
	    air_image_get_pixel_from_plane(sp, si, sc_int + 1, sr_int + 1));
}

/*----------------------------------------------------------------------------*/

/**
 * Calculate the mean square error of the two images.
 *
 * @param si [IN] Source image
 * @param ti [IN] Target image
 *
 * @return The mean square error.
 **/

double
air_cpu_calculate_mse(image* si, image* ti) 
{
    double acc  = 0.0;
    double diff = 0.0;
    
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CPU_CALCULATE_MSE);

    for (uint r = 0; r < si->rows; r++) {
	for (uint c = 0; c < si->columns; c++) {
	    diff  = (double)air_image_pixel(ti, c, r) - 
		    (double)air_image_pixel(si, c, r);
	    diff *= diff;
	    acc  += diff;
	}
    }
    
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CPU_CALCULATE_MSE);

    return ((double)(acc) / (si->rows*si->columns));
}

/*----------------------------------------------------------------------------*/

/**
 * Calculate the peak signal to noise ratio.
 *
 * @param si        [IN] Source image
 * @param ti        [IN] Target image
 * @param max_value [IN] Maximum possible pixel value 
 * @param mse [IN] Pre-calculated mean square error (use -1.0 to do on-the-fly).
 *
 * @return The peak signal to noise ratio.
 **/

double
air_cpu_calculate_psnr(image* si, image* ti, double max_value, double mse) 
{
    mse = (mse < 0.0) ? air_cpu_calculate_mse(si, ti) : mse;

    return (10*log10(max_value*max_value/mse));
}

/*----------------------------------------------------------------------------*/

/**
 * Optimization evaluation function.
 *
 * @param v      [IN] Parameter vector
 * @param params [IN] Meta parameters
 *
 * @return Scalar result of evaluation.  In this case, the mean square error.
 *
 * This function uses the parameter vector (v) to setup a transformation matrix
 * that is then used to transform the source image into a dump image.  The mean
 * square error of this new image and the target image is then calculated and
 * returned.  The pointers to the image structures are passed in using the meta
 * parameters pointer (params).
 **/

double
air_cpu_evaluate(const gsl_vector* v, void* params) 
{
#define AIR_FN "air_cpu_evaluate"
    image** iv = (image**)params;
    image*  si = iv[0];
    image*  ti = iv[1];
    image*  di = iv[2];
    
    matrix    m;
    parameter p;
    float     mse;

    /* Copy parameters from vector into local parameter array */
    air_parameter_dx(p) = gsl_vector_get(v, 0);
    air_parameter_dy(p) = gsl_vector_get(v, 1);
    air_parameter_sx(p) = gsl_vector_get(v, 2);
    air_parameter_sy(p) = gsl_vector_get(v, 3);
    air_parameter_angle(p) = gsl_vector_get(v, 4);

    /* Compute the transformation matrix based on the parameters */
    air_matrix_compute(m, si->columns/2, si->rows/2, p); 

    /* Transform the source image using the computed matrix */
    air_cpu_transform(si, ti, di, m);

    /* Calculate the mean square error of the new image and the target image */
    mse = air_cpu_calculate_mse(ti, di);

    if (AIR_Debug > 1) {
	float psnr = air_cpu_calculate_psnr(NULL, NULL, AIR_PGM_MAX_VALUE, mse);

	fprintf(CTU_DebugStream, "[D] %s: ", AIR_FN);
	air_parameter_write_stream(CTU_DebugStream, p);
	air_debug(AIR_FN, "mse = %lf, psnr = %lf", mse, psnr);
    }

    return (mse);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

/**
 * Perform CPU version of image registration.
 *
 * @param si         [IN] Source image
 * @param ti         [IN] Target image
 * @param levels     [IN] Number of pyramid levels
 * @param fe	     [IN] Function evaluator used by optimizer
 *
 * @return Error status
 *
 * This function performs the process of image registration by:
 * 
 * - Initializing the transformation parameter set.
 *
 * - Creating initialization source, target, and dump image pyramids.
 *
 * - Optimizing at each level of the pyramids, passing the parameters at the
 *   preceeding level to the next level of the pyramid.
 *
 * - Outputing the results of the optimizations.
 **/

ERR_TYPE
air_cpu_register(image* si, image* ti, int levels, func_eval fe)
{
    image*  di;
    image** sp;
    image** tp;
    image** dp;
    double* p;

    float   mse  = 0.0;
    float   psnr = 0.0;

    /* Start timer */
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CPU_REGISTER);

    /* Create parameter set */
    p = malloc(sizeof(double)*AIR_PARAMETER_SIZE);
    air_parameter_dx(p) = 0.0;
    air_parameter_dy(p) = 0.0;
    air_parameter_sx(p) = 1.0;
    air_parameter_sy(p) = 1.0;
    air_parameter_angle(p) = 0.0;

    /* Create destination/dump image */
    di = air_image_alloc(si->columns, si->rows);

    /* Create pyramids */
    air_cpu_pyramid(si, levels, &sp);
    air_cpu_pyramid(ti, levels, &tp);
    air_cpu_pyramid(di, levels, &dp);

    /* For each level of the pyramid perform optimization */
    for (int i = levels; i > 0; i--) {
	int clevel = i - 1;
	image* csi = sp[clevel];
	image* cti = tp[clevel];
	image* cdi = dp[clevel];

	/* Perform optimization using last set of parameters */
	mse = air_optimize(csi, cti, cdi, (double**)&p, fe);

	/* Free resources when appropriate */
	if (clevel) {
	    air_image_free(csi); air_image_free(cti); air_image_free(cdi);
	}
    }
    
    /* Output results */
    psnr = air_cpu_calculate_psnr(NULL, NULL, AIR_PGM_MAX_VALUE, mse);

    printf("[ cpu ] "); air_parameter_write_stream(stdout, p);
    printf("[ cpu ] mse = %f, psnr = %f\n", mse, psnr);

    /* Free resources */
    free(p); free(sp); free(tp); free(dp); free(di);
    
    /* Stop timer */
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CPU_REGISTER);

    return (ERR_SUCCESS);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
