/*------------------------------------------------------------------------------
 * air_transform.c: air transform program
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
 *------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>
#include <unistd.h>

#include <ctu.h>

#include "air.h"

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

float AIR_Dx = 0.0;
float AIR_Dy = 0.0;
float AIR_Sx = 1.0;
float AIR_Sy = 1.0;
float AIR_Angle = 0.0;

int   AIR_Implementation = AIR_CPU_IMPLEMENTATION;

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

void air_transform_init(int, char*[], char**, char**);
void air_transform_usage(int, char*[]);

/*------------------------------------------------------------------------------
 * Functions
**----------------------------------------------------------------------------*/

int	    
main(int argc, char* argv[]) 
{
    char*  src_file;
    image* src_image;

    char*  tgt_file;
    image* tgt_image;
    
    matrix    m;
    parameter p;

    air_transform_init(argc, argv, &src_file, &tgt_file);

    air_image_read(src_file, &src_image);
    tgt_image = air_image_alloc(src_image->columns, src_image->rows);

    air_parameter_dx(p) = AIR_Dx;
    air_parameter_dy(p) = AIR_Dy;
    air_parameter_sx(p) = AIR_Sx;
    air_parameter_sy(p) = AIR_Sy;
    air_parameter_angle(p) = AIR_Angle;

    air_matrix_compute(m, src_image->columns/2, src_image->rows/2, p);
    
    switch (AIR_Implementation) {
	case AIR_CPU_IMPLEMENTATION:
	    air_cpu_transform(src_image, tgt_image, tgt_image, m);
	    break;
#ifdef AIR_WITH_CUDA	
	case AIR_CUDA_IMPLEMENTATION:
	    air_cuda_transform(src_image, tgt_image, tgt_image, m);
	    break;
#endif
	default:
	    air_warn("air_register", "Unknown implementation type(%d)", 
				     AIR_Implementation);
	    break;
    }
    
    air_image_write(tgt_file, &tgt_image);

    air_image_free(src_image);
    air_image_free(tgt_image);

    if (AIR_Time) ctu_timer_print_summary(false);
	
    return (EXIT_SUCCESS);
}

/*----------------------------------------------------------------------------*/

void
air_transform_init(int argc, char* argv[], char** src_file, char** tgt_file) 
{
    char c;

    CTU_DebugStream = CTU_ErrorStream = stderr;

    while ((c = getopt(argc, argv, "atd:k:i:x:y:s:r:h")) != -1) {
	switch (c) {
	    case 'a':
		AIR_Assert = true;
		break;
	    
	    case 't':
		AIR_Time = true;
		break;
	    
	    case 'd':
		AIR_Debug  = strtol(optarg, NULL, 10);
		break;
	    
	    case 'i':
		AIR_Implementation = strtol(optarg, NULL, 10);
		break;

	    case 'k':
		AIR_TransformKernel = strtol(optarg, NULL, 10);
		break;

	    case 'x':
		AIR_Dx = strtof(optarg, NULL);
		break;
	    
	    case 'y':
		AIR_Dy = strtof(optarg, NULL);
		break;
	    
	    case 's':
		AIR_Sx = AIR_Sy = strtof(optarg, NULL);
		break;
	    
	    case 'r':
		AIR_Angle = strtof(optarg, NULL);
		break;

	    case 'h':
	    default:
		air_transform_usage(argc, argv);
		break;
	}
    }

    if (argc - optind != 2) 
	air_transform_usage(argc, argv);

    if (AIR_Time)
	ctu_timer_init(AIR_TIMERS_SIZE, AIR_TIMER_STRINGS);

    *src_file = argv[optind];
    *tgt_file = argv[optind+1];
}

/*----------------------------------------------------------------------------*/

void
air_transform_usage(int argc, char* argv[]) 
{
    fprintf(stderr, "usage: %s [options] src_file tgt_file\n", argv[0]);
    fprintf(stderr, "\nGeneral Options:\n");
    fprintf(stderr, "  -a                  Check assertions\n");
    fprintf(stderr, "  -t                  Show timing information\n");
    fprintf(stderr, "  -h                  Show this help message\n");
    fprintf(stderr, "  -d <Level>          Show debug level messages\n");
    fprintf(stderr, "  -k <Kernel>         Transformation kernel (Bilinear = 0, Bicubic = 1)\n");
#ifdef AIR_WITH_CUDA	
    fprintf(stderr, "  -i <Implementation> Implementation (CPU = 0, CUDA = 1)\n");
#endif
    fprintf(stderr, "\nTransformation Options:\n");
    fprintf(stderr, "  -x <Dx>             Translate Dx\n");
    fprintf(stderr, "  -y <Dy>             Translate Dy\n");
    fprintf(stderr, "  -s <Scale>          Scale by s\n");
    fprintf(stderr, "  -r <Rotation>       Rotate about origin by r\n");
    exit(EXIT_FAILURE);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
