/*------------------------------------------------------------------------------
 * air_filter.c: air filter
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <math.h>

#include "air_error.h"
#include "air_filter.h"

/*------------------------------------------------------------------------------
 * Functions
**----------------------------------------------------------------------------*/

ERR_TYPE
air_filter_initialize(filter* f, filter_func fc, uint radius, float step) {
#define AIR_FN "air_filter_initialize"
    if (AIR_Assert)
	air_assert(AIR_ERROR, AIR_FN, f != NULL && fc != NULL, ERR_BAD_INPUT);

    f->size = ((float)radius/step)*2 + 1;
    f->step = step;
    f->weight = malloc(sizeof(float) * f->size);

    if (AIR_Assert)
	air_assert(AIR_ERROR, AIR_FN, f->weight != NULL, ERR_ERRNO);

    for (uint i = 0; i <= f->size/2; i++) {
	f->weight[f->size/2 + i] = f->weight[f->size/2 - i] = fc(f->step*i);

	if (AIR_Debug) {
	    air_debug(AIR_FN, "weight[%f] = (%f)", f->step*i, f->weight[f->size/2 + i]);
	    air_debug(AIR_FN, "weight[%f] = (%f)", -(f->step*i), f->weight[f->size/2 - i]);
	}
    }

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

float
air_bspline_cubic(float x) {
    float ax = fabs(x);

    if (-1 <= x && x <= 1) {
	return (1.0/6)*(-3*pow(1 - ax, 3) + 3*pow(1 - ax, 2) + 3*(1 - ax) + 1);
    } else if (1 <= ax && ax <= 2) {
	return (1.0/6)*(pow(2 - ax, 3));
    } else {
	return (0);
    }
}

/*----------------------------------------------------------------------------*/

float
air_catmull_rom_cubic(float x) {
    float ax = fabs(x);

    if (-1 <= x && x <= 1) {
	return (1.0/2)*(-3*pow(1 - ax, 3) + 4*pow(1 - ax, 2) + (1 - ax));
    } else if (1 <= ax && ax <= 2) {
	return (1.0/2)*(pow(2 - ax, 3) - pow(2 - ax, 2));
    } else {
	return (0);
    }
}

/*----------------------------------------------------------------------------*/

float
air_mitch_netra_cubic(float x) {
    return ((1.0/3)*air_bspline_cubic(x) + 
	    (2.0/3)*air_catmull_rom_cubic(x));
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
