/*------------------------------------------------------------------------------
 * air_cuda.cu: air cuda functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdlib.h>

#include <gsl/gsl_multimin.h>
#include <cuda.h>

#include "air_cuda.h"
#include "air_cpu.h"
#include "air_image_pgm.h"
#include "air_matrix.h"
#include "air_timer.h"

/*------------------------------------------------------------------------------
 * Type Definitions
**----------------------------------------------------------------------------*/

typedef cudaChannelFormatDesc cudaCFDesc;
typedef	texture<float, 2, cudaReadModeElementType> cudaFloat2DTexture;

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

void	air_cuda_set_texture(int, cudaArray*);

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

const size_t AIR_CUDA_FETCH_BLOCK_SIZE   = 16;
const size_t AIR_CUDA_FETCH_BLOCK_SIZE_2 = AIR_CUDA_FETCH_BLOCK_SIZE*2;

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

/* Source and Target Texture Pyramid References */
cudaFloat2DTexture  AIR_SourceTexture;
cudaFloat2DTexture  AIR_TargetTexture;

cudaArray**  AIR_SourceArray;	/* CUDA source arrays */
cudaArray**  AIR_TargetArray;	/* CUDA target arrays */

int*	AIR_SourceSize = NULL;	/* Array of source sizes */
int*	AIR_TargetSize = NULL;	/* Array of target sizes */

float*	AIR_DeviceTemporaryPlane  = NULL;

/*----------------------------------------------------------------------------*/

int	AIR_CurrentPyramidLevel	  = 0;	/* Current pyramid level */

/*----------------------------------------------------------------------------*/

int*	AIR_HostDestinationPlane   = NULL; /* Destination plane on host */
int*	AIR_DeviceDestinationPlane = NULL; /* Destination plane on device */
size_t	AIR_DestinationPitch  = 0; /* Destination plane pitch  */
size_t	AIR_DestinationStride = 0; /* Destination plane stride */

/*------------------------------------------------------------------------------
 * Macros
**----------------------------------------------------------------------------*/

#define air_cuda_is_valid_texel(c, r) \
    (0 <= (c) && (c) < columns && 0 <= (r) && (r) < rows)

#define air_cuda_get_texel(t, c, r) \
    tex2D((t), (c) + 0.5f, (r) + 0.5f)

#define air_cuda_get_texture(t) \
    ((t) == 0 ? AIR_SourceTexture : AIR_TargetTexture)

#define air_cuda_get_src_texel(c, r) \
    air_cuda_get_texel(AIR_SourceTexture, (c), (r))

#define air_cuda_get_tgt_texel(c, r) \
    air_cuda_get_texel(AIR_TargetTexture, (c), (r))

#define air_cuda_mod(a, b) \
    ((a)&((b) - 1))

#define air_cuda_local_element(c, r) \
    SharedBlock[(r)*AIR_CUDA_FETCH_BLOCK_SIZE + (c)]

/*------------------------------------------------------------------------------
 * Device Constant Resources
**----------------------------------------------------------------------------*/

__constant__
float	ConstantMatrix[AIR_MATRIX_SIZE];  /* Transformation Matrix */

/*------------------------------------------------------------------------------
 * Device Shared Resources
**----------------------------------------------------------------------------*/

__shared__
int	SharedBlock[AIR_CUDA_FETCH_BLOCK_SIZE_2];
    
/*------------------------------------------------------------------------------
 * Device Functions
**----------------------------------------------------------------------------*/

__device__
float
air_cuda_cubic(float t, float p0, float p1, float p2, float p3) 
{
    float t2 = t*t;
    float t3 = t2*t;

    return ((p0 * (-(t3 + t)/2.0 + t2)) +
	    (p1 * ((3*t3 - 5*t2)/2.0 + 1)) +
	    (p2 * ((-3*t3 + t)/2.0 + 2.0*t2)) +
	    (p3 * ((t3 - t2)/2.0)));
}

/*----------------------------------------------------------------------------*/

__global__
void
air_cuda_pyramid_kernel(int psize, float* tp, int is_tgt)
{
    int ic  = threadIdx.x;
    int ir  = threadIdx.y;
    
    int tc  = (blockIdx.x * blockDim.x) + ic; /* Target column */
    int tr  = (blockIdx.y * blockDim.y) + ir; /* Target row    */

    int sc  = tc + tc;
    int sr  = tr + tr;

    int sum = 0;
   
    if (!is_tgt) {
	sum = air_cuda_get_texel(AIR_SourceTexture, sc,     sr)     + 
	      air_cuda_get_texel(AIR_SourceTexture, sc,     sr + 1) +
	      air_cuda_get_texel(AIR_SourceTexture, sc + 1, sr)     +
	      air_cuda_get_texel(AIR_SourceTexture, sc + 1, sr + 1);
    } else {
	sum = air_cuda_get_texel(AIR_TargetTexture, sc,     sr)     + 
	      air_cuda_get_texel(AIR_TargetTexture, sc,     sr + 1) +
	      air_cuda_get_texel(AIR_TargetTexture, sc + 1, sr)     +
	      air_cuda_get_texel(AIR_TargetTexture, sc + 1, sr + 1);
    }

    tp[tr * psize + tc] = roundf(sum/4.0);
}

/*----------------------------------------------------------------------------*/

__global__
void
air_cuda_bilinear_transform_kernel(int* dp, size_t dstride, 
				   uint rows, uint columns) 
{
    int ic = threadIdx.x;
    int ir = threadIdx.y;

    int tc = (blockIdx.x * blockDim.x) + ic; /* Target column */
    int tr = (blockIdx.y * blockDim.y) + ir; /* Target row    */
    
    /* Translate coordinates based on transformation matrix */
    float sc = (air_matrix_a(ConstantMatrix)*tc) + /* Source column */
	       (air_matrix_b(ConstantMatrix)*tr) + 
	        air_matrix_e(ConstantMatrix);
    float sr = (air_matrix_c(ConstantMatrix)*tc) + /* Source row */
	       (air_matrix_d(ConstantMatrix)*tr) + 
	        air_matrix_f(ConstantMatrix);

    air_cuda_local_element(ic, ir)  = air_cuda_get_src_texel(sc, sr);
    air_cuda_local_element(ic, ir) -= air_cuda_get_tgt_texel(tc, tr);
    air_cuda_local_element(ic, ir) *= air_cuda_local_element(ic, ir);
    
    __syncthreads();

    if ((ic + ir) == 0) {
	int sum = 0.0;

	for (ir = 0; ir < AIR_CUDA_FETCH_BLOCK_SIZE; ir++)
	    for (ic = 0; ic < AIR_CUDA_FETCH_BLOCK_SIZE; ic++)
		sum += air_cuda_local_element(ic, ir); 

	dp[blockIdx.y * dstride + blockIdx.x] = sum;
    } 
}

/*----------------------------------------------------------------------------*/

__global__
void
air_cuda_bicubic_transform_kernel(int* dp, size_t dstride, 
				uint rows, uint columns) 
{
    int ic = threadIdx.x;
    int ir = threadIdx.y;

    int tc = (blockIdx.x * blockDim.x) + ic; /* Target column */
    int tr = (blockIdx.y * blockDim.y) + ir; /* Target row    */
    
    /* Translate coordinates based on transformation matrix */
    float sc = (air_matrix_a(ConstantMatrix)*tc) + /* Source column */
	       (air_matrix_b(ConstantMatrix)*tr) + 
	        air_matrix_e(ConstantMatrix);
    float sr = (air_matrix_c(ConstantMatrix)*tc) + /* Source row */
	       (air_matrix_d(ConstantMatrix)*tr) + 
	        air_matrix_f(ConstantMatrix);

    float sc_int  = floor(sc);
    float sc_frac = sc - sc_int;
    float sr_int  = floor(sr);
    float sr_frac = sr - sr_int;

    float c0 = air_cuda_cubic(sr_frac,
		    air_cuda_get_src_texel(sc_int-1, sr_int-1),
		    air_cuda_get_src_texel(sc_int-1, sr_int),
		    air_cuda_get_src_texel(sc_int-1, sr_int+1),
		    air_cuda_get_src_texel(sc_int-1, sr_int+2));

    float c1 = air_cuda_cubic(sr_frac,
		    air_cuda_get_src_texel(sc_int,   sr_int-1),
		    air_cuda_get_src_texel(sc_int,   sr_int),
		    air_cuda_get_src_texel(sc_int,   sr_int+1),
		    air_cuda_get_src_texel(sc_int,   sr_int+2));

    float c2 = air_cuda_cubic(sr_frac,
		    air_cuda_get_src_texel(sc_int+1, sr_int-1),
		    air_cuda_get_src_texel(sc_int+1, sr_int),
		    air_cuda_get_src_texel(sc_int+1, sr_int+1),
		    air_cuda_get_src_texel(sc_int+1, sr_int+2));

    float c3 = air_cuda_cubic(sr_frac,
		    air_cuda_get_src_texel(sc_int+2, sr_int-1),
		    air_cuda_get_src_texel(sc_int+2, sr_int),
		    air_cuda_get_src_texel(sc_int+2, sr_int+1),
		    air_cuda_get_src_texel(sc_int+2, sr_int+2));

    air_cuda_local_element(ic, ir)  = air_cuda_cubic(sc_frac, c0, c1, c2, c3);
    air_cuda_local_element(ic, ir) -= air_cuda_get_tgt_texel(tc, tr);
    air_cuda_local_element(ic, ir) *= air_cuda_local_element(ic, ir);

    __syncthreads();

    if ((ic + ir) == 0) {
	int sum = 0;

	for (ir = 0; ir < AIR_CUDA_FETCH_BLOCK_SIZE; ir++)
	    for (ic = 0; ic < AIR_CUDA_FETCH_BLOCK_SIZE; ic++)
		sum += air_cuda_local_element(ic, ir); 

	dp[blockIdx.y * dstride + blockIdx.x] = sum;
    } 
}

/*------------------------------------------------------------------------------
 * Host Functions
**----------------------------------------------------------------------------*/

inline
void
air_cuda_check_error(const char* fn, const char* fc)
{
    cudaError_t err = cudaGetLastError();

    if (err != cudaSuccess)
	air_warn(fn, "%s -> %s", fc, cudaGetErrorString(err));
}

/*----------------------------------------------------------------------------*/

ERR_TYPE 
air_cuda_pyramid(image* ip,uint levels,int** isize,cudaArray*** iarray,int tx)
{
#define AIR_FN "air_cuda_pyramid"
    float*  tfp	= NULL;		/* Temporary float plane */

    int*	psize  = NULL;	/* Pyramid sizes */
    cudaArray**	parray = NULL;	/* Pyramid arrays */
    
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CUDA_PYRAMID);

    /* Initialize image size array */
    if (*isize == NULL) {
	psize = (*isize) = (int*)malloc(sizeof(int)*levels);

	psize[0] = ip->columns; /* HACK: Okay b/c column == rows */

	for (int i = 1; i < levels; i++) 
	    psize[i] = psize[i - 1]/2;

	if (AIR_Debug > 1) 
	    for (int i = 0; i < levels; i++) 
		air_debug(AIR_FN, "psize[%d] = %d", i, psize[i]);
    } else {
	return (ERR_BAD_INPUT);
    }

    /* Initialize image cuda arrays */
    if (*iarray == NULL) {
	parray = *iarray = (cudaArray**)malloc(sizeof(cudaArray*)*levels);

	for (int i = 0; i < levels; i++) {
	    cudaCFDesc tc = cudaCreateChannelDesc<float>();/* Texture channel */
	    cudaMallocArray(&parray[i], &tc, psize[i], psize[i]);
    
	    if (AIR_Assert)
		air_cuda_check_error(AIR_FN, "cudaMallocArray");
	}
    } else {
	return (ERR_BAD_INPUT);
    }

    /* Allocate temporary float plane */
    cudaMallocHost((void**)&tfp, sizeof(float)*psize[0]*psize[0]);
    air_image_copy_to_plane(ip, tfp);
	    
    if (AIR_Assert)
	air_cuda_check_error(AIR_FN, "cudaMallocHost");

    /* Copy temporary float plane to first image cuda array */
    cudaMemcpy2DToArray(parray[0], 0, 0, tfp, 
	    sizeof(float)*psize[0], sizeof(float)*psize[0], 
	    psize[0], cudaMemcpyHostToDevice);
    
    if (AIR_Assert)
	air_cuda_check_error(AIR_FN, "cudaMemcpy2DToArray");
	
    /* Setup device temporary plane if not initialized */
    if (AIR_DeviceTemporaryPlane == NULL) {
	cudaMalloc((void**)&AIR_DeviceTemporaryPlane, 
		   sizeof(float)*psize[0]*psize[0]);
    
	if (AIR_Assert)
	    air_cuda_check_error(AIR_FN, "cudaMalloc");
    }

    /* Generate rest of pyramid */
    for (int plevel = 1; plevel < levels; plevel++) {
	/* Grid and block dimensions */
	dim3   grid(psize[plevel]/AIR_CUDA_FETCH_BLOCK_SIZE,
		    psize[plevel]/AIR_CUDA_FETCH_BLOCK_SIZE);
	dim3   block(AIR_CUDA_FETCH_BLOCK_SIZE,
		     AIR_CUDA_FETCH_BLOCK_SIZE);

	/* Set read texture to previous level */
	air_cuda_set_texture(tx, parray[plevel - 1]);

	/* Perform pyramid kernel */
	air_cuda_pyramid_kernel<<<grid,block>>>(
	    psize[plevel], AIR_DeviceTemporaryPlane, tx);
	
	cudaThreadSynchronize();
	
	if (AIR_Assert)
	    air_cuda_check_error(AIR_FN, "air_cuda_pyramid_kernel");

	/* Copy new pyramid level in temporary plane to current array level */
	cudaMemcpy2DToArray(parray[plevel], 0, 0, AIR_DeviceTemporaryPlane, 
	    sizeof(float)*psize[plevel], sizeof(float)*psize[plevel], 
	    psize[plevel], cudaMemcpyDeviceToDevice);
	
	if (AIR_Assert)
	    air_cuda_check_error(AIR_FN, "cudaMemcpy2DToArray");
    }

    /* Free resources */
    cudaFreeHost(tfp);
    
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CUDA_PYRAMID);

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

inline
void
air_cuda_set_texture(int tx, cudaArray* ta) 
{
    cudaBindTextureToArray(air_cuda_get_texture(tx), ta); 

    air_cuda_get_texture(tx).filterMode     = cudaFilterModeLinear;
    air_cuda_get_texture(tx).addressMode[0] = cudaAddressModeClamp;
    air_cuda_get_texture(tx).addressMode[1] = cudaAddressModeClamp;
}

/*----------------------------------------------------------------------------*/

float
air_cuda_calculate_mse(image* si, image* ti) {
    int*   hdp = AIR_HostDestinationPlane;   /* Destination plane on host */
    int*   ddp = AIR_DeviceDestinationPlane; /* Destination plane on device */
    size_t dpitch = AIR_DestinationPitch;   /* Destination plane pitch  */

    size_t dpcolumns = AIR_SourceSize[AIR_CurrentPyramidLevel] / 
		       AIR_CUDA_FETCH_BLOCK_SIZE;
    size_t dprows    = AIR_SourceSize[AIR_CurrentPyramidLevel] / 
		       AIR_CUDA_FETCH_BLOCK_SIZE;
    float  mse	     = 0.0;

    if (AIR_Time) ctu_timer_start(AIR_TIMER_CUDA_CALCULATE_MSE);

    /* Copy device destination plane to host destination plane */
    cudaMemcpy2D(hdp, sizeof(int)*dpcolumns, 
		 ddp, dpitch, sizeof(int)*dpcolumns, dprows, 
		 cudaMemcpyDeviceToHost);
    
    for (uint r = 0; r < dprows; r++) 
	for (uint c = 0; c < dpcolumns; c++) 
	    mse += hdp[r * dpcolumns + c];
    
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CUDA_CALCULATE_MSE);

    return (mse/(dpcolumns * dprows * AIR_CUDA_FETCH_BLOCK_SIZE_2));
}

/*----------------------------------------------------------------------------*/

inline
void
air_cuda_transform_init(image* si, image* ti, image* di)
{
#define AIR_FN "air_cuda_transform_init"
    int*   hdp = AIR_HostDestinationPlane;   /* Destination plane on device */
    int*   ddp = AIR_DeviceDestinationPlane; /* Destination plane on host */
    size_t dpitch  = AIR_DestinationPitch;   /* Destination plane pitch  */
    size_t dstride = AIR_DestinationStride;  /* Destination plane stride */

    size_t columns = AIR_SourceSize[AIR_CurrentPyramidLevel]; /* HACK */
    size_t rows    = AIR_SourceSize[AIR_CurrentPyramidLevel]; /* HACK */

    size_t dpcolumns = columns / AIR_CUDA_FETCH_BLOCK_SIZE;
    size_t dprows    = rows / AIR_CUDA_FETCH_BLOCK_SIZE;

    if (AIR_Time) ctu_timer_start(AIR_TIMER_CUDA_TRANSFORM_INIT);
    
    /* If source image is not loaded yet, copy to texture, and bind */
    if (ddp) cudaFree(ddp);
    if (hdp) free(hdp);

    /* Set source and target texture bindings */
    air_cuda_set_texture(0, AIR_SourceArray[AIR_CurrentPyramidLevel]);
    air_cuda_set_texture(1, AIR_TargetArray[AIR_CurrentPyramidLevel]);

    /* Allocate destination plane */
    hdp = (int*)malloc(sizeof(int)*dpcolumns*dprows);
    cudaMallocPitch((void**)&ddp, &dpitch, sizeof(int)*dpcolumns, dprows);
    dstride = dpitch / sizeof(int);

    if (AIR_Assert)
	air_cuda_check_error(AIR_FN, "cudaMallocPitch");

    /* Save global variables */
    AIR_HostDestinationPlane   = hdp;
    AIR_DeviceDestinationPlane = ddp;
    AIR_DestinationPitch  = dpitch;
    AIR_DestinationStride = dstride;
    
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CUDA_TRANSFORM_INIT);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE 
air_cuda_transform(image* si, image* ti, image* di, float* m)
{
#define AIR_FN "air_cuda_transform"
    int*   ddp = AIR_DeviceDestinationPlane; /* Destination plane on device */
    size_t dstride = AIR_DestinationStride;  /* Destination plane stride */
    
    size_t columns = AIR_SourceSize[AIR_CurrentPyramidLevel]; /* HACK */
    size_t rows    = AIR_SourceSize[AIR_CurrentPyramidLevel]; /* HACK */

    dim3   grid(columns/AIR_CUDA_FETCH_BLOCK_SIZE,  /* Grid dimensions */
	        rows/AIR_CUDA_FETCH_BLOCK_SIZE);
    dim3   block(AIR_CUDA_FETCH_BLOCK_SIZE,	    /* Block dimensions */
		 AIR_CUDA_FETCH_BLOCK_SIZE);

    /* Start timer */
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CUDA_TRANSFORM);

    /* Copy transformation matrix coefficients */
    cudaMemcpyToSymbol(ConstantMatrix, m, sizeof(float)*AIR_MATRIX_SIZE);

    /* Execute transformation kernel */
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CUDA_TRANSFORM_KERNEL);

    switch (AIR_TransformKernel) {
	case AIR_BILINEAR_TRANSFORM_KERNEL:
	    if (AIR_Debug > 1)
		air_debug(AIR_FN, "executing bilinear kernel");

	    air_cuda_bilinear_transform_kernel<<<grid, block>>>(ddp, dstride, 
								rows, columns);

	    if (AIR_Assert)
		air_cuda_check_error(AIR_FN, 
				     "air_cuda_bilinear_transform_kernel");
	    break;

	case AIR_BICUBIC_TRANSFORM_KERNEL:
	    if (AIR_Debug > 1)
		air_debug(AIR_FN, "executing bicubic kernel");

	    air_cuda_bicubic_transform_kernel<<<grid, block>>>(ddp, dstride, 
							       rows, columns);

	    if (AIR_Assert)
		air_cuda_check_error(AIR_FN, 
				     "air_cuda_bicubic_transform_kernel");
	    break;

	default:
	    air_warn(AIR_FN, "Unknown transform kernel type(%d)", 
			     AIR_TransformKernel);
	    break;
    }

    cudaThreadSynchronize();

    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CUDA_TRANSFORM_KERNEL);
    
    /* Stop timer */
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CUDA_TRANSFORM);

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

double
air_cuda_evaluate(const gsl_vector* v, void* params) 
{
#define AIR_FN "air_cuda_evaluate"
    image** iv = (image**)params;
    image*  si = iv[0];
    image*  ti = iv[1];
    image*  di = iv[2];

    matrix    m;
    parameter p;
    float     mse;
    int	      ssize;
    
    /* Copy parameters from vector into local parameter array */
    air_parameter_dx(p) = gsl_vector_get(v, 0);
    air_parameter_dy(p) = gsl_vector_get(v, 1);
    air_parameter_sx(p) = gsl_vector_get(v, 2);
    air_parameter_sy(p) = gsl_vector_get(v, 3);
    air_parameter_angle(p) = gsl_vector_get(v, 4);

    /* Compute the transformation matrix based on the parameters */
    if (AIR_Debug > 1)
	air_debug(AIR_FN, "AIR_CurrentPyramidLevel = %d", 
		  AIR_CurrentPyramidLevel);

    ssize = AIR_SourceSize[AIR_CurrentPyramidLevel];
	
    if (AIR_Debug > 1) 
	air_debug(AIR_FN, "ssize = %d", ssize);

    air_matrix_compute(m, ssize/2, ssize/2, p); 

    /* Transform the source image using the computed matrix */
    air_cuda_transform(si, ti, di, m);

    /* Calculate the mean square error of the new image and the target image */
    mse = air_cuda_calculate_mse(ti, di);

    if (AIR_Debug > 1) {
	float psnr = air_cpu_calculate_psnr(NULL, NULL, AIR_PGM_MAX_VALUE, mse);

	fprintf(CTU_DebugStream, "[D] %s: ", AIR_FN);
	air_parameter_write_stream(CTU_DebugStream, p);
	air_debug(AIR_FN, "mse = %lf, psnr = %lf", mse, psnr);
    }

    return (mse);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_cuda_register(image* si, image* ti, int levels, func_eval fe)
{
    double* p;

    float   mse  = 0.0;
    float   psnr = 0.0;

    /* Start timer */
    if (AIR_Time) ctu_timer_start(AIR_TIMER_CUDA_REGISTER);

    /* Create parameter set */
    p = (double*)malloc(sizeof(double)*AIR_PARAMETER_SIZE);
    air_parameter_dx(p) = 0.0;
    air_parameter_dy(p) = 0.0;
    air_parameter_sx(p) = 1.0;
    air_parameter_sy(p) = 1.0;
    air_parameter_angle(p) = 0.0;

    /* Create pyramids */
    air_cuda_pyramid(si, levels, &AIR_SourceSize, &AIR_SourceArray, 0);
    air_cuda_pyramid(ti, levels, &AIR_TargetSize, &AIR_TargetArray, 1);

    /* For each level of the pyramid perform optimization */
    for (int i = levels; i > 0; i--) {
	AIR_CurrentPyramidLevel = i - 1;
    
	air_cuda_transform_init(NULL, NULL, NULL);

	/* Perform optimization using last set of parameters */
	mse = air_optimize(NULL, NULL, NULL, (double**)&p, fe);
    }
    
    /* Output results */
    psnr = air_cpu_calculate_psnr(NULL, NULL, AIR_PGM_MAX_VALUE, mse);

    printf("[ cda ] "); air_parameter_write_stream(stdout, p);
    printf("[ cda ] mse = %f, psnr = %f\n", mse, psnr);

    /* Free resources */
    free(p);
    
    /* Stop timer */
    if (AIR_Time) ctu_timer_stop(AIR_TIMER_CUDA_REGISTER);

    return (ERR_SUCCESS);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
