#!/usr/bin/env python

#-------------------------------------------------------------------------------
# SConscript: accelerated image registration
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <pbui@cse.nd.edu>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

import os

Import('env uninstall')

#-------------------------------------------------------------------------------
# Configuration
#-------------------------------------------------------------------------------

air_common_src = [
    'air_common.c', 
    'air_cpu.c', 
    'air_error.c', 
    'air_filter.c', 
    'air_image.c', 
    'air_image_pgm.c', 
    'air_image_raw.c', 
    'air_matrix.c', 
    'air_optimize.c', 
    'air_timer.c', 
]

air_env	= env.Clone()

air_cflags  = ['-std=c99', '-Wall']
air_libs    = ['ctu', 'air', 'gsl', 'gslcblas']
air_libpath = [air_env.build_dir, air_env.build_lib_dir] 

air_env.Append(LIBS    = air_libs)
air_env.Append(LIBPATH = air_libpath)

if int(ARGUMENTS.get('with_cuda', 0)):
    air_env.Append(CFLAGS  = ['-DAIR_WITH_CUDA'])
    air_env.Append(LIBS    = ['aircuda', 'cudart'])
    air_env.Append(LIBPATH = ['/usr/local/cuda/lib'])

#-------------------------------------------------------------------------------
# Targets
#-------------------------------------------------------------------------------

# Executables

air_compare   = air_env.Program(target = 'air_compare', 
			        source = 'air_compare.c',
				CFLAGS = air_env['CFLAGS'] + air_cflags)

air_register  = air_env.Program(target = 'air_register', 
			        source = 'air_register.c',
				CFLAGS = air_env['CFLAGS'] + air_cflags)

air_transform = air_env.Program(target = 'air_transform', 
			        source = 'air_transform.c',
				CFLAGS = air_env['CFLAGS'] + air_cflags)

air_env.Alias('build', [air_compare, air_register, air_transform])

#-------------------------------------------------------------------------------

# Libraries

if int(ARGUMENTS.get('with_cuda', 0)):
    air_cuda = air_env.Library(target = 'aircuda', source = 'air_cuda.cu')

    air_env.Depends(air_register, air_cuda)
    air_env.Depends(air_transform, air_cuda)

air_common = air_env.Library(target = 'air', 
			     source = air_common_src,
			     CFLAGS = air_env['CFLAGS'] + air_cflags)

air_env.Depends(air_compare, air_common)
air_env.Depends(air_register, air_common)
air_env.Depends(air_transform, air_common)

#-------------------------------------------------------------------------------

air_bin = air_env.Install(air_env.dest_bin_dir, [air_register, air_transform]) 

air_env.Alias('install', [air_bin])
air_env.Alias('uninstall', air_env.Command('#u', "#SConstruct", uninstall))

air_env.uninstall_files = air_bin

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
