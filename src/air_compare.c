/*------------------------------------------------------------------------------
 * air_compare.c: air compare program
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
 *------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <getopt.h>
#include <unistd.h>

#include <ctu.h>

#include "air.h"

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

bool AIR_PrintRaw = false;

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

void air_compare_init(int, char*[], char**, char**);
void air_compare_usage(int, char*[]);

/*------------------------------------------------------------------------------
 * Functions
**----------------------------------------------------------------------------*/

int	    
main(int argc, char* argv[]) {
    char*   sf;
    image*  si;
    
    char*   tf;
    image*  ti;

    float   mse;
    float   psnr;

    air_compare_init(argc, argv, &sf, &tf);

    air_image_read(sf, &si);
    air_image_read(tf, &ti);

    if (AIR_PrintRaw) {
	air_image_raw_write_stream(stdout, &si);
	air_image_raw_write_stream(stdout, &ti);
    }

    mse  = air_cpu_calculate_mse(si, ti);
    psnr = air_cpu_calculate_psnr(si, ti, AIR_PGM_MAX_VALUE, mse);

    printf("%s, %s: mse = %2.4f, psnr = %2.4f\n", sf, tf, mse, psnr);

    air_image_free(si);
    air_image_free(ti);

    return (EXIT_SUCCESS);
}

/*----------------------------------------------------------------------------*/

void
air_compare_init(int argc, char* argv[], char** sf, char** tf) {
    char c;

    CTU_DebugStream = CTU_ErrorStream = stderr;

    while ((c = getopt(argc, argv, "ard:h")) != -1) {
	switch (c) {
	    case 'a':
		AIR_Assert = true;
		break;
	    
	    case 'r':
		AIR_PrintRaw = true;
		break;
	    
	    case 'd':
		AIR_Debug = strtol(optarg, NULL, 10);
		break;
	    
	    case 'h':
	    default:
		air_compare_usage(argc, argv);
		break;
	}
    }

    if (argc - optind != 2) 
	air_compare_usage(argc, argv);

    *sf = argv[optind];
    *tf = argv[optind+1];
}

/*----------------------------------------------------------------------------*/

void
air_compare_usage(int argc, char* argv[]) {
    fprintf(stderr, "usage: %s [options] source target\n\n", argv[0]);
    fprintf(stderr, "\nGeneral Options:\n");
    fprintf(stderr, "  -a                  Check assertions\n");
    fprintf(stderr, "  -h                  Show this help message\n");
    fprintf(stderr, "  -r                  Print raw pixel values\n");
    fprintf(stderr, "  -d <Level>          Show debug level messages\n");
    exit(EXIT_FAILURE);
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
