/*------------------------------------------------------------------------------
 * air_image_pgm.c: air portable grayscale media (pgm) image support
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "air_image_pgm.h"

#include "air_error.h"
#include "air_image.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

const char* AIR_PGM_MAGIC_NUMBER[] = {
    "P2",
    "P5",
    NULL,
};

const uint AIR_PGM_BUFFER_SIZE = 4096;
const uint AIR_PGM_MAX_VALUE   = 255;

/*------------------------------------------------------------------------------
 * Macros
**----------------------------------------------------------------------------*/

#define AIR_IMAGE_PGM_READ_NEXT_TOKEN(fn, fs, buffer, bufptr, token) \
    if (!bufptr || *bufptr == 0) \
	bufptr = air_image_pgm_read_line(fs, (char*)&buffer); \
    if (AIR_Assert) \
	air_assert(AIR_ERROR, fn, bufptr != NULL, ERR_ERRNO); \
    if (AIR_Debug > 1) \
	air_debug(fn, "bufptr = (%s)", bufptr); \
    bufptr = air_image_pgm_get_token(bufptr, (char*)&token); \
    if (AIR_Debug > 1) \
	air_debug(fn, "token = (%s)", (char*)&token);

/*------------------------------------------------------------------------------
 * Functions 
**----------------------------------------------------------------------------*/

char*
air_image_pgm_get_token(char* buffer, char* token) {
    uint slen;
    char*  bufptr;

    if (*buffer == '#') 
	goto aipgt_fail;

    for (slen = 0, bufptr = buffer; isalnum(*bufptr++); slen++) ;
    
    if (slen == 0) 
	goto aipgt_fail;

    strncpy(token, buffer, slen);
    token[slen] = 0;

    if (AIR_Debug > 1)
	air_debug("air_image_pgm_get_token", "bufptr = (%s)", bufptr);

    return (bufptr);

aipgt_fail:
    token[0] = 0;
    return (NULL);
}

/*----------------------------------------------------------------------------*/

char*
air_image_pgm_skip_space(char* bufptr) {
    while (*bufptr != 0 && isspace(*bufptr++)) ;

    if (AIR_Debug > 1)
	air_debug("air_image_pgm_skip_space", "bufptr = (%s)", bufptr - 1);

    return (*bufptr == 0 ? NULL : --bufptr);
}

/*----------------------------------------------------------------------------*/

char*
air_image_pgm_read_line(FILE* fs, char* buffer) {
    char* bufptr;

    bufptr = fgets(buffer, AIR_PGM_BUFFER_SIZE, fs);

    if (bufptr) bufptr[strlen(bufptr) - 1] = 0;

    return (bufptr);
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_pgm_read(const char* fn, image** img) {
#define AIR_FN "air_image_pgm_read"
    FILE* fs;
    
    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, fn && img, ERR_NULL_INPUT);

    fs = fopen(fn, "r");

    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, fs, ERR_ERRNO);

    air_image_pgm_read_stream(fs, img);
    fclose(fs);
    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_pgm_read_stream(FILE* fs, image** img) {
#define AIR_FN "air_image_pgm_read_stream"
    typedef enum {
	AIPRS_MAGIC_NUMBER,
	AIPRS_COLUMNS,
	AIPRS_ROWS,
	AIPRS_MAX_VALUE,
	AIPRS_PIXELS,
	AIPRS_WHITE_SPACE,
	AIPRS_FINISHED,
    } AIPRS_STATE;

    const char*	AIPRS_STATE_STRING[] = {
	"Magic Number",
	"Columns",
	"Rows",
	"Max Value",
	"Pixels",
	"White Space",
	"Finished",
    };

    AIPRS_STATE cstate;
    AIPRS_STATE nstate;

    image* im;

    char  buffer[AIR_PGM_BUFFER_SIZE];
    char  token[AIR_PGM_BUFFER_SIZE];
    char* bufptr;

    uint  columns;
    uint  rows;
    uint  magic_number;
    uint  max_value; 

    cstate = AIPRS_MAGIC_NUMBER;
    nstate = AIPRS_MAGIC_NUMBER;
    bufptr = NULL;
    magic_number = 0;
    im     = NULL;
    
    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, fs, ERR_NULL_INPUT);

    while (cstate != AIPRS_FINISHED) {
	if (AIR_Debug > 1)
	    air_debug(AIR_FN, "cstate = (%s)", AIPRS_STATE_STRING[cstate]);

	switch (cstate) {
	    case AIPRS_MAGIC_NUMBER:
		AIR_IMAGE_PGM_READ_NEXT_TOKEN(AIR_FN, fs, buffer, bufptr, token)

		if (bufptr) {
		    for (uint i = 0; AIR_PGM_MAGIC_NUMBER[i] != NULL; i++) {
			uint  slen = strlen(AIR_PGM_MAGIC_NUMBER[i]);
			char* sptr = (char*)AIR_PGM_MAGIC_NUMBER[i];

			if (!strncmp(sptr, token, slen)) {
			    cstate = AIPRS_WHITE_SPACE;
			    nstate = AIPRS_COLUMNS;
			    magic_number = strtol(&sptr[1], NULL, 10);
			}
		    }
		}
		break;

	    case AIPRS_COLUMNS:
		AIR_IMAGE_PGM_READ_NEXT_TOKEN(AIR_FN, fs, buffer, bufptr, token)

		if (bufptr) {
		    sscanf(token, "%d", &columns);
		    cstate = AIPRS_WHITE_SPACE;
		    nstate = AIPRS_ROWS;
		}
		break;

	    case AIPRS_ROWS:
		AIR_IMAGE_PGM_READ_NEXT_TOKEN(AIR_FN, fs, buffer, bufptr, token)
		
		if (bufptr) {
		    sscanf(token, "%d", &rows);
		    cstate = AIPRS_WHITE_SPACE;
		    nstate = AIPRS_MAX_VALUE;
		}
		break;

	    case AIPRS_MAX_VALUE:
		AIR_IMAGE_PGM_READ_NEXT_TOKEN(AIR_FN, fs, buffer, bufptr, token)
		
		if (bufptr) {
		    sscanf(token, "%u", &max_value);
		    cstate = AIPRS_WHITE_SPACE;
		    nstate = AIPRS_PIXELS;
		}
		break;

	    case AIPRS_PIXELS:
		im = air_image_alloc(columns, rows);
			
		for (uint r = 0; r < rows; r++) {
		    if (magic_number == 2) /* P2 */
			for (uint c = 0; c < columns; c++) 
			    fscanf(fs, "%u", (uint*)&(air_image_pixel(im,c,r)));
		    else /* P5 */
			fread(&(im->pixels[r*columns]),sizeof(byte),columns,fs);
			
		    /* Normalize values */
		    for (uint c = 0; c < columns; c++) {
			float p_value = air_image_pixel(im, c, r);
			p_value *= AIR_PGM_MAX_VALUE;
			p_value /= max_value;
			air_image_pixel(im, c, r) = rintf(p_value);
		    }
		}

		cstate = AIPRS_FINISHED;
		break;

	    case AIPRS_WHITE_SPACE:
		if (!bufptr) 
		    bufptr = air_image_pgm_read_line(fs, (char*)&buffer);

		bufptr = air_image_pgm_skip_space(bufptr);
		cstate = nstate;
		break;

	    default:
		break;
	}
    }

    (*img) = im;

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_pgm_write(const char* fn, image** img) {
#define AIR_FN "air_image_pgm_write"
    FILE* fs;

    if (AIR_Assert)
	air_assert(AIR_ERROR, AIR_FN, fn, ERR_NULL_INPUT);

    fs = fopen(fn, "w+");

    if (AIR_Assert)
	air_assert(AIR_ERROR, AIR_FN, fs, ERR_ERRNO);

    air_image_pgm_write_stream(fs, img);
    fclose(fs);

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_pgm_write_stream(FILE* fs, image** img) {
#define AIR_FN "air_image_pgm_write_stream"
    image* im;

    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, fs, ERR_NULL_INPUT);

    im = (*img);

    fprintf(fs, "%s\n", AIR_PGM_MAGIC_NUMBER[1]);
    fprintf(fs, "# AIR PGM HANDLER\n");
    fprintf(fs, "%u %u\n", im->columns, im->rows);
    fprintf(fs, "%u\n", AIR_PGM_MAX_VALUE);

    for (uint r = 0; r < im->rows; r++) 
	fwrite(&(im->pixels[r*im->columns]), sizeof(byte), im->columns, fs);

    return (ERR_SUCCESS);
#undef AIR_FN
}

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
