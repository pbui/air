/*------------------------------------------------------------------------------
 * air_image.c: air image data structure
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
 *------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <string.h>

#include "air_error.h"
#include "air_image.h"
#include "air_image_pgm.h"
#include "air_image_raw.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

static const image_handler
AIR_IMAGE_HANDLERS[] = {
    { "pgm", 
      air_image_pgm_read, 
      air_image_pgm_write, 
      air_image_pgm_read_stream, 
      air_image_pgm_write_stream
    },
    { "raw", 
      NULL,
      NULL,
      NULL,
      air_image_raw_write_stream
    },
    { NULL, NULL, NULL, NULL, NULL },
};

/*------------------------------------------------------------------------------
 * Macros
**----------------------------------------------------------------------------*/
    
#define AIR_IMAGE_GET_HANDLER(_op, _fn, _img, _h) \
    if (AIR_Assert) \
	air_assert(AIR_WARN, (_op), (_fn) && (_img), ERR_NULL_INPUT); \
    (_h) = air_image_get_handler(_fn); \
    if (AIR_Assert) \
	air_assert(AIR_ERROR, (_op), (_h), ERR_NO_HANDLER);

/*------------------------------------------------------------------------------
 * Functions 
**----------------------------------------------------------------------------*/

image*
air_image_alloc(uint columns, uint rows) {
#define AIR_FN "air_image_alloc"
    image* img;

    img = malloc(sizeof(image));

    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, img, ERR_ERRNO);

    img->columns = columns;
    img->rows    = rows;

    img->pixels = malloc(sizeof(byte) * img->rows * img->columns);
    
    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, img->pixels, ERR_ERRNO);

    return (img);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

image*
air_image_free(image* img) {
#define AIR_FN "air_image_free"
    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, img, ERR_NULL_INPUT);

    if (img->pixels) free(img->pixels);
    free(img);

    return (NULL);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

image_handler*
air_image_get_handler(const char* fn) {
#define AIR_FN "air_image_get_handler"
    char* ext;
    image_handler* handler;

    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, fn, ERR_NULL_INPUT);
    
    ext = strrchr(fn, '.'); 
    ext = (ext ? ext + 1 : (char*)fn);

    if (AIR_Debug > 1)
	air_debug(AIR_FN, "ext is (%s)", ext);

    handler = (image_handler*)AIR_IMAGE_HANDLERS;

    while (handler->extension != NULL) {
	if (strncmp(ext, handler->extension, strlen(ext)) == 0) 
	    return (handler);
	handler++;
    }
    
    return (NULL);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_read(const char* fn, image** img) {
    image_handler* handler;

    AIR_IMAGE_GET_HANDLER("air_image_read", fn, img, handler)

    return (handler->read(fn, img));
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_write(const char* fn, image** img) {
    image_handler* handler;

    AIR_IMAGE_GET_HANDLER("air_image_write", fn, img, handler)

    return (handler->write(fn, img));
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_read_stream(FILE* fs, const char* ext, image** img) {
    image_handler* handler;

    AIR_IMAGE_GET_HANDLER("air_image_read_stream", ext, img, handler)

    return (handler->read_stream(fs, img));
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_write_stream(FILE* fs, const char* ext, image** img) {
    image_handler*  handler;

    AIR_IMAGE_GET_HANDLER("air_image_write_stream", ext, img, handler)

    return (handler->write_stream(fs, img));
}

/*----------------------------------------------------------------------------*/

float*
air_image_alloc_plane(int columns, int rows)
{
#define AIR_FN "air_image_alloc_plane"
    float* plane;

    plane = (float*)malloc(sizeof(float)*rows*columns);

    if (AIR_Assert) air_assert(AIR_ERROR, AIR_FN, plane, ERR_ERRNO);

    return (plane);
#undef AIR_FN
}

/*----------------------------------------------------------------------------*/

ERR_TYPE
air_image_copy_to_plane(image* si, float* dp)
{   
    for (size_t r = 0; r < si->rows; r++) 
	for (size_t c = 0; c < si->columns; c++) 
	    air_image_plane_value(dp,si,c,r) = (float)air_image_pixel(si,c,r);

    return (ERR_SUCCESS);
}

/*----------------------------------------------------------------------------*/
    
/* TODO: Clamp? */

ERR_TYPE
air_image_copy_from_plane(image* di, float* sp)
{   
    for (size_t r = 0; r < di->rows; r++) 
	for (size_t c = 0; c < di->columns; c++) 
	    air_image_pixel(di, c, r) = air_image_plane_value(sp, di, c, r);

    return (ERR_SUCCESS);
} 

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=cpp 
**----------------------------------------------------------------------------*/
