/*------------------------------------------------------------------------------
 * air_timer.h: air timer functions
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#ifndef	__AIR_TIMER_H__
#define __AIR_TIMER_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

enum {
    AIR_TIMER_REGISTER,
    AIR_TIMER_CPU_REGISTER,
    AIR_TIMER_CPU_PYRAMID,
    AIR_TIMER_CPU_TRANSFORM,
    AIR_TIMER_CPU_CALCULATE_MSE,
    AIR_TIMER_OPTIMIZE,
    AIR_TIMER_CUDA_REGISTER,
    AIR_TIMER_CUDA_PYRAMID,
    AIR_TIMER_CUDA_TRANSFORM_INIT,
    AIR_TIMER_CUDA_TRANSFORM,
    AIR_TIMER_CUDA_TRANSFORM_KERNEL,
    AIR_TIMER_CUDA_CALCULATE_MSE,
    AIR_TIMERS_SIZE
};

extern const char* AIR_TIMER_STRINGS[];

/*------------------------------------------------------------------------------
 * Global Variables
**----------------------------------------------------------------------------*/

extern bool AIR_Time;

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
