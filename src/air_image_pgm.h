/*------------------------------------------------------------------------------
 * air_image_pgm.h: air portable grayscale media (pgm) image support
**----------------------------------------------------------------------------*/

/* Copyright (c) 2008 Peter Bui. All Rights Reserved.
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from the
 * use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software in a
 * product, an acknowledgment in the product documentation would be appreciated
 * but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.  
 *
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * Peter Bui <pbui@cse.nd.edu>
 *
**----------------------------------------------------------------------------*/

#ifndef	__AIR_IMAGE_PGM_H__
#define	__AIR_IMAGE_PGM_H__

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------------
 * Includes
**----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include "air_error.h"
#include "air_image.h"

/*------------------------------------------------------------------------------
 * Constants
**----------------------------------------------------------------------------*/

extern const uint AIR_PGM_MAX_VALUE;

/*------------------------------------------------------------------------------
 * Function Prototypes
**----------------------------------------------------------------------------*/

char*	 air_image_pgm_get_token(char*, char*);
char*	 air_image_pgm_skip_space(char*);
char*	 air_image_pgm_read_line(FILE*, char*);

ERR_TYPE air_image_pgm_read(const char*, image**);
ERR_TYPE air_image_pgm_read_stream(FILE*, image**);
ERR_TYPE air_image_pgm_write(const char*, image**);
ERR_TYPE air_image_pgm_write_stream(FILE*, image**);

/*----------------------------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif

/*------------------------------------------------------------------------------
 * vim: sts=4 sw=4 ts=8 ft=c
**----------------------------------------------------------------------------*/
