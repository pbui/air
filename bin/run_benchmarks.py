#!/usr/bin/env python

#-------------------------------------------------------------------------------
# run_benchmarks.py: data mine
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <peter.j.bui@gmail.com>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

import os
import pickle
import re
import sys
import time

from glob import glob

from config import *
from common import *

#-------------------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------------------

def get_log_dir(log_dir, mtime=None):
    mtime = time.localtime(mtime)
    mtime = '%04d%02d%02d' % (mtime.tm_year, mtime.tm_mon, mtime.tm_mday)
	
    new_log_dir = '.'.join([log_dir, mtime])
    new_log_dir = '.'.join([new_log_dir, str(len(glob(new_log_dir + '*')))])

    return new_log_dir

def run_cpu_benchmarks(log_dir):
    for i in AIR_CPU_LEVELS:
	os.system('cd %s; scons -Q with_opt_level=%d' % (AIR_BASE_DIR, i))

	for image in AIR_IMAGES:
	    src = image + '.pgm'
	    src = os.path.join(AIR_PGM_DIR, src)

	    for transformation in AIR_TRANSFORMATIONS:
		tgt = image + '_' + transformation + '.pgm'
		tgt = os.path.join(AIR_PGM_DIR, tgt)

		log = '_'.join(['cpu', str(i), image, transformation + '.log'])
		log = os.path.join(log_dir, log)

		print 'cpu %d registering %s (%s)' % (i, image, transformation)

		os.system('%s -i 0 -t %s %s | tee %s' % 
			 (AIR_REGISTER, src, tgt, log))

def run_cuda_benchmarks(log_dir):
    os.system('cd %s; scons -Q with_opt_level=%d with_cuda=1' % 
	      (AIR_BASE_DIR, AIR_CPU_LEVELS[-1]))

    for i in AIR_CUDA_KERNELS:
	for image in AIR_IMAGES:
	    src = image + '.pgm'
	    src = os.path.join(AIR_PGM_DIR, src)

	    for transformation in AIR_TRANSFORMATIONS:
		tgt = image + '_' + transformation + '.pgm'
		tgt = os.path.join(AIR_PGM_DIR, tgt)

		log = '_'.join(['cuda', str(i), image, transformation + '.log'])
		log = os.path.join(log_dir, log)
		
		print 'cuda %d registering %s (%s)' % (i, image, transformation)

		os.system('%s -i 1 -k %d -t %s %s | tee %s' % 
			 (AIR_REGISTER, i, src, tgt, log))

#-------------------------------------------------------------------------------
# Main Execution
#-------------------------------------------------------------------------------

if __name__ == '__main__':
    if os.path.exists(AIR_LOG_DIR):
	new_log_dir = get_log_dir(AIR_LOG_DIR, os.stat(AIR_LOG_DIR).st_mtime)
	os.system('mv %s %s' % (AIR_LOG_DIR, new_log_dir))
    
    log_dir = get_log_dir(AIR_LOG_DIR) 
    os.mkdir(log_dir)    

    run_cuda_benchmarks(log_dir)
    run_cpu_benchmarks(log_dir)

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
