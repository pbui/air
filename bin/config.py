#!/usr/bin/env python

#-------------------------------------------------------------------------------
# config.py: configuration
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <peter.j.bui@gmail.com>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

import os
import sys 

from glob import glob

#-------------------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------------------

AIR_BASE_DIR  = os.path.join(os.path.dirname(sys.argv[0]), '..')

AIR_DATA_DIR  = os.path.join(AIR_BASE_DIR, 'data')
AIR_PGM_DIR   = os.path.join(AIR_DATA_DIR, 'pgm')

AIR_TMP_DIR   = os.path.join(AIR_BASE_DIR, 'tmp')
AIR_LOG_DIR   = os.path.join(AIR_TMP_DIR,  'log')
AIR_PLOT_DIR  = os.path.join(AIR_TMP_DIR,  'plot')

AIR_BUILD_DIR = os.path.join(AIR_BASE_DIR, 'build')
AIR_REGISTER  = os.path.join(AIR_BUILD_DIR, 'air_register')
AIR_TRANSFORM = os.path.join(AIR_BUILD_DIR, 'air_transform')

#-------------------------------------------------------------------------------

AIR_DATA_FILE = os.path.join(AIR_TMP_DIR, 'data.pickle')

#-------------------------------------------------------------------------------

AIR_CPU_LEVELS   = range(0, 4)
AIR_CPU_ALIASES  = [ 'O' + str(l) for l in AIR_CPU_LEVELS ]
#AIR_CPU_ALIASES  = ['CPU']
#AIR_CPU_LEVELS   = [3]

AIR_CUDA_ALIASES = ['Bilinear', 'Bicubic']
AIR_CUDA_KERNELS = range(0, 2)

AIR_ALIASES = AIR_CPU_ALIASES + AIR_CUDA_ALIASES

#-------------------------------------------------------------------------------

AIR_TIME_AVERAGE = 0
AIR_TIME_TOTAL   = 1
AIR_TIME_RUNS    = 2

#-------------------------------------------------------------------------------

AIR_CUDA_NPROCS	 = 128

#-------------------------------------------------------------------------------

AIR_IMAGES = [ os.path.basename(s).split('.')[0] 
		for s in glob(AIR_PGM_DIR + '/*.pgm') if not '_' in s ]

AIR_TRANSFORMATIONS = [ 'dx_05', 'rot_05', 'scale_1.5' ]
AIR_TRANSFORMATIONS_FLAGS = [ '-x 5', '-r 5', '-s 1.5' ]

AIR_CPU_IMPLEMENTATIONS  = map(lambda i: "cpu " + str(i), AIR_CPU_LEVELS)
AIR_CUDA_IMPLEMENTATIONS = map(lambda i: "cuda " + str(i), AIR_CUDA_KERNELS) 
AIR_IMPLEMENTATIONS      = AIR_CPU_IMPLEMENTATIONS + AIR_CUDA_IMPLEMENTATIONS

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
