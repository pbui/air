#!/usr/bin/env python

#-------------------------------------------------------------------------------
# summary.py: print summary 
#-------------------------------------------------------------------------------

# Copyright (c) 2008 Peter Bui. All Rights Reserved.

# This software is provided 'as-is', without any express or implied warranty.
# In no event will the authors be held liable for any damages arising from the
# use of this software.

# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:

# 1. The origin of this software must not be misrepresented; you must not claim
# that you wrote the original software. If you use this software in a product,
# an acknowledgment in the product documentation would be appreciated but is
# not required.

# 2. Altered source versions must be plainly marked as such, and must not be
# misrepresented as being the original software.  

# 3. This notice may not be removed or altered from any source distribution.

# Peter Bui <peter.j.bui@gmail.com>

#-------------------------------------------------------------------------------
# Imports
#-------------------------------------------------------------------------------

from __future__ import with_statement

import copy
import pickle
import sys

from config import *
from common import *

#-------------------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------------------

FIELDS = ['psnr','runtime','su3','karp_flatt','efficiency3','transform_time']
FIELD_WIDTH = 10

#-------------------------------------------------------------------------------
# Main Execution
#-------------------------------------------------------------------------------

if __name__ == '__main__':
    df = AIR_DATA_FILE + '.average'
    dm = None

    if len(sys.argv) >= 2:
        df = sys.argv[1]

    with open(df, 'r') as fs:
	dm = pickle.load(fs)

    for image in get_images_by_size(dm):
	for transformation in AIR_TRANSFORMATIONS + ['avg']:
	    print ':: %s (%s)' % (image, transformation)

	    print 'imp'.center(FIELD_WIDTH - 4, ' '),
	    for f in FIELDS:
		print f.center(FIELD_WIDTH, ' '),
	    print

	    for implementation in AIR_IMPLEMENTATIONS:
		dm_it  = dm[image]['transformations'][transformation]
		dm_iti = dm_it[implementation]
		
		if transformation != 'avg':
		    if not 'avg' in dm[image]['transformations']:
			dm[image]['transformations']['avg'] = {}
			for i in AIR_IMPLEMENTATIONS:
			    dm[image]['transformations']['avg'][i] = {}

		    dm_at  = dm[image]['transformations']['avg']
		    dm_ati = dm_at[implementation]

		print implementation.ljust(FIELD_WIDTH - 4, ' '), 

		for f in FIELDS:
		    if transformation != 'avg':
			if f in dm_ati:
			    dm_ati[f] += dm_iti[f]
			else:
			    dm_ati[f]  = dm_iti[f]
		    else:
			dm_iti[f] /= len(AIR_TRANSFORMATIONS)
		    print ('%.4f' % dm_iti[f]).rjust(FIELD_WIDTH, ' '),
		print

#-------------------------------------------------------------------------------
# vim: sts=4 sw=4 ts=8 ft=python
#-------------------------------------------------------------------------------
