CC	 = gcc
CFLAGS	 = -Wall -std=gnu99 -O2 -fPIC -I$(CTU_PATH)/src
LDFLAGS	 = 
LIBS     = -lm -lgsl -lgslcblas

CTU_PATH = contrib/ctu

# Source

COMMON_SOURCE = src/air_common.c \
		src/air_cpu.c \
		src/air_error.c \
		src/air_filter.c \
		src/air_image.c \
		src/air_image_pgm.c \
		src/air_image_raw.c \
		src/air_matrix.c \
		src/air_optimize.c \
		src/air_timer.c

COMMON_HEADER = src/air_cpu.h \
		src/air_cuda.h \
		src/air_error.h \
		src/air_filter.h \
		src/air.h \
		src/air_image.h \
		src/air_image_pgm.h \
		src/air_image_raw.h \
		src/air_matrix.h \
		src/air_optimize.h \
		src/air_timer.h

COMMON_OBJECT = $(COMMON_SOURCE:.c=.o)
		
COMPARE_OBJECT   = src/air_compare.o
REGISTER_OBJECT  = src/air_register.o
TRANSFORM_OBJECT = src/air_transform.o

PROGRAMS = air_compare air_register air_transform

LIBCTU	 = $(CTU_PATH)/src/libctu.a

# Rules

all: $(LIBCTU) $(PROGRAMS)

%.o: %.c $(LIBRARY_HEADER)
	$(CC) $(CFLAGS) -o $@ -c $<

%: src/%.o $(COMMON_OBJECT) $(LIBRARY_HEADER)
	$(CC) $(CFLAGS) -o $@ $< $(COMMON_OBJECT) $(LIBCTU) $(LDFLAGS) $(LIBS)

$(LIBCTU): $(CTU_PATH)
	cd $(CTU_PATH); make

doc:
	doxygen

clean:
	rm -f $(COMMON_OBJECT) $(COMPARE_OBJECT) $(REGISTER_OBJECT) $(TRANFORM_OBJECT) $(PROGRAMS)
